#ifndef __MEMORY_LEAK_DETECTION_H__
#define __MEMORY_LEAK_DETECTION_H__

// includes and defines needed to show memory leak information at the end of the programm
#ifdef __cplusplus
# if defined(WIN32) && defined(MSVC) && defined(_DEBUG)
#  define _CRTDBG_MAP_ALLOC
#  include <stdlib.h>
#  include <crtdbg.h>
#  define DEBUG_NEW_LEAKAGE new( _NORMAL_BLOCK, __FILE__, __LINE__ )
# else
#  define DEBUG_NEW_LEAKAGE new
# endif  // defined(WIN32) && defined(_DEBUG)
#endif // __cplusplus


#endif // __MEMORY_LEAK_DETECTION_H__