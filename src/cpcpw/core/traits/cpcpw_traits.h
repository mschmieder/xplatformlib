#ifndef __CPCPW_TYPE_TRAITS_H__
#define __CPCPW_TYPE_TRAITS_H__

#include <cpcpw/core/core_config.h>
#include <cpcpw/core/memory/Endianness.h>

#include <iostream>

# include <memory>
# include <type_traits>
# include <utility>
# include <iostream>
# include <sstream>
# include <functional>

#include <array>
#include <vector>
#include <list>
#include <deque>
#include <queue>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>

#if defined (CPCPW_USE_QT5)
# include <QString>
#endif

  namespace cpcpw
  {
    namespace traits
    {
      /*
      - Multiple inheritance forces ambiguity of member names.
      - SFINAE is used to make aliases to member names.
      - Expression SFINAE is used in just one generic has_member that can accept
      any alias we pass it.
      */

      //Variadic to force ambiguity of class members.  C++11 and up.
      template <typename... Args> struct ambiguate : public Args... {};

      //Non-variadic version of the line above.
      //template <typename A, typename B> struct ambiguate : public A, public B {};

      template<typename A, typename = void>
      struct got_type : std::false_type
      {
      };

      template<typename A>
      struct got_type<A> : std::true_type
      {
        typedef A type;
      };

      template<typename T, T>
      struct sig_check : std::true_type
      {
      };

      template<typename Alias, typename AmbiguitySeed>
      struct has_member
      {
        template<typename C> static char((&f(decltype(&C::value))))[1];
        template<typename C> static char((&f(...)))[2];

        //Make sure the member name is consistently spelled the same.
        static_assert(
          (sizeof(f<AmbiguitySeed>(0)) == 1)
          , "Member name specified in AmbiguitySeed is different from member name specified in Alias, or wrong Alias/AmbiguitySeed has been specified."
        );

        static bool const value = sizeof(f<Alias>(0)) == 2;
      };


      //Check for any member with given name, whether var, func, class, union, enum.
#define CREATE_MEMBER_CHECK(member)                                         \
  \
  template<typename T, typename = std::true_type>                             \
  struct Alias_##member;                                                      \
  \
  template<typename T>                                                        \
  struct Alias_##member <                                                     \
  T, std::integral_constant<bool, got_type<decltype(&T::member)>::value>  \
  > { static const decltype(&T::member) value; };                             \
  \
  struct AmbiguitySeed_##member{ char member; };                             \
  \
  template<typename T>                                                        \
  struct has_member_##member{ \
    static const bool value                                                 \
      = has_member<                                                       \
        Alias_##member<ambiguate<T, AmbiguitySeed_##member>>            \
        , Alias_##member<AmbiguitySeed_##member>                        \
        >::value                                                            \
        ;                                                                       \
  }

      //Check for member variable with given name.
#define CREATE_MEMBER_VAR_CHECK(var_name)                                   \
  \
  template<typename T, typename = std::true_type>                             \
  struct has_member_var_##var_name : std::false_type{};                      \
  \
  template<typename T>                                                        \
  struct has_member_var_##var_name<                                           \
    T                                                                       \
    , std::integral_constant<                                               \
    bool                                                                \
  , !std::is_member_function_pointer<decltype(&T::var_name)>::value   \
  >                                                                       \
> : std::true_type{}


      //Check for member function with given name AND signature.
#define CREATE_MEMBER_FUNC_SIG_CHECK(func_name, func_sig, templ_postfix)    \
  \
  template<typename T, typename = std::true_type>                             \
  struct has_member_func_##templ_postfix : std::false_type{};                \
  \
  template<typename T>                                                        \
  struct has_member_func_##templ_postfix<                                     \
    T, std::integral_constant<                                              \
    bool                                                                \
    , sig_check<func_sig, &T::func_name>::value                         \
    >                                                                       \
    > : std::true_type{}

      //Check for member class with given name.
#define CREATE_MEMBER_CLASS_CHECK(class_name)               \
  \
  template<typename T, typename = std::true_type>             \
  struct has_member_class_##class_name : std::false_type{};  \
  \
  template<typename T>                                        \
  struct has_member_class_##class_name<                       \
    T                                                       \
    , std::integral_constant<                               \
    bool                                                \
    , std::is_class<                                    \
    typename got_type<typename T::class_name>::type \
    >::value                                            \
    >                                                       \
    > : std::true_type{}

      //Check for member union with given name.
#define CREATE_MEMBER_UNION_CHECK(union_name)               \
  \
  template<typename T, typename = std::true_type>             \
  struct has_member_union_##union_name : std::false_type{};  \
  \
  template<typename T>                                        \
  struct has_member_union_##union_name<                       \
    T                                                       \
    , std::integral_constant<                               \
    bool                                                \
    , std::is_union<                                    \
    typename got_type<typename T::union_name>::type \
    >::value                                            \
    >                                                       \
    > : std::true_type{}

      //Check for member enum with given name.
#define CREATE_MEMBER_ENUM_CHECK(enum_name)                 \
  \
  template<typename T, typename = std::true_type>             \
  struct has_member_enum_##enum_name : std::false_type{};    \
  \
  template<typename T>                                        \
  struct has_member_enum_##enum_name<                         \
    T                                                       \
    , std::integral_constant<                               \
    bool                                                \
    , std::is_enum<                                     \
    typename got_type<typename T::enum_name>::type  \
    >::value                                            \
    >                                                       \
    > : std::true_type{}

      //Check for function with given name, any signature.
#define CREATE_MEMBER_FUNC_CHECK(func)          \
  template<typename T>                            \
  struct has_member_func_##func{ \
    static const bool value                     \
      = has_member_##func<T>::value           \
        && !has_member_var_##func<T>::value     \
        && !has_member_class_##func<T>::value   \
        && !has_member_union_##func<T>::value   \
        && !has_member_enum_##func<T>::value    \
        ;                                           \
  }

      //Create all the checks for one member.  Does NOT include func sig checks.
#define CREATE_MEMBER_CHECKS(member)    \
  CREATE_MEMBER_CHECK(member);            \
  CREATE_MEMBER_VAR_CHECK(member);        \
  CREATE_MEMBER_CLASS_CHECK(member);      \
  CREATE_MEMBER_UNION_CHECK(member);      \
  CREATE_MEMBER_ENUM_CHECK(member);       \
  CREATE_MEMBER_FUNC_CHECK(member)



      //Func signature MUST have T as template variable here... simpler this way
      CREATE_MEMBER_FUNC_SIG_CHECK(serialize, size_t (T::*)(Endian, char*) const, cpcpw_serialize);
      CREATE_MEMBER_FUNC_SIG_CHECK(deserialize, T(*)(Endian, const char*, size_t&), cpcpw_deserialize);
      CREATE_MEMBER_FUNC_SIG_CHECK(sizeofType, size_t(T::*)() const, cpcpw_sizeofT);

      // std::string T::toString() const
      CREATE_MEMBER_FUNC_SIG_CHECK(toString, std::string(T::*)() const, to_string);

      // static T T::fromString()
      CREATE_MEMBER_FUNC_SIG_CHECK(fromString, T(*)(const std::string&), from_string);

#define CREATE_HAS_FUNCTION_TRAIT(FNCTN_NAME)                         \
  template<typename type_t, typename return_type_t>                   \
  struct has_function_ ## FNCTN_NAME                                   \
  {                                                                   \
    /* SFINAE operator-has-correct-sig :) */                          \
    template<typename A>                                              \
    static std::true_type test(return_type_t(A::*) () const) {        \
      return std::true_type();                                        \
    }                                                                 \
    /**/                                                              \
    /* SFINAE operator-exists :) */                                   \
    template <typename A>                                             \
    static decltype(test(&A:: ## FNCTN_NAME))                         \
    test(decltype(&A:: ## FNCTN_NAME), void*) {                       \
      /* Operator exists. What about sig? */                          \
      typedef decltype (test(&A:: ## FNCTN_NAME)) return_type;        \
      return return_type();                                           \
    }                                                                 \
    /* SFINAE game over :( */                                         \
    template<typename A>                                              \
    static std::false_type test(...) {                                \
      return std::false_type();                                       \
    }                                                                 \
    /* This will be either `std::true_type` or `std::false_type` */   \
    typedef decltype (test<type_t>(0, 0)) type;                       \
    static const bool value = type::value; /* Which is it? */         \
  };

      /*
      A tag type returned by operator == for the any struct in this namespace
      when T does not support ==.
      */
      struct tag
      {
      };

      /*
      This type soaks up any implicit conversions and makes the following operator ==
      less preferred than any other such operator found via ADL.
      */
      struct any
      {
        /* Conversion constructor for any type. */
        template <class T>
        any(T const&);
      };

      /* Fallback operator == for types T that don't support ==. */
      tag operator == (any const&, any const&);

      /* Fallback operator << for types T that don't support ==. */
      tag operator<< (std::ostream& os, any const&);

      /* Fallback operator << for types T that don't support ==. */
      tag operator! (any const&);

      /* tag operator<<( partial_ordering_helper1, partial_ordering_helper2 ); */

      /*
      Two overloads to distinguish whether T supports a certain operator expression.
      The first overload returns a reference to a two-element character array and is chosen if
      T does not support the expression, such as ==, whereas the second overload returns a char
      directly and is chosen if T supports the expression. So using sizeof(check(<expression>))
      returns 2 for the first overload and 1 for the second overload.
      */
      typedef char yes;
      typedef char (&no)[2];

      no check(tag);

      template <class T>
      yes check(T const&);

      template <class T>
      yes check(T&);

      /* Implementation for our is_equality_comparable template metafunction. */
      template <class T, class ReturnType = bool>
      struct is_equality_comparable_impl
      {
        static typename std::remove_cv<typename std::remove_reference<T>::type>::type const& x;
        typedef decltype (x == x) return_type;
        static const bool value = std::is_same< ReturnType, return_type >::value;
      };

#if defined MSVC
  #pragma warning(push)
  #pragma warning(disable:4244) // due to SFINAE strongly typed enums with size_t can emit warnings conversion warnings when building with msvc12
#endif
      template<typename T>
      class is_output_streamable_impl
      {
          template<typename TT>
          static auto test(int) -> decltype( std::declval<std::stringstream>() << std::declval<TT&>(), std::true_type() );

          template<typename, typename>
          static auto test(...) -> std::false_type;

        public:
          static const bool value = decltype(test<T>(0))::value;
      };

      template<typename T>
      class is_input_streamable_impl
      {
          template<typename TT>
          static auto test(int) -> decltype( std::declval<std::stringstream>() >> std::declval<TT&>(), std::true_type() );

          template<typename, typename>
          static auto test(...) -> std::false_type;

        public:
          static const bool value = decltype(test<T>(0))::value;
      };

#if defined MSVC
  #pragma warning(pop)
#endif

      template <class T>
      struct is_binary_invertible_impl
      {
        static typename std::remove_cv<typename std::remove_reference<T>::type>::type const& x;
        static const bool value = sizeof(check(!x)) == sizeof(yes);
      };


      template <class T>
      struct is_equality_comparable : traits::is_equality_comparable_impl<T> {};

      template< class T, class Alloc>
      struct is_equality_comparable< std::vector<T, Alloc> > : is_equality_comparable<T> {};

      template< class T, class Alloc>
      struct is_equality_comparable< std::list<T, Alloc> > : is_equality_comparable<T> {};

      template< class T, class Container>
      struct is_equality_comparable< std::deque<T, Container> > : is_equality_comparable<T> {};

      template< class T, class Container>
      struct is_equality_comparable< std::queue<T, Container> > : is_equality_comparable<T> {};

      template<class T, class Container, class Compare>
      struct is_equality_comparable<std::priority_queue<T, Container, Compare> > : is_equality_comparable<T> {};

      template<class Key, class Compare, class Alloc>
      struct is_equality_comparable<std::set<Key, Compare, Alloc> > : is_equality_comparable<Key> {};

      template<class Key, class Compare, class Alloc>
      struct is_equality_comparable<std::multiset<Key, Compare, Alloc> > : is_equality_comparable<Key> {};

      template<class K, class T, class Comp, class Alloc>
      struct is_equality_comparable<std::map<K, T, Comp, Alloc> >
      {
        const static bool value = is_equality_comparable<T>::value && is_equality_comparable<T>::value;
      };

      template<class K, class T, class Comp, class Alloc>
      struct is_equality_comparable<std::multimap<K, T, Comp, Alloc> >
      {
        const static bool value = is_equality_comparable<T>::value && is_equality_comparable<T>::value;
      };

      template <class T>
      struct is_output_streamable: traits::is_output_streamable_impl<T> {};

      template <class T>
      struct is_input_streamable: traits::is_input_streamable_impl<T> {};

      template <class T>
      struct is_binary_invertible: traits::is_binary_invertible_impl<T> {};

      //////////////////////////////////////////////////////////////////////////////////////////////
      // TRAIT: is_associative_container
      //////////////////////////////////////////////////////////////////////////////////////////////
      template<class T> struct is_associative_container: public std::false_type {};

      /* std::map */
      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< std::map<Key, T, Compare, Allocator> >: public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< std::map<Key, T, Compare, Allocator>& > : public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container < std::map<Key, T, Compare, Allocator>&& > : public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< const std::map<Key, T, Compare, Allocator> >: public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< const std::map<Key, T, Compare, Allocator>& >: public std::true_type {};

      /* std::multimap */
      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< std::multimap<Key, T, Compare, Allocator> >: public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< std::multimap<Key, T, Compare, Allocator>& > : public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container < std::multimap<Key, T, Compare, Allocator>&& > : public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< const std::multimap<Key, T, Compare, Allocator> >: public std::true_type {};

      template < class Key, class T, class Compare, class Allocator >
      struct is_associative_container< const std::multimap<Key, T, Compare, Allocator>& >: public std::true_type {};

      /* std::unordered_map */
      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< std::unordered_map<Key, T, Hash, KeyEqual, Allocator> >: public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< std::unordered_map<Key, T, Hash, KeyEqual, Allocator>& > : public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container < std::unordered_map<Key, T, Hash, KeyEqual, Allocator>&& > : public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< const std::unordered_map<Key, T, Hash, KeyEqual, Allocator> >: public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< const std::unordered_map<Key, T, Hash, KeyEqual, Allocator>& >: public std::true_type {};

      /* std::unordered_map */
      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> >: public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>& > : public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container < std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>&& > : public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< const std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> >: public std::true_type {};

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_associative_container< const std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>& >: public std::true_type {};

      //////////////////////////////////////////////////////////////////////////////////////////////
      // TRAIT: is_container
      //////////////////////////////////////////////////////////////////////////////////////////////
      template<class T> struct is_container: public std::false_type {};

      /* vectors / lists */
      template<class T, class Alloc>
      struct is_container<std::vector<T, Alloc> > : public std::true_type {};

      template<class T, class Alloc>
      struct is_container<std::list<T, Alloc> > : public std::true_type {};

      /* array */
      template<class T, std::size_t N>
      struct is_container<std::array<T, N> > : public std::true_type {};

      /* quques */
      template<class T, class Container>
      struct is_container<std::deque<T, Container> > : public std::true_type {};

      template<class T, class Container>
      struct is_container<std::queue<T, Container> > : public std::true_type {};

      template<class T, class Container, class Compare>
      struct is_container<std::priority_queue<T, Container, Compare> > : public std::true_type {};

      /* map / sets */
      template<class Key, class Compare, class Alloc>
      struct is_container<std::set<Key, Compare, Alloc> > : public std::true_type {};

      template<class Key, class Compare, class Alloc>
      struct is_container<std::multiset<Key, Compare, Alloc> > : public std::true_type {};

      template<class K, class T, class Comp, class Alloc>
      struct is_container<std::map<K, T, Comp, Alloc> > : public std::true_type {};

      template<class K, class T, class Comp, class Alloc>
      struct is_container<std::multimap<K, T, Comp, Alloc> > : public std::true_type {};


      /* vectors / lists */
      template<class T, class Alloc>
      struct is_container<const std::vector<T, Alloc> > : public std::true_type {};

      template<class T, class Alloc>
      struct is_container<const std::list<T, Alloc> > : public std::true_type {};

      /* array */
      template<class T, std::size_t N>
      struct is_container<const std::array<T, N> > : public std::true_type {};

      /* queues */
      template<class T, class Container>
      struct is_container<const std::deque<T, Container> > : public std::true_type {};

      template<class T, class Container>
      struct is_container<const std::queue<T, Container> > : public std::true_type {};

      template<class T, class Container, class Compare>
      struct is_container<const std::priority_queue<T, Container, Compare> > : public std::true_type {};

      /* map / sets */
      template<class Key, class Compare, class Alloc>
      struct is_container<const std::set<Key, Compare, Alloc> > : public std::true_type {};

      template<class Key, class Compare, class Alloc>
      struct is_container<const std::multiset<Key, Compare, Alloc> > : public std::true_type {};

      template<class K, class T, class Comp, class Alloc>
      struct is_container<const std::map<K, T, Comp, Alloc> > : public std::true_type {};

      template<class K, class T, class Comp, class Alloc>
      struct is_container<const std::multimap<K, T, Comp, Alloc> > : public std::true_type {};

      //////////////////////////////////////////////////////////////////////////////////////////////
      // STL ARRAY TRAITS
      //////////////////////////////////////////////////////////////////////////////////////////////
      template<class T> struct is_stl_array : public std::false_type
      {
        typedef void array_value_type;
        static const size_t array_size = 0;
      };

      template<class T, std::size_t N>
      struct is_stl_array< const std::array<T, N> > : public std::true_type
      {
        typedef typename std::array<T, N>::value_type array_value_type;
        static const size_t array_size = N;
      };

      template<class T, std::size_t N>
      struct is_stl_array< std::array<T, N> > : public std::true_type
      {
        typedef typename std::array<T, N>::value_type array_value_type;
        static const size_t array_size = N;
      };

      template< class type_t>
      struct is_trivialy_memcopyable
      {
        const static bool value = (std::is_fundamental<typename std::remove_reference<type_t>::type>::value && !std::is_array<typename std::remove_reference<type_t>::type>::value)
                                  || std::is_union<typename std::remove_reference<type_t>::type>::value
                                  || std::is_enum<typename std::remove_reference<type_t>::type>::value
                                  || (is_stl_array<typename std::remove_reference<type_t>::type>::value && std::is_fundamental<typename is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value);
      };

      template<class type_t>
      struct is_serializable
      {
        typedef typename std::remove_const<typename std::remove_reference<type_t>::type>::type type_p;

        const static bool isFundamental = std::is_fundamental<type_p>::value || std::is_array<type_p>::value;
        const static bool hasSerialize = has_member_func_cpcpw_serialize< type_p >::value;
        const static bool hasDeserialize = has_member_func_cpcpw_deserialize< type_p >::value;
        const static bool hasSizeOfT = has_member_func_cpcpw_sizeofT< type_p >::value;
        const static bool isString = std::is_same<std::string, type_p>::value;
        const static bool isUnion = std::is_union<type_p>::value;

        const static bool value = isString || isFundamental || (hasSerialize && hasDeserialize && hasSizeOfT) || isUnion;
      };

      template<class type_t>
      struct has_to_string
      {
        typedef typename std::remove_const<typename std::remove_reference<typename std::remove_pointer<type_t>::type>::type>::type type_p;
        const static bool value = has_member_func_to_string<type_p>::value;
      };

      template<class type_t>
      struct has_from_string
      {
        typedef typename std::remove_const<typename std::remove_reference<typename std::remove_pointer<type_t>::type>::type>::type type_p;
        const static bool value = has_member_func_from_string<type_p>::value;
      };

      template<class type_t>
      struct is_ascii_serializable
      {
        typedef typename std::remove_const<typename std::remove_reference<typename std::remove_pointer<type_t>::type>::type>::type type_p;
        const static bool value = (has_to_string<type_p>::value && has_from_string<type_p>::value) ||
                                  (is_output_streamable<type_p>::value && is_input_streamable_impl<type_p>::value);
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::vector
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template<class type_t, class Alloc>
      struct is_serializable<std::vector<type_t, Alloc> >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<const std::vector<type_t, Alloc> >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<const std::vector<type_t, Alloc>& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<std::vector<type_t, Alloc>& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable < std::vector<type_t, Alloc>&& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      // is_ascii_serializable
      template<class type_t, class Alloc>
      struct is_ascii_serializable<std::vector<type_t, Alloc> >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<const std::vector<type_t, Alloc> >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<const std::vector<type_t, Alloc>& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<std::vector<type_t, Alloc>& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable < std::vector<type_t, Alloc>&& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::list
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template<class type_t, class Alloc>
      struct is_serializable<std::list<type_t, Alloc> >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<const std::list<type_t, Alloc> >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<const std::list<type_t, Alloc>& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<std::list<type_t, Alloc>& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      // is_ascii_serializable
      template<class type_t, class Alloc>
      struct is_ascii_serializable<std::list<type_t, Alloc> >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<const std::list<type_t, Alloc> >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<const std::list<type_t, Alloc>& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<std::list<type_t, Alloc>& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::deque
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template<class type_t, class Alloc>
      struct is_serializable<std::deque<type_t, Alloc> >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<const std::deque<type_t, Alloc> >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<const std::deque<type_t, Alloc>& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_serializable<std::deque<type_t, Alloc>& >
      {
        const static bool value = is_serializable<type_t>::value;
      };

      // is_ascii_serializable
      template<class type_t, class Alloc>
      struct is_ascii_serializable<std::deque<type_t, Alloc> >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<const std::deque<type_t, Alloc> >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<const std::deque<type_t, Alloc>& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      template<class type_t, class Alloc>
      struct is_ascii_serializable<std::deque<type_t, Alloc>& >
      {
        const static bool value = is_ascii_serializable<type_t>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::set
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template<class T> struct is_stl_set : public std::false_type {};

      template<class Key, class Compare, class Alloc>
      struct is_stl_set<std::set<Key, Compare, Alloc> > : public std::true_type {};

      template<class Key, class Compare, class Alloc>
      struct is_stl_set<const std::set<Key, Compare, Alloc> > : public std::true_type {};

      template<class Key, class Compare, class Alloc>
      struct is_stl_set<const std::set<Key, Compare, Alloc>& > : public std::true_type {};

      template<class Key, class Compare, class Alloc>
      struct is_stl_set<std::set<Key, Compare, Alloc>& > : public std::true_type {};

      template<class Key, class Compare, class Alloc>
      struct is_serializable<std::set<Key, Compare, Alloc> >
      {
        const static bool value = is_serializable<Key>::value;
      };

      template<class Key, class Compare, class Alloc>
      struct is_serializable<const std::set<Key, Compare, Alloc> >
      {
        const static bool value = is_serializable<Key>::value;
      };

      template<class Key, class Compare, class Alloc>
      struct is_serializable<const std::set<Key, Compare, Alloc>& >
      {
        const static bool value = is_serializable<Key>::value;
      };

      template<class Key, class Compare, class Alloc>
      struct is_serializable<std::set<Key, Compare, Alloc>& >
      {
        const static bool value = is_serializable<Key>::value;
      };

      // is_ascii_serializable
      template<class Key, class Compare, class Alloc>
      struct is_ascii_serializable<std::set<Key, Compare, Alloc> >
      {
        const static bool value = is_ascii_serializable<Key>::value;
      };

      template<class Key, class Compare, class Alloc>
      struct is_ascii_serializable<const std::set<Key, Compare, Alloc> >
      {
        const static bool value = is_ascii_serializable<Key>::value;
      };

      template<class Key, class Compare, class Alloc>
      struct is_ascii_serializable<const std::set<Key, Compare, Alloc>& >
      {
        const static bool value = is_ascii_serializable<Key>::value;
      };

      template<class Key, class Compare, class Alloc>
      struct is_ascii_serializable<std::set<Key, Compare, Alloc>& >
      {
        const static bool value = is_ascii_serializable<Key>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::map
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< std::map<Key, T, Compare, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< std::map<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< const std::map<Key, T, Compare, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< const std::map<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable < std::map<Key, T, Compare, Allocator>&& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      // is_ascii_serializable
      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< std::map<Key, T, Compare, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< std::map<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< const std::map<Key, T, Compare, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< const std::map<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable < std::map<Key, T, Compare, Allocator>&& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::multimap
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< std::multimap<Key, T, Compare, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< std::multimap<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< const std::multimap<Key, T, Compare, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable< const std::multimap<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_serializable < std::multimap<Key, T, Compare, Allocator>&& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      // is_ascii_serializable
      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< std::multimap<Key, T, Compare, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< std::multimap<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< const std::multimap<Key, T, Compare, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable< const std::multimap<Key, T, Compare, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Compare, class Allocator >
      struct is_ascii_serializable < std::multimap<Key, T, Compare, Allocator>&& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::unordered_map
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< std::unordered_map<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< std::unordered_map<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< const std::unordered_map<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< const std::unordered_map<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable < std::unordered_map<Key, T, Hash, KeyEqual, Allocator>&& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      // is_ascii_serializable
      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< std::unordered_map<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< std::unordered_map<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< const std::unordered_map<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< const std::unordered_map<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable < std::unordered_map<Key, T, Hash, KeyEqual, Allocator>&& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      ///////////////////////////////////////////////////////////////////////////////////////////////
      // std::unordered_multimap
      ///////////////////////////////////////////////////////////////////////////////////////////////
      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< const std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable< const std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_serializable < std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>&& >
      {
        const static bool value = is_serializable<Key>::value && is_serializable<T>::value;
      };

      // is_ascii_serializable
      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< const std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable< const std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

      template < class Key, class T, class Hash, class KeyEqual, class Allocator >
      struct is_ascii_serializable < std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator>&& >
      {
        const static bool value = is_ascii_serializable<Key>::value && is_ascii_serializable<T>::value;
      };

#if defined (USE_QT)
      // QT Types
      template <>
      struct is_ascii_serializable<QString>
      {
        const static bool value = true;
      };
#endif


    }// namespace traits
  } /* namespace cpcpw */

/**
* @}
*/

#endif
