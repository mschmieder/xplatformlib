#include <cpcpw/core/exception/Exception.h>
#include <cpcpw/core/MemoryLeakDetection.h>
#define new DEBUG_NEW_LEAKAGE

using namespace std;
using namespace cpcpw;

Exception::Exception(const std::string& errorMsg, const std::string& codeInfo) throw() : std::exception()
{
  m_strMessage  = errorMsg;
  m_strCodeInfo = codeInfo;
#if defined _DEBUG || defined DEBUG
  std::cerr << "exception raised: \"" << this->what() << "\"" << std::endl;
  std::cerr << "   at: " << codeInfo;
#endif
}

Exception::Exception() throw()
{
}

Exception::~Exception() throw()
{
}

const char* Exception::what() const throw()
{
  return m_strMessage.c_str();
}

const std::string& Exception::codeInfo() const throw()
{
  return m_strCodeInfo;
}

void Exception::setCodeInfo(const std::ostringstream& codeInfo)
{
  m_strCodeInfo = codeInfo.str();
}
