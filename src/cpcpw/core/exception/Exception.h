#ifndef _CPCPW_EXCEPTION_H_
# define _CPCPW_EXCEPTION_H_

/****************************************************************************
* INCLUDES
****************************************************************************/
#include <cpcpw/core/core_config.h>
#include <exception>
#include <sstream>
#include <iostream>

/****************************************************************************
* MACROS
****************************************************************************/
#define EXCEPTION_FILE_INFO __FILE__ <<": "
#define EXCEPTION_LINE_INFO __LINE__ <<": "
#define EXCEPTION_FUNCTION_INFO __FUNCTION__ <<"(..)"


#define CPCPW_ERROR(arg) CPCPW_THROW(cpcpw::Exception, arg)

#define CPCPW_THROW(EX_TYPE, MSG) {std::ostringstream os; os<< MSG; std::ostringstream codeInfo;\
  codeInfo <<EXCEPTION_FILE_INFO<<EXCEPTION_LINE_INFO<<EXCEPTION_FUNCTION_INFO<<std::endl;\
  throw EX_TYPE(os.str(),codeInfo.str() );}

#define CPCPW_THROWOBJ(EX_OBJECT) {std::ostringstream codeInfo;\
  codeInfo <<EXCEPTION_FILE_INFO<<EXCEPTION_LINE_INFO<<EXCEPTION_FUNCTION_INFO<<std::endl;\
  EX_OBJECT.setCodeInfo(codeInfo);\
  throw EX_OBJECT;}


namespace cpcpw {
  class CPCPW_CORE_EXCEPTION_DECL Exception : public std::exception
  {
  public:
    Exception(const std::string& errorMsg, const std::string& codeInfo = "") throw();
    Exception() throw ();

    virtual ~Exception() throw();

    virtual const char* what() const throw();
    virtual const std::string& codeInfo() const throw();
    void setCodeInfo(const std::ostringstream& codeInfo);

  protected:
    std::string m_strMessage;
    std::string m_strCodeInfo;
  }; // class Exception
} // cpcpw

#endif //_CPCPW_EXCEPTION_H_