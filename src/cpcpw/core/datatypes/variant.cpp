#include "variant.h"
#include <cpcpw/core/exception/Exception.h>
#include <cpcpw/core/memory/Endianness.h>
#include <cpcpw/core/memory/ArrayDeleter.h>

#include <iostream>
#include <sstream>
#include <string>

#include <functional>

#include <cpcpw/core/MemoryLeakDetection.h>
#define new DEBUG_NEW_LEAKAGE

//#if defined (CPCPW_USE_QT5)
//# include <QBitmap>
//# include<QBitArray>
//# include<QBitmap>
//# include<QBrush>
//# include<QByteArray>
//# include<QChar>
//# include<QColor>
//# include<QCursor>
//# include<QDate>
//# include<QDateTime>
//# include<QFont>
//# include<QHash>
//# include<QIcon>
//# include<QImage>
//# include<QLine>
//# include<QLineF>
//# include<QList>
//# include<QLocale>
//# include<QMatrix>
//# include<QTransform>
//# include<QMatrix4x4>
//# include<QPalette>
//# include<QPen>
//# include<QPixmap>
//# include<QPoint>
//# include<QPointF>
//# include<QPolygon>
//# include<QQuaternion>
//# include<QRect>
//# include<QRectF>
//# include<QRegExp>
//# include<QRegion>
//# include<QSize>
//# include<QSizeF>
//# ifdef QT_WIDGETS_LIB
//#  include<QSizePolicy>
//# endif
//# include<QString>
//# include<QStringList>
//# include<QTextFormat>
//# include<QTextLength>
//# include<QTime>
//# include<QUrl>
//# include<QVector2D>
//# include<QVector3D>
//# include<QVector4D>
//#endif

using namespace std::placeholders;

namespace cpcpw {
  std::shared_ptr<char> variant_base::serialize(Endian endian, size_t& nSize) const
  {
    // create buffer
    nSize = sizeofT();
    std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
    nSize = serialize(endian, pBuffer.get());
    return pBuffer;
  }

#if defined (CPCPW_USE_QT5)
#if defined(MSVC)
# pragma warning( push )
# pragma warning( disable:4063 )
# pragma warning( disable:4068 )
#else
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wswitch"
#endif
  variant_base* variant_base::fromQVariant(const QVariant& qVariant)
  {
    variant_base* pVariant = nullptr;
//    switch (qVariant.type())
//    {
//    case QVariant::Invalid:
//      // nothing to do here
//      break;
//    case QVariant::BitArray:
//      pVariant = variant::createRaw(qVariant.toBitArray());
//      break;
//    case QVariant::Bitmap:
//      pVariant = variant::createRaw(qVariant.value<QBitmap>());
//      break;
//    case QVariant::Bool:
//      pVariant = variant::createRaw(qVariant.toBool());
//      break;
//    case QVariant::Brush:
//      pVariant = variant::createRaw(qVariant.toInt());
//      break;
//    case QVariant::ByteArray:
//    {
//      QByteArray byteArrayBuffer = qVariant.toByteArray();
//      const unsigned char* begin = reinterpret_cast<unsigned char*>(byteArrayBuffer.data());
//      const unsigned char* end = begin + byteArrayBuffer.length();
//      pVariant = variant::createRaw(std::vector<uint8_t>(begin, end));
//    }
//    break;
//    case QVariant::Char:
//      pVariant = variant::createRaw(qVariant.toChar());
//      break;
//    case QVariant::Color:
//      pVariant = variant::createRaw(qVariant.value<QColor>());
//      break;
//    case QVariant::Cursor:
//      pVariant = variant::createRaw(qVariant.value<QCursor>());
//      break;
//    case QVariant::Date:
//      pVariant = variant::createRaw(qVariant.toDate());
//      break;
//    case QVariant::DateTime:
//      pVariant = variant::createRaw(qVariant.toDateTime());
//      break;
//    case QVariant::Double:
//      pVariant = variant::createRaw(qVariant.toDouble());
//      break;
//    case QMetaType::Float:
//      // XXX : float is only defined as QMetaType and is considered to be a user type (\sa QVariant::type())
//      pVariant = variant::createRaw(qVariant.toFloat());
//      break;
//    case QVariant::Font:
//      pVariant = variant::createRaw(qVariant.value<QFont>());
//      break;
//    case QVariant::Hash:
//      pVariant = variant::createRaw(qVariant.toHash());
//      break;
//    case QVariant::Icon:
//      pVariant = variant::createRaw(qVariant.value<QIcon>());
//      break;
//    case QVariant::Image:
//      pVariant = variant::createRaw(qVariant.value<QImage>());
//      break;
//    case QVariant::Int:
//      pVariant = variant::createRaw(qVariant.toInt());
//      break;
//    case QVariant::Line:
//      pVariant = variant::createRaw(qVariant.toLine());
//      break;
//    case QVariant::LineF:
//      pVariant = variant::createRaw(qVariant.toLineF());
//      break;
//    case QVariant::List:
//      pVariant = variant::createRaw(qVariant.toList());
//      break;
//    case QVariant::Locale:
//      pVariant = variant::createRaw(qVariant.toLocale());
//      break;
//    case QVariant::LongLong:
//      pVariant = variant::createRaw(qVariant.toLongLong());
//      break;
//    case QVariant::Matrix:
//      pVariant = variant::createRaw(qVariant.value<QMatrix>());
//      break;
//    case QVariant::Transform:
//      pVariant = variant::createRaw(qVariant.value<QTransform>());
//      break;
//    case QVariant::Matrix4x4:
//      pVariant = variant::createRaw(qVariant.value<QMatrix4x4>());
//      break;
//    case QVariant::Palette:
//      pVariant = variant::createRaw(qVariant.value<QPalette>());
//      break;
//    case QVariant::Pen:
//      pVariant = variant::createRaw(qVariant.value<QPen>());
//      break;
//    case QVariant::Pixmap:
//      pVariant = variant::createRaw(qVariant.value<QPixmap>());
//      break;
//    case QVariant::Point:
//      pVariant = variant::createRaw(qVariant.toPoint());
//      break;
//    case QVariant::PointF:
//      pVariant = variant::createRaw(qVariant.toPointF());
//      break;
//    case QVariant::Polygon:
//      pVariant = variant::createRaw(qVariant.value<QPolygon>());
//      break;
//    case QVariant::Quaternion:
//      pVariant = variant::createRaw(qVariant.value<QQuaternion>());
//      break;
//    case QVariant::Rect:
//      pVariant = variant::createRaw(qVariant.toRect());
//      break;
//    case QVariant::RectF:
//      pVariant = variant::createRaw(qVariant.toRectF());
//      break;
//    case QVariant::RegExp:
//      pVariant = variant::createRaw(qVariant.toRegExp());
//      break;
//    case QVariant::Region:
//      pVariant = variant::createRaw(qVariant.value<QRegion>());
//      break;
//    case QVariant::Size:
//      pVariant = variant::createRaw(qVariant.toSize());
//      break;
//    case QVariant::SizeF:
//      pVariant = variant::createRaw(qVariant.toSizeF());
//      break;
//#ifdef QT_WIDGETS_LIB
//    case QVariant::SizePolicy:
//      pVariant = variant::createRaw(qVariant.value<QSizePolicy>());
//      break;
//#endif
//    case QVariant::String:
//      pVariant = variant::createRaw( std::string(qVariant.toString().toLatin1().constData()) );
//      break;
//    case QVariant::StringList:
//      pVariant = variant::createRaw(qVariant.toStringList());
//      break;
//    case QVariant::TextFormat:
//      pVariant = variant::createRaw(qVariant.value<QTextFormat>());
//      break;
//    case QVariant::TextLength:
//      pVariant = variant::createRaw(qVariant.value<QTextLength>());
//      break;
//    case QVariant::Time:
//      pVariant = variant::createRaw(qVariant.toTime());
//      break;
//    case QVariant::UInt:
//      pVariant = variant::createRaw(qVariant.toUInt());
//      break;
//    case QVariant::ULongLong:
//      pVariant = variant::createRaw(qVariant.toULongLong());
//      break;
//    case QVariant::Url:
//      pVariant = variant::createRaw(qVariant.toUrl());
//      break;
//    case QVariant::Vector2D:
//      pVariant = variant::createRaw(qVariant.value<QVector2D>());
//      break;
//    case QVariant::Vector3D:
//      pVariant = variant::createRaw(qVariant.value<QVector3D>());
//      break;
//    case QVariant::Vector4D:
//      pVariant = variant::createRaw(qVariant.value<QVector3D>());
//      break;
//    case QVariant::UserType:
//      break;
//    default:
//      break;
//    }
    return pVariant;
  }
#if defined(MSVC)
# pragma warning( pop )
#else
# pragma GCC diagnostic pop
#endif

#endif
} // namespace cpcpw
