#ifndef _HDP_ITERATOR_H_
# define _HDP_ITERATOR_H_

#include <cpcpw/core/core_config.h>
#include <cpcpw/core/datatypes/variant.h>

#include <map>
#include <vector>
#include <string>
#include <iterator>
#include <limits>

#ifdef max
# undef max
#endif

#ifdef min
# undef min
#endif

namespace cpcpw
{
   // forward declaration needed by the iterator
    template <typename index_type_t> class hdp;
  
    /** @class hdp_iterator
    ******************************************************************************************************
    @brief 
    @author schmieder (2012)
    \ingroup module_core_datatypes
    *****************************************************************************************************/
    template <typename input_iterator, class index_type_t, class data_type_t>
    class hdp_iterator : public std::iterator<std::bidirectional_iterator_tag, index_type_t> 
    {
    public:
      //default constructor
      hdp_iterator(){}
  
      // operator* overload
      data_type_t operator*() const;
  
      const input_iterator& operator->() const;
  
      // operator++ overload
      const hdp_iterator<input_iterator,index_type_t,data_type_t>& operator++();    // prefix ++
  
      // ++operator overload
      hdp_iterator<input_iterator,index_type_t,data_type_t> operator++(int);        // postfix ++
  
      // operator-- overload
      const hdp_iterator<input_iterator,index_type_t,data_type_t>& operator--();    // prefix --
  
      // --operator overload
      hdp_iterator<input_iterator,index_type_t,data_type_t> operator--(int);        // postfix --
  
      // operator!= overload
      bool operator!=(const hdp_iterator<input_iterator,index_type_t,data_type_t>& other) const;
  
      // operator== overload
      bool operator==(const hdp_iterator<input_iterator,index_type_t,data_type_t>& other) const;
  
    private:
      input_iterator m_currentMapIt;
  
      // is by hdp<index_type_t> so it can call the private constructor
      friend class hdp<index_type_t>; 
  
      // private constructor that can only be called by friend class hdp<index_type_t>
      hdp_iterator(input_iterator item) 
        : m_currentMapIt(item)
      {}
    };
  
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    data_type_t hdp_iterator<input_iterator,index_type_t,data_type_t>::operator*() const
    {
      return *m_currentMapIt;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    const input_iterator& hdp_iterator<input_iterator,index_type_t,data_type_t>::operator->() const
    {
      return m_currentMapIt;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    const hdp_iterator<input_iterator,index_type_t,data_type_t>& hdp_iterator<input_iterator,index_type_t,data_type_t>::operator++()
    {
      m_currentMapIt++;
      return *this;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    hdp_iterator<input_iterator,index_type_t,data_type_t> hdp_iterator<input_iterator,index_type_t,data_type_t>::operator++(int)
    {
      hdp_iterator<input_iterator,index_type_t,data_type_t> ans = *this; // copy current iterator
      this->operator++();
      return ans;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    const hdp_iterator<input_iterator,index_type_t,data_type_t>& hdp_iterator<input_iterator,index_type_t,data_type_t>::operator--()
    {
      m_currentMapIt--;
      return *this;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    hdp_iterator<input_iterator,index_type_t,data_type_t> hdp_iterator<input_iterator,index_type_t,data_type_t>::operator--(int)
    {
      hdp_iterator<input_iterator,index_type_t,data_type_t> ans = *this; // copy current iterator
      this->operator--();
      return ans;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    bool hdp_iterator<input_iterator,index_type_t,data_type_t>::operator!=(const hdp_iterator<input_iterator,index_type_t,data_type_t>& other) const
    {
      return this->m_currentMapIt != other.m_currentMapIt;
    }
  
    template <typename input_iterator, class index_type_t, class data_type_t>
    bool hdp_iterator<input_iterator,index_type_t,data_type_t>::operator==(const hdp_iterator<input_iterator,index_type_t,data_type_t>& other) const
    {
      return !(this->operator!=(other));
    }
} /* namespace cpcpw */


#endif //_HDP_ITERATOR_H_
