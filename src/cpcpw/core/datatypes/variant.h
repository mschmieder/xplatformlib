#ifndef __VARIANT_H__
# define __VARIANT_H__
#include <cpcpw/core/core_config.h>
#include <cpcpw/core/memory/Memory.h>
#include <cpcpw/core/traits/cpcpw_traits.h>

#if defined (CPCPW_USE_QT5)
# include <QVariant>
# include <QString>
# include <QByteArray>
# include <QDebug>
#endif

#include <memory>
#include <type_traits>
#include <functional>

#include <sstream>
#include <iostream>


#if defined MSVC
#pragma warning(push)
#pragma warning(disable:4702) // due to SFINAE it can lead to unreachable code inside the variant
#endif

namespace cpcpw {
  // forward declarations
  template <typename type_t> class variant_t;
  class variant_base;

  typedef std::shared_ptr<variant_base> variant;
 
  /** @class variant_base
  ******************************************************************************************************
  @brief
  @author schmieder (2012)
  \ingroup module_core_datatypes
  *****************************************************************************************************/
  class CPCPW_CORE_DATATYPES_DECL variant_base
  {
  public:
    /**
    ****************************************************************************************************
    Typedefs that define the encoder and decoder function interfaces that
    are necessary if you want to implement and register your own encoder/decoders

    These typedefs can be used by simply calling the constructor
    variant_base::encode_functor( &my_encoder_function );

    but also in combination with std::bind
    variant_base::encode_functor = std::bind(my_encoder_function,_1,_2,_3,_4,someOtherValue);
    *****************************************************************************************************/
    virtual ~variant_base()
    {}

    /**
    ****************************************************************************************************
    Trait that identifies whether variant_base is an arithmetic type (either an integral or a floating point type).

    FUNDAMENTAL ARITHMETIC TYPES
    - INTEGRAL TYPES
    - bool
    - char
    - char16_t
    - char32_t
    - wchar_t
    - signed char
    - short int
    - int
    - long int
    - long long int
    - unsigned char
    - unsigned short int
    - uint32_t
    - unsigned long int
    - unsigned long long int
    - FLOATING POINT TYPES
    - float
    - double
    - long double

    *****************************************************************************************************/
    virtual bool isArithmetic()    const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is a class type, but not a union type.
    *****************************************************************************************************/
    virtual bool isArray()         const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is an array type.
    *****************************************************************************************************/
    virtual bool isClass()         const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is a fundamental type.

    - INTEGRAL TYPES
    - bool
    - char
    - char16_t
    - char32_t
    - wchar_t
    - signed char
    - short int
    - int
    - long int
    - long long int
    - unsigned char
    - unsigned short int
    - uint32_t
    - unsigned long int
    - unsigned long long int
    - FLOATING POINT TYPES
    - float
    - double
    - long double
    - VOID
    - void
    - NULL POINTER
    - std::nullptr_t

    *****************************************************************************************************/
    virtual bool isFundamental()   const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is an array type.

    - INTEGRAL TYPES
    - bool
    - char
    - char16_t
    - char32_t
    - wchar_t
    - signed char
    - short int
    - int
    - long int
    - long long int
    - unsigned char
    - unsigned short int
    - uint32_t
    - unsigned long int
    - unsigned long long int
    *****************************************************************************************************/
    virtual bool isIntegral()      const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is an array type.

    - FLOATING POINT TYPES
    - float
    - double
    - long double
    *****************************************************************************************************/
    virtual bool isFloatingPoint()       const = 0;
    virtual bool isContainer()           const = 0;
    virtual bool isStreamable()          const = 0;
    virtual bool isEqualComparable()     const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is a const type

    *****************************************************************************************************/
    virtual bool isConst()              const = 0;

    /**
    ****************************************************************************************************
    trait that identifies whether T is serializable or not

    *****************************************************************************************************/
    virtual bool isSerializable()       const = 0;

    /**
    ****************************************************************************************************
    returns the typeinfo generated by the compiler of the type lying underneeth
    *****************************************************************************************************/
    virtual const std::type_info& getTypeInfo()                                   const = 0;

    /**
    ****************************************************************************************************
    @return  size of datatype type_t
    @retval sizeof(type_t)
    *****************************************************************************************************/
    virtual       size_t          sizeofT()                                       const = 0;

    /**
    ****************************************************************************************************
    @return a void pointer to the start address of the underlying data
    *****************************************************************************************************/
    virtual       const void*     address()                                       const = 0;
    virtual       void*           address() = 0;
    virtual       const void*     addressConst() = 0;

    /**
    ****************************************************************************************************
    @return a ptr_type_t pointer to the underlying data.

    \attention this function will NOT check if ptr_type_t corresponds to the underlying type type_t and
    therefore can cause errors if you use a wrong type
    *****************************************************************************************************/
    template <typename ptr_type_t>
    typename std::enable_if < !std::is_const<ptr_type_t>::value, ptr_type_t* >::type ptr()
    {
      return reinterpret_cast<ptr_type_t*>(this->address());
    }

    template <typename ptr_type_t>
    typename std::enable_if<std::is_const<ptr_type_t>::value, ptr_type_t*>::type ptr()
    {
      return reinterpret_cast<ptr_type_t*>(this->addressConst());
    }


    template <typename ptr_type_t>
    const ptr_type_t* ptr() const;

    /**
    ****************************************************************************************************
    @return a copy of the underlying data.

    \attention this function will NOT check if data_type_t corresponds to the underlying type type_t and
    therefore can cause errors if you use a wrong type
    *****************************************************************************************************/
    template <typename data_type_t>
    data_type_t data();

    /**
    ****************************************************************************************************
    returns a copy of the underlying data.

    \attention this function WILL check if data_type_t corresponds to the underlying type type_t. If you
    know the type explicitly use variant_base::data() which will not induce the type-checking overhead.

    \exception Throws an Exception if correct type is not met.
    *****************************************************************************************************/
    template <typename data_type_t>
    data_type_t value();

    template <typename data_type_t>
    const data_type_t value() const;

    /**
    ****************************************************************************************************
    sets the underlying value to the one provided by the function. This will only work if
    value->getTypeId() == typeid(type_t). If this is not the case the function will return 'false'
    *****************************************************************************************************/

    /**
    ****************************************************************************************************
    This function 'tries' to convert the underlying data to a std::string. Through some template meta
    programming techniques it is assured that you can provide the variant_base with any type of data, even
    if it does not support a streamable operator.

    The function will evaluate (compile time) if type_t has a streamable operator (ostream&<<) or not.
    If a streamable operator is provided the variant_base will use this operator to create a string when
    calling toString(). If no operator is available this function will throw an exception.

    You can provide your own streamble operators for given type_t's only be defining operators before
    including "variant_base.h"
    *****************************************************************************************************/
    virtual       std::string     toString()         const = 0;

    virtual       bool            toBool()           const = 0;
    virtual       int8_t          toSignedInt8()     const = 0;
    virtual       uint8_t         toUnsignedInt8()   const = 0;
    virtual       int16_t         toSignedInt16()    const = 0;
    virtual       uint16_t        toUnsignedInt16()  const = 0;
    virtual       int32_t         toSignedInt32()    const = 0;
    virtual       uint32_t        toUnsignedInt32()  const = 0;
    virtual       int64_t         toSignedInt64()    const = 0;
    virtual       uint64_t        toUnsignedInt64()  const = 0;
    virtual       float           toFloat()          const = 0;
    virtual       double          toDouble()         const = 0;

#if defined (CPCPW_USE_QT5)
    virtual       QVariant             toQVariant() const = 0;
    static        variant_base*        fromQVariant(const QVariant& qvariant_base);
#endif
    /**
    ****************************************************************************************************
    NEW INTERFACES
    *****************************************************************************************************/
    // pBuffer must be preallocated
    virtual       size_t serialize(Endian endian, char* pBuffer) const = 0;

    // pBuffer will be created internal
    std::shared_ptr<char> serialize(Endian endian, size_t& nSize) const;

    virtual bool equal(const std::shared_ptr<variant_base> rhs) const = 0;

    template <typename value_type_t>
    bool equal(const value_type_t& rhs) const;

    template <typename value_type_t>
    bool equal(value_type_t&& rhs) const;

    template <typename type_t>
    bool convertTo(type_t& data);

    // SHARED
    template <typename type_t>
    static std::shared_ptr<variant_base> create(type_t&& data);

    template <typename type_t>
    static std::shared_ptr<variant_base> create(const type_t& data);

    template <typename type_t>
    static std::shared_ptr<variant_base> create(const type_t data[]);

    // RAW
    template <typename type_t>
    static variant_base* createRaw(type_t&& data);

    template <typename type_t>
    static variant_base* createRaw(const type_t& data);

    template <typename type_t>
    static variant_base* createRaw(const type_t data[]);

    // SHARED
    template <typename type_t>
    static std::shared_ptr<variant_t<type_t>> create_(const type_t& data);

    template <typename type_t>
    static std::shared_ptr<variant_t<type_t>> create_(type_t&& data);

    template <typename type_t>
    static std::shared_ptr<variant_t<type_t>> create_(const type_t data[]);

    // RAW
    template <typename type_t>
    static variant_t<type_t>* createRaw_(const type_t& data);

    template <typename type_t>
    static variant_t<type_t>* createRaw_(type_t&& data);

    template <typename type_t>
    static variant_t<type_t>* createRaw_(const type_t data[]);

  protected:
  };

  template <typename type_t>
  std::shared_ptr<variant_base> variant_base::create(type_t&& rvalue)
  {
    // factory function that uses perfect forwarding
    return std::shared_ptr<variant_base>(new variant_t<typename std::remove_reference<type_t>::type>(std::forward<type_t>(rvalue)));
  }

  template <typename type_t>
  variant_base* variant_base::createRaw(type_t&& rvalue)
  {
    // factory function that uses perfect forwarding
    return new variant_t<typename std::remove_reference<type_t>::type>(std::forward<type_t>(rvalue));
  }


  template <typename type_t>
  std::shared_ptr<variant_t<type_t>> variant_base::create_(type_t&& rvalue)
  {
    // factory function that uses perfect forwarding
    return std::shared_ptr<variant_t<type_t>>(new variant_t<typename std::remove_reference<type_t>::type>(std::forward<type_t>(rvalue)));
  }

  template <typename type_t>
  variant_t<type_t>* variant_base::createRaw_(type_t&& rvalue)
  {
    // factory function that uses perfect forwarding
    return new variant_t<typename std::remove_reference<type_t>::type>(std::forward<type_t>(rvalue));
  }

  template <typename type_t>
  std::shared_ptr<variant_base> variant_base::create(const type_t rvalue[])
  {
    // factory function that uses perfect forwarding
    return std::shared_ptr<variant_base>(new variant_t<const type_t*>(rvalue));
  }

  template <typename type_t>
  variant_base* variant_base::createRaw(const type_t rvalue[])
  {
    // factory function that uses perfect forwarding
    return new variant_t<const type_t*>(rvalue);
  }

  template <typename type_t>
  std::shared_ptr<variant_t<type_t>> variant_base::create_(const type_t rvalue[])
  {
    // factory function that uses perfect forwarding
    return std::shared_ptr<variant_t<type_t>>(new variant_t<const type_t*>(rvalue));
  }

  template <typename type_t>
  variant_t<type_t>* variant_base::createRaw_(const type_t rvalue[])
  {
    // factory function that uses perfect forwarding
    return new variant_t<const type_t*>(rvalue);
  }

  template <typename type_t>
  std::shared_ptr<variant_base> variant_base::create(const type_t& rvalue)
  {
    // factory function that uses perfect forwarding
    return std::shared_ptr<variant_base>(new variant_t<type_t>(rvalue));
  }

  template <typename type_t>
  variant_base* variant_base::createRaw(const type_t& rvalue)
  {
    // factory function that uses perfect forwarding
    return new variant_t<type_t>(rvalue);
  }

  template <typename type_t>
  std::shared_ptr<variant_t<type_t>> variant_base::create_(const type_t& rvalue)
  {
    // factory function that uses perfect forwarding
    return std::shared_ptr<variant_t<type_t>>(new variant_t<type_t>(rvalue));
  }

  template <typename type_t>
  variant_t<type_t>* variant_base::createRaw_(const type_t& rvalue)
  {
    // factory function that uses perfect forwarding
    return new variant_t<type_t>(rvalue);
  }

  template <typename type_t>
  bool variant_base::convertTo(type_t& data)
  {
    bool bSuccess = false;
    if (this->getTypeInfo() == typeid(type_t))
    {
      const variant_t<type_t>* typedPtr = reinterpret_cast<const variant_t<type_t>*>(this);
      data = typedPtr->data();
      bSuccess = true;
    }
    return bSuccess;
  }

  template <typename ptr_type_t>
  const ptr_type_t* variant_base::ptr() const
  {
    return reinterpret_cast<const ptr_type_t*>(this->address());
  }

  template <typename data_type_t>
  data_type_t variant_base::data()
  {
    return *this->ptr<data_type_t>();
  }

  template <typename data_type_t>
  data_type_t variant_base::value()
  {
    if (this->getTypeInfo() != typeid(data_type_t))
    {
      CPCPW_THROW(Exception, "variant_base-type (" << this->getTypeInfo().name() << ")" << "!= Template-type (" << typeid(data_type_t).name() << ")");
    }
    return *(this->ptr<data_type_t>());
  }

  template <typename data_type_t>
  const data_type_t variant_base::value() const
  {
    if (this->getTypeInfo() != typeid(data_type_t))
    {
      CPCPW_THROW(Exception, "variant_base-type (" << this->getTypeInfo().name() << ")" << "!= Template-type (" << typeid(data_type_t).name() << ")");
    }
    return *(this->ptr<data_type_t>());
  }

  template <typename value_type_t>
  bool variant_base::equal(const value_type_t& rhs) const
  {
    return this->equal(variant_base::create(rhs));
  }

  template <typename value_type_t>
  bool variant_base::equal(value_type_t&& rhs) const
  {
    return this->equal(variant_base::create(std::forward<value_type_t>(rhs)));
  }

  template <typename type_t>
  class variant_t : public variant_base
  {
  public:
    /**
    **************************************************************************
    variant_t<type_t> holds a member of type T, which is initialized by the
    constructor. However T has no restriction and in particular it may be a
    reference. If T is a value type, we would like to pass it by reference-to-
    const; but if T is a reference, writing const T& is illegal. So instead
    we use

    typedef typename std::add_reference<const T>::type argument_type;

    add_reference<T> returns const T&, as desired. If T is reference or
    reference-to-const, const T is T, and add_reference returns T, so
    argument type is again T
    *************************************************************************/
    // copy constructor
    variant_t(const type_t& lvalue);
    // move constructor
    variant_t(type_t&& rvalue);

    template<typename T>
    typename std::enable_if < !std::is_const<T>::value, void >::type
    moveImpl(T&& rvalue)
    {
      m_data = std::move(rvalue);
    }

    template<typename T>
    typename std::enable_if<std::is_const<T>::value, void>::type
    moveImpl(T&& /*rvalue*/)
    {
      CPCPW_THROW(Exception, "A variant holding const data can not be moved!");
    }

    virtual void move(type_t&& rvalue)
    {
      moveImpl<type_t>(std::forward<type_t>(rvalue));
    }


    virtual std::string toString() const;

    virtual size_t serialize(Endian endian, char* pBuffer) const;

    virtual bool equal(const std::shared_ptr<variant_base> rhs) const;

    virtual ~variant_t();

    virtual bool isArithmetic()     const
    {
      return std::is_arithmetic<type_t>::value;
    }
    virtual bool isArray()          const
    {
      return std::is_array<type_t>::value;
    }
    virtual bool isClass()          const
    {
      return std::is_class<type_t>::value;
    }
    virtual bool isFundamental()    const
    {
      return std::is_fundamental<type_t>::value;
    }
    virtual bool isIntegral()       const
    {
      return std::is_integral<type_t>::value;
    }
    virtual bool isFloatingPoint()  const
    {
      return std::is_floating_point<type_t>::value;
    }
    virtual bool isContainer()      const
    {
      return traits::is_container<type_t>::value;
    }
    virtual bool isStreamable()     const
    {
      return (traits::has_to_string<type_t>::value || traits::is_output_streamable<type_t>::value);
    }
    virtual bool isEqualComparable()const
    {
      return traits::is_equality_comparable<type_t>::value;
    }
    virtual bool isConst()          const
    {
      return std::is_const<type_t>::value;
    }
    virtual bool isSerializable()   const
    {
      return traits::is_serializable<type_t>::value;
    }

    template <typename convert_type>
    bool isConvertible() const
    {
      return std::is_convertible<type_t, convert_type>::value;
    }

    type_t        data() const;
    type_t*       ptr();

    virtual bool toBool()    const
    {
      return convert<bool>();
    }
    virtual int8_t  toSignedInt8()    const
    {
      return convert<int8_t>();
    }
    virtual uint8_t toUnsignedInt8()  const
    {
      return convert<uint8_t>();
    }
    virtual int16_t toSignedInt16()   const
    {
      return convert<int16_t>();
    }
    virtual uint16_t toUnsignedInt16() const
    {
      return convert<uint16_t>();
    }
    virtual int32_t toSignedInt32()   const
    {
      return convert<int32_t>();
    }
    virtual uint32_t toUnsignedInt32() const
    {
      return convert<uint32_t>();
    }
    virtual int64_t toSignedInt64()   const
    {
      return convert<int64_t>();
    }
    virtual uint64_t toUnsignedInt64() const
    {
      return convert<uint64_t>();
    }
    virtual float toFloat()         const
    {
      return convert<float>();
    }
    virtual double toDouble()        const
    {
      return convert<double>();
    }
#if defined (CPCPW_USE_QT5)
    virtual QVariant toQVariant()      const
    {
      return toQVariant_imp<type_t>();
    }
#endif

    virtual const std::type_info& getTypeInfo()                                   const;
    virtual       size_t          sizeofT()                                       const;

    virtual       const void*     address()                                       const;
    virtual             void*     address();
    virtual       const void*     addressConst();

  protected:
    /************************************************************
     * Trait-Function:
     * serialize_imp
     ************************************************************/
    template <typename U>
    typename std::enable_if < (traits::is_serializable<U>::value&& !std::is_same<const char*, U>::value)
#ifdef USE_OPENCV
    || std::is_same<typename std::remove_reference<U>::type, cv::Mat>::value
#endif
    , size_t >::type
    serialize_imp(Endian endian, char* pBuffer) const
    {
      return cpcpw::Memory::serializeT(endian, pBuffer, m_data);
    }

    template <typename U>
    typename std::enable_if < traits::is_serializable<U>::value&&
    std::is_same<const char*, U>::value, size_t >::type
    serialize_imp(Endian endian, char* pBuffer) const
    {
      return cpcpw::Memory::serializeT(endian, pBuffer, this->toString());
    }

    template <typename U>
    typename std::enable_if < !traits::is_serializable<U>::value
#ifdef USE_OPENCV
    && !std::is_same<typename std::remove_reference<U>::type, cv::Mat>::value
#endif
    , size_t >::type
    serialize_imp(Endian /*endian*/, char* /*pBuffer*/) const
    {
      CPCPW_THROW(cpcpw::Exception, "type '" << typeid(U).name() << "' can not be serialized ");
    }

    /************************************************************
     * Trait-Function:
     * sizeofT_imp
     ************************************************************/
    template <typename U>
    typename std::enable_if <
    (traits::is_serializable<U>::value&& !std::is_same<const char*, U>::value)
#ifdef USE_OPENCV
    || std::is_same<typename std::remove_reference<U>::type, cv::Mat>::value
#endif
    , size_t >::type
    sizeofT_imp() const
    {
      return Memory::sizeofT(m_data);
    }

    template <typename U>
    typename std::enable_if < traits::is_serializable<U>::value&&
    std::is_same<const char*, U>::value
    , size_t >::type
    sizeofT_imp() const
    {
      return Memory::sizeofT(std::string(m_data));
    }


    template <typename U>
    typename std::enable_if < !traits::is_serializable<U>::value
#ifdef USE_OPENCV
    && !std::is_same<typename std::remove_reference<U>::type, cv::Mat>::value
#endif
    , size_t >::type
    sizeofT_imp() const
    {
      CPCPW_THROW(Exception, "sizeT not available for this type: " << typeid(U).name());
    }

    /************************************************************/

    template<typename U>
    typename std::enable_if<std::is_const<U>::value, void*>::type
    address_imp()
    {
      CPCPW_THROW(Exception, "Can't get void* address pointer from const data!");
    }

    template<typename U>
    typename std::enable_if < !std::is_const<U>::value, void* >::type
    address_imp()
    {
      return (void*)&m_data;
    }

    // this is actual a metafunction that uses SFINAE to find out
    //  1. if type U is equality comparable ( operator== )
    template <typename U>
    typename std::enable_if<traits::is_equality_comparable<U>::value, bool>::type
    equal_imp(const std::shared_ptr<variant_base> rhs) const
    {
      bool bEqual = false;
      if (this->isIntegral() && rhs->isIntegral())
      {
        bEqual = this->toUnsignedInt64() == rhs->toUnsignedInt64();
      }
      else if (rhs->getTypeInfo() == typeid(type_t))
      {
        bEqual = (*reinterpret_cast<const type_t*>(rhs->addressConst()) == m_data);
      }
      return bEqual;
    }

    // Fallback implementation (SFINAE) for variant_base::equal if is either
    //  1. not equality comparable (operator==) or
    template <typename U>
    typename std::enable_if < !traits::is_equality_comparable<U>::value, bool >::type
    equal_imp(const std::shared_ptr<variant_base>) const
    {
      CPCPW_THROW(Exception, "no comparison operator available for type: " << typeid(type_t).name());
    }


    /**********************
     * TO STRING
     **********************/

#if defined (CPCPW_USE_QT5)
    template<typename U>
    typename std::enable_if<std::is_floating_point<U>::value, QVariant>::type
    toQVariant_imp() const
    {
      return QVariant::fromValue(m_data);
    }

    template<typename U>
    typename std::enable_if<std::is_integral<U>::value, QVariant>::type
    toQVariant_imp() const
    {
      return QVariant::fromValue(m_data);
    }

    template<typename U>
    typename std::enable_if<std::is_same<U, std::vector<uint8_t>>::value, QVariant>::type
        toQVariant_imp() const
    {
      QVariant qVar;
      if (!m_data.empty())
      {
        qVar = QVariant::fromValue(QByteArray((const char*) & (m_data[0]), static_cast<int32_t>(m_data.size())));
      }
      return qVar;
    }


    template<typename U>
    typename std::enable_if<std::is_same<U, std::string>::value, QVariant>::type
    toQVariant_imp() const
    {
      return QVariant(this->toString().c_str());
    }

    template<typename U>
    typename std::enable_if < !std::is_floating_point<U>::value&&
    !std::is_integral<U>::value&&
    !std::is_same<U, std::string>::value&&
    !std::is_same<U, std::vector<uint8_t>>::value, QVariant >::type
                                        toQVariant_imp() const
    {
      qDebug() << "no conversion to QVariant available for " << QString(typeid(U).name());
      CPCPW_THROW(Exception, "no conversion to QVariant available for " << typeid(U).name());
      return QVariant();
    }
#endif

    template<typename conversion_type>
    typename std::enable_if < std::is_convertible<type_t, conversion_type>::value&&
    !std::is_same<conversion_type, bool>::value, conversion_type >::type
    convert() const
    {
      return static_cast<conversion_type>(m_data);
    }

    template<typename conversion_type>
    typename std::enable_if < std::is_convertible<type_t, conversion_type>::value&&
    std::is_same<conversion_type, bool>::value&&
    std::is_fundamental<type_t>::value, conversion_type >::type
    convert() const
    {
      return m_data ? true : false;
    }

    template<typename conversion_type>
    typename std::enable_if < std::is_convertible<type_t, conversion_type>::value&&
    std::is_same<conversion_type, bool>::value&&
    !std::is_fundamental<type_t>::value, conversion_type >::type
    convert() const
    {
      CPCPW_THROW(Exception, typeid(type_t).name() << " is not convertible to " << typeid(conversion_type).name());
    }

    template<typename conversion_type>
    typename std::enable_if < std::is_convertible<type_t, conversion_type>::value&&
    !std::is_same<conversion_type, bool>::value&&
    !std::is_fundamental<type_t>::value&&
    !std::is_enum<type_t>::value, conversion_type >::type
    convert() const
    {
      return static_cast<conversion_type>(m_data);
    }

    template<typename conversion_type>
    typename std::enable_if < !std::is_convertible<type_t, conversion_type>::value, conversion_type >::type
    convert() const
    {
      CPCPW_THROW(Exception, typeid(type_t).name() << " is not convertible to " << typeid(conversion_type).name());
    }

    type_t m_data;
  };


  template<typename type_t>
  variant_t<type_t>::variant_t(const type_t&  lvalue)
    : m_data(lvalue)
  {}
  template<typename type_t>
  variant_t<type_t>::variant_t(type_t&&  rvalue)
    : m_data(std::move(rvalue))
  {}

  template<typename type_t>
  std::string variant_t<type_t>::toString() const
  {
    if (traits::is_ascii_serializable<type_t>::value)
    {
      return Memory::toStringT(m_data);
    }
    else
    {
      CPCPW_THROW(cpcpw::Exception, "type '" << typeid(type_t).name() << "' can not be serialized to string");
    }
  }

  /******************************************************************************************************************************************
  ******************************************************************************************************************************************/
  template<typename type_t>
  size_t variant_t<type_t>::serialize(Endian endian, char* pBuffer) const
  {
    return this->serialize_imp<type_t>(endian, pBuffer);
  }

  /******************************************************************************************************************************************
  ******************************************************************************************************************************************/

  template<typename type_t>
  bool variant_t<type_t>::equal(const std::shared_ptr<variant_base> rhs) const
  {
    return this->equal_imp<type_t>(rhs);
  }

  template<typename type_t>
  variant_t<type_t>::~variant_t()
  {}

  template<typename type_t>
  const void* variant_t<type_t>::address() const
  {
    return &m_data;
  }

  template<typename type_t>
  const void* variant_t<type_t>::addressConst()
  {
    return (const void*)&m_data;
  }

  template<typename type_t>
  void* variant_t<type_t>::address()
  {
    return this->address_imp<type_t>();
  }

  template<typename type_t>
  type_t variant_t<type_t>::data() const
  {
    return m_data;
  }

  template<typename type_t>
  type_t* variant_t<type_t>::ptr()
  {
    return &m_data;
  }

  template<typename type_t>
  size_t variant_t<type_t>::sizeofT() const
  {
    return this->sizeofT_imp<type_t>();
  }

  template<typename type_t>
  const std::type_info& variant_t<type_t>::getTypeInfo() const
  {
    return typeid(type_t);
  }

} // namespace cpcpw


#if defined CPCPW_USE_QT5
Q_DECLARE_METATYPE(cpcpw::variant);
#endif

#if defined MSVC
#pragma warning(pop)
#endif


#endif //__VARIANT_H__
