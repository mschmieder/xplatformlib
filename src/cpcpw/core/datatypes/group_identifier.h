#if !defined GROUPDISCRIMINATOR_H
# define GROUPDISCRIMINATOR_H

#include <cpcpw/core/core_config.h>
#include <string>

namespace cpcpw {
  /** @class group_identifier
  ******************************************************************************************************
  @brief
  @author schmieder (2012)
  \ingroup module_core_datatypes
  *****************************************************************************************************/
  template <typename type_t>
  class group_identifier
  {
  public:
    group_identifier()
      : m_bHasNext(false), m_bIsValid(false)
    {
    }

    group_identifier(const type_t& current, const type_t& next)
      : m_current(current), m_next(next), m_bHasNext(true), m_bIsValid(true)
    {
    }

    explicit group_identifier(const type_t& current)
      : m_current(current), m_bHasNext(false), m_bIsValid(true)
    {
    }

    type_t current() const
    {
      return m_current;
    }

    virtual type_t next() const
    {
      return m_next;
    }

    bool hasNext() const
    {
      return m_bHasNext;
    }

    bool isValid() const
    {
      return m_bIsValid;
    }

  private:
    type_t m_current;
    type_t m_next;
    bool   m_bHasNext;
    bool   m_bIsValid;
  };

  /**
  * @brief discriminate this function will discriminate (search for) a group in the
  *        given string 'strId' which is separated by strPattern.
  *
  * @param strId value that will be search for strPattern to separate groups
  * @param strPattern pattern which will separate groups
  * @return returns a group_identifier<std::string> defining all groups
  */
  CPCPW_CORE_DATATYPES_DECL
  group_identifier<std::string> discriminate(const std::string& strId, const std::string& strPattern);

  template <typename pattern_type_t>
  group_identifier<pattern_type_t> discriminate(const pattern_type_t& id)
  {
    return group_identifier<pattern_type_t>(id);
  }

  template <>
  CPCPW_CORE_DATATYPES_DECL group_identifier<std::string> discriminate<std::string>(const std::string& strId);
}
#endif //GROUPDISCRIMINATOR_H
