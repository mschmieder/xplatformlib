#ifndef _HDP_H_
# define _HDP_H_

#include <cpcpw/core/core_config.h>
#include <cpcpw/core/exception/Exception.h>
#include <cpcpw/core/datatypes/variant.h>
#include <cpcpw/core/datatypes/group_identifier.h>
#include <cpcpw/core/datatypes/hdp_iterator.h>

#include <map>
#include <vector>
#include <string>
#include <iterator>
#include <limits>

#include <regex>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

namespace cpcpw {
    /** @class hdp
    ******************************************************************************************************
    @author schmieder (2012)
    \ingroup module_core_datatypes
    *****************************************************************************************************/

    // forward declaration
    template<typename index_type_t>
    class hdp;

    class hdp_base
    {
    public:
        virtual ~hdp_base() {};

        /**
        **************************************************************************
        Access element operator
        If x matches the key of an element in the container, the function returns
        a reference to its mapped value. If x does not match the key of any element
        in the container, the function inserts a new element with that key and returns
        a reference to its mapped value. Notice that this always increases the map size
        by one, even if no mapped value is assigned to the element
        (the element is constructed using its default constructor).

        A call to this function is equivalent to:

        \code
        (*((this->insert(make_pair(x,T()))).first)).second
        \endcode

        Return value
        A reference to the element with a key value equal to x.
        T is the second template parameter, which defines the type of the mapped
        values in the container.
        *************************************************************************/
        template <typename index_type_t>
        variant& operator[](const index_type_t& idx);


        /**
        **************************************************************************
        The insert functions are used to insert a new element into the hdp.
        You can  give the insert function a shared pointer to a already instantiated Variant.

        If the identifier 'id' was already in the hdp, it will not be replaced.


        @param [in]  id            identifier
        @param [in]  ptrVariant    value to insert

        Return value
        returns a pair, with its member pair::first set to an iterator pointing to either
        the newly inserted element or to the element that already had its same key in the map.
        The pair::second element in the pair is set to true if a new element was inserted or
        false if an element with the same key existed.

        @return std::pair<typename hdp<index_type_t>::iterator,bool> to the removed element
        *************************************************************************/
        template <typename index_type_t>
        std::pair<typename hdp<index_type_t>::iterator, bool> insert(const index_type_t& id, const variant ptrVariant);

        template <typename index_type_t, typename value_type_t>
        std::pair<typename hdp<index_type_t>::iterator, bool> insert(const index_type_t& id, value_type_t&& value)
        {
          return this->insert(id, variant_base::crate(std::forward<value_type_t>(value)));
        }

        /**
        **************************************************************************
        The set function is used to insert a new element into the hdp.

        If the identifier 'id' was already in the hdp, it will be replaced,
        in contrast to the insert-functon, which will not replace the value.

        @param [in]  id            identifier
        @param [in]  ptrVariant    value to insert

        Return value
        returns a pair, with its member pair::first set to an iterator pointing to either
        the newly inserted element or to the element that already had its same key in the map.
        The pair::second element in the pair is set to true if a new element was inserted or
        false if an element with the same key existed.

        @return std::pair<typename hdp<index_type_t>::iterator,bool> to the inserted element
        *************************************************************************/
        template <typename index_type_t>
        std::pair<typename hdp<index_type_t>::iterator, bool> set(const index_type_t& id, const variant ptrVariant);

        /**
        **************************************************************************
        This 'get' function is actually a convenience function that builds on the
        generic 'get' function that returns a Variant pointer. This function has
        a bool return type and therefore checks wheter the element identifierd by
        'id' actually exists AND if the type of the data 'data_type_t' corresponds
        to the type stored inside the hdp

        @param [in]  id            identifier
        @param [in]  data          reference to typed data

        @return bool
        @retval true if value was found inside the storage AND value is of 'data_type_t'
        *************************************************************************/
        template <typename index_type_t, typename data_type_t>
        bool get(const index_type_t& id, data_type_t& data) const;

        /**
        **************************************************************************
        Generic 'get' function that returns the shared pointer to the element
        identified by 'id'. The return pointer will be 'empty' if value could not
        be found

        @param [in]  id            identifier

        @return variant
        @retval variant::empty() == true if value could not be found
        *************************************************************************/
        template <typename index_type_t>
        variant get(const index_type_t& id) const;

        /**
        **************************************************************************
        removes stored element identified by 'id'. Will return true if value was
        found and was removed successfully.

        @param [in]  id            identifier

        @return bool
        @retval true if element was found, false else
        *************************************************************************/
        template <typename index_type_t>
        bool erase(const index_type_t& id);

        /**
        **************************************************************************
        clears the complete storage
        *************************************************************************/
        virtual void clear() = 0;

        /**
        **************************************************************************
        Returns the number of elements inside the storage

        @return size_t
        *************************************************************************/
        virtual size_t size() const = 0;

        /**
        **************************************************************************
        STL-style iterators to the 'end' of the datastorage

        return iterator
        @retval iterator to begin
        *************************************************************************/
        template <typename index_type_t>
        typename hdp<index_type_t>::iterator begin();

        template <typename index_type_t>
        typename hdp<index_type_t>::const_iterator begin() const;

        /**
        **************************************************************************
        STL-style iterators to the 'begin' of the datastorage

        return iterator
        @retval iterator to begin
        *************************************************************************/
        template <typename index_type_t>
        typename hdp<index_type_t>::iterator end();

        template <typename index_type_t>
        typename hdp<index_type_t>::const_iterator end() const;

    protected:
        /**
        **************************************************************************
        convenience function that converts a given ptr to a typed
        hdp<index_type_t> ptr;

        return hdp<index_type_t>
        *************************************************************************/
        template <typename index_type_t>
        hdp<index_type_t>* cvtPtr();

        template <typename index_type_t>
        const hdp<index_type_t>* cvtPtr() const;

        hdp_base() {};
    };

    template <typename index_type_t>
    variant& hdp_base::operator[](const index_type_t& idx)
    {
        return (*this->cvtPtr<index_type_t>())[idx];
    }

    template <typename index_type_t>
    std::pair<typename hdp<index_type_t>::iterator, bool> hdp_base::insert(const index_type_t& id, const variant ptrVariant)
    {
        return this->cvtPtr<index_type_t>()->set(id, ptrVariant);
    }

    template <typename index_type_t>
    std::pair<typename hdp<index_type_t>::iterator, bool> hdp_base::set(const index_type_t& id, const variant ptrVariant)
    {
        return this->set(id, ptrVariant);
    }

    template <typename index_type_t, typename data_type_t>
    bool hdp_base::get(const index_type_t& id, data_type_t& data) const
    {
        return this->cvtPtr<index_type_t>()->get(id, data);
    }

    template <typename index_type_t>
    variant hdp_base::get(const index_type_t& id) const
    {
        return this->cvtPtr<index_type_t>()->get(id);
    }

    template <typename index_type_t>
    bool hdp_base::erase(const index_type_t& id)
    {
        return this->cvtPtr<index_type_t>()->erase(id);
    }

    template <typename index_type_t>
    hdp<index_type_t>* hdp_base::cvtPtr()
    {
        hdp<index_type_t>* modThisPtr = dynamic_cast<hdp<index_type_t>*>(this);
        if (!modThisPtr)
        {
            CPCPW_THROW(Exception, "Wrong template type " << typeid(index_type_t).name() );
        }
        return modThisPtr;
    }

    template <typename index_type_t>
    const hdp<index_type_t>* hdp_base::cvtPtr() const
    {
        const hdp<index_type_t>* modThisPtr = dynamic_cast<const hdp<index_type_t>*>(this);
        if (!modThisPtr)
        {
            CPCPW_THROW(Exception, "Wrong template type " << typeid(index_type_t).name() );
        }
        return modThisPtr;
    }

    template <typename index_type_t>
    typename hdp<index_type_t>::iterator hdp_base::begin()
    {
        return this->cvtPtr<index_type_t>()->begin();
    }

    template <typename index_type_t>
    typename hdp<index_type_t>::const_iterator hdp_base::begin() const
    {
        return this->cvtPtr<index_type_t>()->begin();
    }

    template <typename index_type_t>
    typename hdp<index_type_t>::iterator hdp_base::end()
    {
        return this->cvtPtr<index_type_t>()->end();
    }

    template <typename index_type_t>
    typename hdp<index_type_t>::const_iterator hdp_base::end() const
    {
        return this->cvtPtr<index_type_t>()->end();
    }


    template<typename index_type_t>
    class hdp : public hdp_base
    {
    public:
        typedef index_type_t index_type;

        /**
        **************************************************************************
        iterator typedefs for easier usage by external user
        *************************************************************************/
        typedef hdp_iterator
        <
        typename std::map<index_type_t, variant>::iterator,
                 index_type_t,
                 std::pair<index_type_t, variant>
                 >
                 iterator;

        typedef hdp_iterator
        <
        typename std::map<index_type_t, variant>::const_iterator,
                 index_type_t,
                 std::pair<index_type_t, variant> const
                 >
                 const_iterator;

        /**
        **************************************************************************
        General class constructor and destructor
        *************************************************************************/
        hdp();
        virtual ~hdp();

        /**
        ****************************************************************************************************
        Copy constructor
        *****************************************************************************************************/
        hdp(const hdp& rhs);

        /**
        ****************************************************************************************************
        Move constructor
        *****************************************************************************************************/
        hdp(hdp&& rhs);

        /**
        ****************************************************************************************************
        Assignment operator
        *****************************************************************************************************/
        hdp& operator=(const hdp& rhs);

        /**
        **************************************************************************
        Access element operator
        If x matches the key of an element in the container, the function returns
        a reference to its mapped value. If x does not match the key of any element
        in the container, the function inserts a new element with that key and returns
        a reference to its mapped value. Notice that this always increases the map size
        by one, even if no mapped value is assigned to the element
        (the element is constructed using its default constructor).

        A call to this function is equivalent to:
        (*((this->insert(make_pair(x,T()))).first)).second

        Return value
        A reference to the element with a key value equal to x.
        T is the second template parameter, which defines the type of the mapped
        values in the container.
        *************************************************************************/
        variant& operator[](const index_type_t& idx);

        bool operator==(hdp const& rhs) const;
        bool operator!=(hdp const& rhs) const;

        /**
        **************************************************************************
        The hdp container is extended by inserting a single new element
        This effectively increases the container size by the amount of elements inserted.
        Because hdp containers do not allow for duplicate key values, the insertion
        operation checks for each element inserted whether another element exists already
        in the container with the same key value, if so, the element is not inserted and
        its mapped value is not changed in any way.

        Internally, map containers keep all their elements sorted following the criterion
        specified by its comparison object on construction. Each element is inserted in its
        respective position following this ordering. Efficiency of this operation can be
        dramatically improved by providing the appropriate value in parameter position.

        Return value
        returns a pair, with its member pair::first set to an iterator pointing to either
        the newly inserted element or to the element that already had its same key in the map.
        The pair::second element in the pair is set to true if a new element was inserted or
        false if an element with the same key existed.

        @param [in]  id            identifier
        @param [in]  ptrVariant    value to insert

        @return std::pair<iterator,bool>
        *************************************************************************/
        std::pair<iterator, bool> insert(const index_type_t& id, const variant ptrVariant);

        template <typename value_type_t>
        std::pair<iterator, bool> insert(const index_type_t& id, value_type_t&& value){
            return this->insert(id,variant_base::create(std::forward<value_type_t>(value)));
        }

        /**
        **************************************************************************
        The set function is used to insert a new element into the hdp.

        If the identifier 'id' was already in the hdp, it will be replaced,
        in contrast to the insert-functon, which will not replace the value.

        @param [in]  id            identifier
        @param [in]  ptrVariant    value to insert

        @return std::pair<typename hdp<index_type_t>::iterator,bool> to the removed element
        @retval NULL if no element was removed, else shared pointer to element
        *************************************************************************/
        std::pair<iterator, bool> set(const index_type_t& id, const variant ptrVariant);


        template <typename data_type_t>
        bool get(const index_type_t& id, data_type_t& data) const;

        template <typename data_type_t>
        const data_type_t& get(const index_type_t& strId) const;

        variant get(const index_type_t& id) const;

        template<class... args_t>
        std::vector<variant> multi_get( args_t... args ) const
        {
            std::vector<variant> vecOut(sizeof...(args_t), nullptr);
            this->get_variadic_impl(vecOut, sizeof...(args_t), args...);
            return vecOut;
        }

        std::vector<index_type_t> getIds() const;

        bool erase(const index_type_t& id);
        bool erase(iterator& it);
        virtual void clear();

        bool empty() const {return m_pDataPool->empty();}

        /**
        **************************************************************************
        STL-style iterators to the 'end' of the datastorage

        return iterator
        @retval iterator to begin
        *************************************************************************/
        iterator       begin()         { return iterator(m_pDataPool->begin());      }
        const_iterator begin()   const { return const_iterator(m_pDataPool->begin()); }

        /**
        **************************************************************************
        STL-style iterators to the 'begin' of the datastorage

        return iterator
        @retval iterator to begin
        *************************************************************************/
        iterator       end()         { return iterator(m_pDataPool->end());      }
        const_iterator end()   const { return const_iterator(m_pDataPool->end());}

        /**
        **************************************************************************
        The find function searches for a given key inside the datastorage and
        returns an iterator to it if anything was found. If the element defined
        by 'key' was not found iterator will point to hdp<index_type_t>::end()

        return iterator
        @retval iterator to the found element. Points to end() if nothing found
        *************************************************************************/
        iterator       find(const index_type_t& key);
        const_iterator find(const index_type_t& key) const;


        virtual size_t size() const {return m_pDataPool->size();}

    protected:
        template<typename current_t>
        void get_variadic_impl(std::vector<variant>& vec, size_t nMax, current_t current) const
        {
            vec[ nMax - 1 ] = this->get(current);
        }


        template<typename current_t, class... rest_t>
        void get_variadic_impl(std::vector<variant>& vec, size_t nMax, current_t current, rest_t... rest ) const
        {
            vec[ nMax - sizeof...(rest_t) - 1 ] = this->get(current);
            this->get_variadic_impl(vec, nMax, rest...);
        }

        std::map<index_type_t, variant>* m_pDataPool;
    };

    template <typename index_type_t>
    hdp<index_type_t>::hdp()
        : m_pDataPool(nullptr)
    {
        m_pDataPool = new std::map<index_type_t, variant>();
    }

    template <typename index_type_t>
    hdp<index_type_t>::~hdp()
    {
        if (nullptr != m_pDataPool)
        {
            delete m_pDataPool;
            m_pDataPool = nullptr;
        }
    }

    // copy constructor
    template <typename index_type_t>
    hdp<index_type_t>::hdp(const hdp<index_type_t>& rhs)
    {
        m_pDataPool = new std::map<index_type_t, variant>();
        for (auto ds_elem : *rhs.m_pDataPool)
        {
            if ( ds_elem.second->getTypeInfo() == typeid(hdp<index_type_t>) )
            {
                m_pDataPool->insert( std::make_pair(ds_elem.first, variant_base::create( hdp<index_type_t>( ds_elem.second->template data<hdp<index_type_t>>()) )));
            }
            else
            {
                m_pDataPool->insert( std::make_pair(ds_elem.first, ds_elem.second) );
            }
        }
    }

    // move operator
    template <typename index_type_t>
    hdp<index_type_t>::hdp(hdp<index_type_t>&& rhs)
    {
        m_pDataPool     = rhs.m_pDataPool; // copy pointer only
        rhs.m_pDataPool = nullptr;
    }

    // assignment operator
    template <typename index_type_t>
    hdp<index_type_t>& hdp<index_type_t>::operator=(const hdp<index_type_t>& rhs)
    {
        // old pool must be deleted before assignment!
        delete m_pDataPool;

        m_pDataPool = new std::map<index_type_t, variant>();
        for (auto ds_elem : *rhs.m_pDataPool)
        {
            if ( ds_elem.second->getTypeInfo() == typeid(hdp<index_type_t>) )
            {
                m_pDataPool->insert( std::make_pair(ds_elem.first, variant_base::create( hdp<index_type_t>( ds_elem.second->template data<hdp<index_type_t>>()) )));
            }
            else
            {
                m_pDataPool->insert( std::make_pair(ds_elem.first, ds_elem.second) );
            }
        }

        return *this;
    }

    template <typename index_type_t>
    variant& hdp<index_type_t>::operator[](const index_type_t& idx)
    {
        // split group and value from index
        auto groupItem = discriminate<index_type_t>(idx);
        if (groupItem.isValid())
        {
            // check if current id can be found
            auto it = m_pDataPool->find( groupItem.current() );
            if (it != m_pDataPool->end()) // found
            {
                if ( !groupItem.hasNext() )
                {
                    return (*m_pDataPool)[idx];
                }
                else
                {
                    // check if current type is a storage type
                    if (it->second->getTypeInfo() == typeid( hdp<index_type_t> ))
                    {
                        return (*it->second->template ptr<hdp<index_type_t>>())[groupItem.next()];
                    }
                    else
                    {
                        CPCPW_THROW(Exception, "not a valid storage type: " << groupItem.current() );
                    }
                }
            }
        }
        else
        {
            CPCPW_THROW(Exception, "invalid id");
        }

        return (*m_pDataPool)[idx];// TODO: how can we store references to temporary variables so we don´t need to use "return" inside the function??? How to return "NULL-Ref"?
    }

    template <typename index_type_t>
    bool hdp<index_type_t>::operator==(hdp<index_type_t> const& rhs) const
    {
        bool bEqual = (rhs.size() == this->size());
        if (bEqual)
        {
            for (auto it = this->begin(); it != this->end(); ++it)
            {
                auto itRhs = rhs.find(it->first);
                if (itRhs == rhs.end() || !itRhs->second->equal(it->second) )
                {
                    bEqual = false;
                    break;
                }
            }
        }
        return bEqual;
    }

    template <typename index_type_t>
    bool hdp<index_type_t>::operator!=(hdp<index_type_t> const& rhs) const
    {
        return !(*this == rhs);
    }

    template <typename index_type_t>
    std::pair<typename hdp<index_type_t>::iterator, bool> hdp<index_type_t>::insert(const index_type_t& id, const variant ptrVariant)
    {
        std::pair<typename hdp<index_type_t>::iterator, bool> retIt;

        // split group and value from index
        auto groupItem = discriminate<index_type_t>(id);
        if (groupItem.isValid())
        {
            index_type_t currentGroup = groupItem.current();
            auto it = m_pDataPool->find( currentGroup );
            if (it != m_pDataPool->end()) // found
            {
                if ( !groupItem.hasNext() ) // no other group --> do not forward
                {
                    // insert value;
                    auto insertResult = m_pDataPool->insert( std::make_pair(currentGroup, ptrVariant) );
                    retIt = std::make_pair( iterator(insertResult.first), insertResult.second );
                }
                else // has inner storage
                {
                    if (it->second->getTypeInfo() == typeid( hdp<index_type_t> ))
                    {
                        retIt = it->second->template ptr<hdp<index_type_t>>()->insert( groupItem.next(), ptrVariant );
                    }
                    else
                    {
                        CPCPW_THROW(Exception, "not a valid storage type: " << currentGroup);
                    }
                }
            }
            else
            {
                if ( !groupItem.hasNext() )
                {
                    // insert value;
                    auto insertResult = m_pDataPool->insert( std::make_pair(currentGroup, ptrVariant) );
                    retIt = std::make_pair( iterator(insertResult.first), insertResult.second );
                }
                else
                {
                    // insert group
                    auto sptrGroup = variant_base::create_( hdp<index_type>() );
                    sptrGroup->ptr()->insert( groupItem.next(), ptrVariant );
                    retIt = this->insert( currentGroup, sptrGroup );
                }
            }
        }
        else
        {
            CPCPW_THROW(Exception, "invalid id");
        }

        return retIt;
    }

    template <typename index_type_t>
    std::pair<typename hdp<index_type_t>::iterator, bool> hdp<index_type_t>::set(const index_type_t& id, const variant ptrVariant)
    {
        std::pair<typename hdp<index_type_t>::iterator, bool> retIt;

        // split group and value from index
        auto groupItem = discriminate<index_type_t>(id);
        if (groupItem.isValid())
        {
            index_type_t currentGroup = groupItem.current();
            auto it = m_pDataPool->find(currentGroup);
            if (it != m_pDataPool->end()) // found. If so, do not insert a new element, but replace the old one.
            {
                if (!groupItem.hasNext()) // no other group --> do not forward
                {
                    // set value;
                    it->second = ptrVariant;
                    //set the second value to "true", as there will ALWAYS one element be inserted.
                    retIt = std::make_pair(hdp<index_type_t>::iterator(it), true);
                }
                else // has inner storage
                {
                    if (it->second->getTypeInfo() == typeid(hdp<index_type_t>))
                    {
                        retIt = it->second->template ptr<hdp<index_type_t>>()->set(groupItem.next(), ptrVariant);
                    }
                    else
                    {
                        CPCPW_THROW(Exception, "not a valid storage type: " << currentGroup);
                    }
                }
            }
            else  // if not found, insert a new element
            {
                if (!groupItem.hasNext())
                {
                    // insert value;
                    auto insertResult = m_pDataPool->insert(std::make_pair(currentGroup, ptrVariant));
                    retIt = std::make_pair(iterator(insertResult.first), insertResult.second);
                }
                else
                {
                    // insert group
                    auto sptrGroup = variant_base::create_(hdp<index_type>());
                    sptrGroup->ptr()->set(groupItem.next(), ptrVariant);
                    retIt = this->set(currentGroup, sptrGroup);
                }
            }
        }
        else
        {
            CPCPW_THROW(Exception, "invalid id");
        }

        return retIt;
    }

    template <typename index_type_t>
    variant hdp<index_type_t>::get(const index_type_t& id) const
    {
        // return value
        variant variantRetPtr;

        // split group and value from index
        auto groupItem = discriminate<index_type_t>(id);
        if (groupItem.isValid())
        {
            // check if current id can be found
            auto it = m_pDataPool->find( groupItem.current() );
            if (it != m_pDataPool->end()) // found
            {
                if ( !groupItem.hasNext() )
                {
                    variantRetPtr = it->second;
                }
                else
                {
                    // check if current type is a storage type
                    if (it->second->getTypeInfo() == typeid( hdp<index_type_t> ))
                    {
                        variantRetPtr = it->second->template ptr<hdp<index_type_t>>()->get( groupItem.next() );
                    }
                    else
                    {
                        CPCPW_THROW(Exception, "not a valid storage type: " << groupItem.current() );
                    }
                }
            } //TODO Why was this outcommented?
            /*else
            {
              CPCPW_THROW(Exception, "Could not find current id! id=" << groupItem.current());
            }*/
        }
        else
        {
            CPCPW_THROW(Exception, "invalid id");
        }

        return variantRetPtr;
    }

    template <typename index_type_t>
    template <typename data_type_t>
    bool hdp<index_type_t>::get(const index_type_t& id, data_type_t& data) const
    {
        bool bSuccess = false;
        variant variantRetPtr = this->get(id);
        if (variantRetPtr)
        {
            std::shared_ptr<cpcpw::variant_t<data_type_t>> ptrVariant = std::dynamic_pointer_cast<cpcpw::variant_t<data_type_t>>(variantRetPtr);
            if (ptrVariant)
            {
                data = ptrVariant->data();
                bSuccess = true;
            }
        }
        return bSuccess;
    }

    template <typename index_type_t>
    std::vector<index_type_t> hdp<index_type_t>::getIds() const
    {
        typename std::vector<index_type_t> idVec;
        typename std::map<index_type_t, variant>::const_iterator it = m_pDataPool->begin();
        typename std::map<index_type_t, variant>::const_iterator itEnd = m_pDataPool->end();

        for (; it != itEnd; ++it)
        {
            idVec.push_back(it->first);
        }

        return idVec;
    }

    template <typename index_type_t>
    bool hdp<index_type_t>::erase(const index_type_t& id)
    {
        bool bSuccess = true;

        // split group and value from index
        auto groupItem = discriminate<index_type_t>(id);
        if (groupItem.isValid())
        {
            index_type_t currentGroup = groupItem.current();
            auto it = m_pDataPool->find( currentGroup );
            if (it != m_pDataPool->end()) // found
            {
                if ( !groupItem.hasNext() ) // no other group --> do not forward
                {
                    m_pDataPool->erase(it);
                }
                else // has inner storage
                {
                    if (it->second->getTypeInfo() == typeid( hdp<index_type_t> ))
                    {
                        bSuccess = it->second->template ptr<hdp<index_type_t>>()->erase(groupItem.next());
                    }
                    else
                    {
                        CPCPW_THROW(Exception, "not a valid storage type: " << currentGroup);
                    }
                }
            }
        }
        else
        {
            CPCPW_THROW(Exception, "invalid id");
        }

        return bSuccess;
    }

    template <typename index_type_t>
    template <typename data_type_t>
    const data_type_t& hdp<index_type_t>::get(const index_type_t& strId) const
    {
        variant variantRetPtr = this->get(strId);
        if (variantRetPtr)
        {
            if (variantRetPtr->getTypeInfo() != typeid(data_type_t))
            {
                CPCPW_THROW(Exception, "type does not match: " << variantRetPtr->getTypeInfo().name() << " != " << typeid(data_type_t).name() );
            }
        }
        else
        {
            CPCPW_THROW(Exception, "Could not find parameter " << strId);
        }
        return *(variantRetPtr->ptr<data_type_t>());
    }

    template <typename index_type_t>
    void hdp<index_type_t>::clear()
    {
        m_pDataPool->clear();
    }

    template <typename index_type_t>
    typename hdp<index_type_t>::iterator hdp<index_type_t>::find(const index_type_t& key)
    {
        return hdp<index_type_t>::iterator(m_pDataPool->find(key));
    }

    template <typename index_type_t>
    typename hdp<index_type_t>::const_iterator hdp<index_type_t>::find(const index_type_t& key) const
    {
        return hdp<index_type_t>::const_iterator(m_pDataPool->find(key));
    }

    template <typename index_type_t>
    bool hdp<index_type_t>::erase( typename hdp<index_type_t>::iterator& it )
    {
        return this->erase(it->first);
    }
} // namespace cpcpw



#endif //_DATASTORAGE_H_
