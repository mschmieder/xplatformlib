#include <cpcpw/core/core_config.h>
#include <cpcpw/core/datatypes/hdp.h>
#include <limits>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gtest/gtest.h>

using namespace cpcpw;

static uint32_t gIdx = 0U;

template <typename type_t>
type_t createValue()
{
  return static_cast<type_t>(gIdx++);
}

template <typename type_t>
std::vector<type_t> createVector()
{
  std::vector<type_t> vec;
  for(size_t idx = 0U; idx < 10U; idx++)
  {
    vec.push_back( createValue<type_t>() );
  }

  return vec;
}

template <typename type_t>
std::list<type_t> createList()
{
  std::list<type_t> lst;
  for(size_t idx = 0U; idx < 10U; idx++)
  {
    lst.push_back( createValue<type_t>() );
  }
  return lst;
}

template <>
bool createValue<bool>()
{
  return true;
}

template <>
std::string createValue<std::string>()
{
  std::stringstream sstr;
  sstr<<createValue<uint32_t>();
  return sstr.str();
}


// Then we define a test fixture class template.
template <class type_t>
class HdpTest : public testing::Test
{
 protected:
  // The ctor calls the factory function to create a prime table
  // implemented by T.
  HdpTest()
  {
    gIdx = 0U;
  }

  virtual ~HdpTest()
  {
  }

  // Note that we test an implementation via the base interface
  // instead of the actual implementation class.  This is important
  // for keeping the tests close to the real world scenario, where the
  // implementation is invoked via the base interface.  It avoids
  // got-yas where the implementation class has a method that shadows
  // a method with the same name (but slightly different argument
  // types) in the base interface, for example.
public:
  hdp<type_t> storage;
};

using testing::Types;
typedef Types
  <
  int8_t,
  uint8_t,
  int16_t,
  uint16_t,
  int32_t,
  uint32_t,
  int64_t,
  uint64_t,
  float,
  double,
  std::string
  > IndexTypes;

TYPED_TEST_CASE(HdpTest, IndexTypes);

TEST(HdpTest, AssignementOperator)
{
  hdp<std::string> storage;

  EXPECT_NO_THROW( storage.insert("Artist.Name",variant_base::create(std::string("Michael Jackson")) ) );
  EXPECT_NO_THROW( storage.insert("Artist.Adress.Road",variant_base::create(std::string("Beverly Hills Strt.")) ) );

  cpcpw::variant ptrName;
  cpcpw::variant ptrAdress;
  cpcpw::variant ptrAdressRoad;

  EXPECT_NO_THROW( ptrName       = storage.get("Artist.Name")        );
  EXPECT_NO_THROW( ptrAdress     = storage.get("Artist.Adress")      );
  EXPECT_NO_THROW( ptrAdressRoad = storage.get("Artist.Adress.Road") );

  EXPECT_TRUE( static_cast<bool>(ptrName)       );
  EXPECT_TRUE( static_cast<bool>(ptrAdress)     );
  EXPECT_TRUE( static_cast<bool>(ptrAdressRoad) );

  {
    hdp<std::string> storagecp;
    EXPECT_NO_THROW( storagecp = storage );
    EXPECT_TRUE( storagecp == storage );
  }

  EXPECT_NO_THROW( ptrName       = storage.get("Artist.Name")        );
  EXPECT_NO_THROW( ptrAdress     = storage.get("Artist.Adress")      );
  EXPECT_NO_THROW( ptrAdressRoad = storage.get("Artist.Adress.Road") );

  EXPECT_TRUE( static_cast<bool>(ptrName)       );
  EXPECT_TRUE( static_cast<bool>(ptrAdress)     );
  EXPECT_TRUE( static_cast<bool>(ptrAdressRoad) );
}

TEST(HdpTest, GroupInsertTest)
{
  hdp<std::string> storage;

  storage.insert("Artist.Name",       variant_base::create(std::string("Michael Jackson")) );
  storage.insert("Artist.Adress.Road",variant_base::create(std::string("Beverly Hills Strt.")) );

  cpcpw::variant ptrName       = storage.get("Artist.Name");
  cpcpw::variant ptrAdress     = storage.get("Artist.Adress");
  cpcpw::variant ptrAdressRoad = storage.get("Artist.Adress.Road");
}

//Test that shall prove that insert DOES replace existing values
TEST(HdpTest, GroupSetAndTestIfReplaceWorks)
{
  hdp<std::string> storage;
  std::string strArtistName;
  std::string strRoadName;

  storage.set("Artist.Name", variant_base::create(std::string("Michael Jackson")));
  storage.set("Artist.Adress.Road", variant_base::create(std::string("Beverly Hills Strt.")));

  storage.get("Artist.Name", strArtistName);
  storage.get("Artist.Adress.Road", strRoadName);
  EXPECT_EQ(strArtistName, "Michael Jackson");
  EXPECT_EQ(strRoadName, "Beverly Hills Strt.");

  storage.set("Artist.Name", variant_base::create(std::string("Michael Jackson")));
  storage.set("Artist.Adress.Road", variant_base::create(std::string("New Strt.")));

  storage.get("Artist.Name", strArtistName);
  storage.get("Artist.Adress.Road", strRoadName);
  EXPECT_EQ(strArtistName, "Michael Jackson");
  EXPECT_EQ(strRoadName, "New Strt.");
  EXPECT_NE(strRoadName, "Beverly Hills Strt.");

}

//Test that shall prove that insert does NOT replace existing values
TEST(HdpTest, GroupInsertAndTestIfReplaceNotHappens)
{
  hdp<std::string> storage;
  std::string strArtistName;
  std::string strRoadName;

  storage.insert("Artist.Name", variant_base::create(std::string("Michael Jackson")));
  storage.insert("Artist.Adress.Road", variant_base::create(std::string("Beverly Hills Strt.")));

  storage.get("Artist.Name", strArtistName);
  storage.get("Artist.Adress.Road", strRoadName);
  EXPECT_EQ(strArtistName, "Michael Jackson");
  EXPECT_EQ(strRoadName, "Beverly Hills Strt.");

  storage.insert("Artist.Name", variant_base::create(std::string("Michael Jackson")));
  storage.insert("Artist.Adress.Road", variant_base::create(std::string("New Strt.")));

  storage.get("Artist.Name", strArtistName);
  storage.get("Artist.Adress.Road", strRoadName);
  EXPECT_EQ(strRoadName, "Beverly Hills Strt.");

}

TEST(HdpTest, EraseAllTest)
{
  hdp<std::string> storage;

  storage.insert("Artist.Name",       variant_base::create(std::string("Michael Jackson")) );
  storage.insert("Artist.Adress.Road",variant_base::create(std::string("Beverly Hills Strt.")) );

  EXPECT_TRUE( storage.erase("Artist") );
}

TEST(HdpTest, GroupEraseTest)
{
  hdp<std::string> storage;

  storage.insert("Artist.Name",       variant_base::create(std::string("Michael Jackson")) );
  storage.insert("Artist.Adress.Road",variant_base::create(std::string("Beverly Hills Strt.")) );

  //erase Adress-node
  EXPECT_TRUE( storage.erase("Artist.Adress") );
  // check erase result
  EXPECT_TRUE ( std::string("Michael Jackson") == storage.get("Artist.Name").get()->value<std::string>() );
  EXPECT_TRUE ( nullptr == storage.get("Artist.Adress") );
  EXPECT_TRUE ( nullptr == storage.get("Artist.Adress.Road") );

  //erase Name-node
  EXPECT_TRUE( storage.erase("Artist.Adress.Road") ); // expect no error if element not exists
  EXPECT_TRUE( storage.erase("Artist.Road") );        // expect no error if element not exists
  EXPECT_TRUE( storage.erase("Artist.Name") );
  // check erase result
  EXPECT_TRUE ( nullptr == storage.get("Artist.Name") );

  EXPECT_TRUE( storage.erase("Artist") );  // expect no error if element empty
}


TEST(HdpTest, GroupArrayOperatorTest)
{
  hdp<std::string> storage;

  storage.insert("Artist.Name",       variant_base::create(std::string("Michael Jackson")) );
  storage.insert("Artist.Adress.Road",variant_base::create(std::string("Beverly Hills Strt.")) );

  //check root node
  ASSERT_TRUE( nullptr != storage["Artist"] );

  //check first node
  ASSERT_TRUE( nullptr != storage["Artist.Name"] );
  EXPECT_TRUE ( std::string("Michael Jackson") == storage["Artist.Name"].get()->value<std::string>() );

  //check second node
  ASSERT_TRUE( nullptr != storage["Artist.Adress"] );
  ASSERT_TRUE( nullptr != storage["Artist.Adress.Road"] );
  EXPECT_TRUE ( std::string("Beverly Hills Strt.") == storage["Artist.Adress.Road"].get()->value<std::string>() );
}

TEST(HdpTest, CopyConstruction)
{
  hdp<std::string> storage;

  storage.insert("Artist.Name",       variant_base::create(std::string("Michael Jackson")) );
  storage.insert("Artist.Adress.Road",variant_base::create(std::string("Beverly Hills Strt.")) );

  hdp<std::string> storageCp(storage);
  ASSERT_TRUE( storageCp.get("Artist.Name")->equal(storage.get("Artist.Name")) );

  storage.erase("Artist.Name");
  storage.set("Artist.Name",variant_base::create(std::string("John Lennon")) );
  ASSERT_FALSE( storageCp.get("Artist.Name")->equal(storage.get("Artist.Name")) );

}


TYPED_TEST(HdpTest, InsertionTests)
{
  hdp<TypeParam>& tStorage = HdpTest<TypeParam>::storage;

  EXPECT_TRUE( tStorage.empty() );
  EXPECT_EQ( tStorage.begin(), tStorage.end() );

  int8_t      s8Value  = createValue< int8_t      >();
  uint8_t     u8Value  = createValue< uint8_t     >();
  int16_t     s16Value = createValue< int16_t     >();
  uint16_t    u16Value = createValue< uint16_t    >();
  int32_t     s32Value = createValue< int32_t     >();
  uint32_t    u32Value = createValue< uint32_t    >();
  int64_t     s64Value = createValue< int64_t     >();
  uint64_t    u64Value = createValue< uint64_t    >();
  float       fValue   = createValue< float       >();
  double      dValue   = createValue< double      >();
  std::string strValue = createValue< std::string >();

  std::vector<int8_t     > vs8Value  = createVector< int8_t      >();
  std::vector<uint8_t    > vu8Value  = createVector< uint8_t     >();
  std::vector<int16_t    > vs16Value = createVector< int16_t     >();
  std::vector<uint16_t   > vu16Value = createVector< uint16_t    >();
  std::vector<int32_t    > vs32Value = createVector< int32_t     >();
  std::vector<uint32_t   > vu32Value = createVector< uint32_t    >();
  std::vector<int64_t    > vs64Value = createVector< int64_t     >();
  std::vector<uint64_t   > vu64Value = createVector< uint64_t    >();
  std::vector<float      > vfValue   = createVector< float       >();
  std::vector<double     > vdValue   = createVector< double      >();
  std::vector<std::string> vstrValue = createVector< std::string >();

  std::list<int8_t     > ls8Value  = createList< int8_t     >();
  std::list<uint8_t    > lu8Value  = createList< uint8_t    >();
  std::list<int16_t    > ls16Value = createList< int16_t    >();
  std::list<uint16_t   > lu16Value = createList< uint16_t   >();
  std::list<int32_t    > ls32Value = createList< int32_t    >();
  std::list<uint32_t   > lu32Value = createList< uint32_t   >();
  std::list<int64_t    > ls64Value = createList< int64_t    >();
  std::list<uint64_t   > lu64Value = createList< uint64_t   >();
  std::list<float      > lfValue   = createList< float      >();
  std::list<double     > ldValue   = createList< double     >();
  std::list<std::string> lstrValue = createList< std::string>();

  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(s8Value )  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(u8Value )  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(s16Value)  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(u16Value)  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(s32Value)  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(u32Value)  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(s64Value)  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(u64Value)  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(fValue  )  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(dValue  )  ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(strValue)  ) );

  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vs8Value ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vu8Value ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vs16Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vu16Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vs32Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vu32Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vs64Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vu64Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vfValue  ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vdValue  ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(vstrValue) ) );

  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(ls8Value ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(lu8Value ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(ls16Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(lu16Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(ls32Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(lu32Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(ls64Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(lu64Value) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(lfValue  ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(ldValue  ) ) );
  EXPECT_NO_THROW( tStorage.insert( createValue<TypeParam>() , variant_base::create(lstrValue) ) );

  EXPECT_TRUE( !tStorage.empty() );

  typename hdp<TypeParam>::iterator it;

  size_t uiCnt = 0U;
  for(it = tStorage.begin(); it != tStorage.end(); ++it)
  {
    EXPECT_TRUE( static_cast<bool>(it->second) );
    uiCnt++;
  }

  EXPECT_EQ( uiCnt, tStorage.size() );
}
