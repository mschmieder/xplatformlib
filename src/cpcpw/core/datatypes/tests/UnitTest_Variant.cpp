#include <cpcpw/core/datatypes/variant.h>
#include <limits>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#if defined CPCPW_USE_QT
  #inc5lude <QString>
#endif

#include <gtest/gtest.h>


#if defined min
  #undef min
#endif

#if defined max
  #undef max
#endif


using namespace cpcpw;

template <typename type_t>
void _createDefaultValue(type_t& data)
{
  srand ( static_cast<uint32_t>(time(NULL)) );
  double dRandValue = static_cast<double>(rand()) / static_cast<double>(RAND_MAX) * static_cast<double>( double(std::numeric_limits<type_t>::max()) - std::numeric_limits<type_t>::min() ) + static_cast<double>(std::numeric_limits<type_t>::min());
  data = static_cast<type_t>(dRandValue);
}

#if defined CPCPW_USE_QT
void _5createDefaultValue(QString& str)
{
  str = "Hello World";
}
#endif

template <typename type_t>
void _createDefaultValue(std::vector<type_t>& data)
{
  data.clear();
  for (size_t idx = 0; idx < 10; idx++)
  {
    type_t value;
    _createDefaultValue(value);
    data.push_back( value );
  }
}

template <typename type_t>
void _createDefaultValue(std::list<type_t>& data)
{
  data.clear();
  for (size_t idx = 0U; idx < 10U; idx++)
  {
    type_t value;
    _createDefaultValue(value);
    data.push_back( value );
  }
}

template <>
void _createDefaultValue<bool>(bool& data)
{
  data = true;
}

template <>
void _createDefaultValue<std::string>(std::string& data)
{
  data = std::string("HELLO WORLD \n Hello World");
}

// Then we define a test fixture class template.
template <class T>
class VariantTest : public testing::Test
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantTest()
    {
      _createDefaultValue(value);
    }

    virtual ~VariantTest()
    {
    }

    // Note that we test an implementation via the base interface
    // instead of the actual implementation class.  This is important
    // for keeping the tests close to the real world scenario, where the
    // implementation is invoked via the base interface.  It avoids
    // got-yas where the implementation class has a method that shadows
    // a method with the same name (but slightly different argument
    // types) in the base interface, for example.
  public:
    virtual T getValue() {return value;}

    T value;
};

template <class T>
class VariantStreamableTest : public VariantTest<T>
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantStreamableTest() : VariantTest<T>()
    {
    }
};

template <class T>
class VariantSerializeTest: public VariantTest<T>
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantSerializeTest() : VariantTest<T>()
    {
    }
};

template <class T>
class VariantSerializeMatTest: public VariantTest<T>
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantSerializeMatTest() : VariantTest<T>()
    {
    }
};

template <class T>
class VariantEqualityTest : public VariantTest<T>
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantEqualityTest() : VariantTest<T>()
    {}
};

template <class T>
class VariantToBoolTest : public VariantTest<T>
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantToBoolTest() : VariantTest<T>()
    {}
};

template <class T>
class VariantToStringTest : public VariantTest<T>
{
  protected:
    // The ctor calls the factory function to create a prime table
    // implemented by T.
    VariantToStringTest() : VariantTest<T>()
    {}
};

using testing::Types;
typedef Types
<
int8_t,
uint8_t,
int16_t,
uint16_t,
int32_t,
uint32_t,
int64_t,
uint64_t,
float,
double,
bool,
std::string,
std::vector<int8_t      >,
std::vector<uint8_t     >,
std::vector<int16_t     >,
std::vector<uint16_t    >,
std::vector<int32_t     >,
std::vector<uint32_t    >,
std::vector<int64_t     >,
std::vector<uint64_t    >,
std::vector<float       >,
std::vector<double      >,
std::vector<bool        >,
std::vector<std::string >,
std::list<int8_t      >,
std::list<uint8_t     >,
std::list<int16_t     >,
std::list<uint16_t    >,
std::list<int32_t     >,
std::list<uint32_t    >,
std::list<int64_t     >,
std::list<uint64_t    >,
std::list<float       >,
std::list<double      >,
std::list<bool        >,
std::list<std::string >
> DataTypes;

typedef Types
<
int8_t,
uint8_t,
int16_t,
uint16_t,
int32_t,
uint32_t,
int64_t,
uint64_t,
float,
double,
bool,
std::string
> StreamableTypes;

typedef Types
<
int8_t,
uint8_t,
int16_t,
uint16_t,
int32_t,
uint32_t,
int64_t,
uint64_t,
float,
double,
bool
> IntegralTypes;

typedef Types
<
int8_t,
uint8_t,
int16_t,
uint16_t,
int32_t,
uint32_t,
int64_t,
uint64_t,
float,
double,
bool,
std::string,
std::vector<int8_t      >,
std::vector<uint8_t     >,
std::vector<int16_t     >,
std::vector<uint16_t    >,
std::vector<int32_t     >,
std::vector<uint32_t    >,
std::vector<int64_t     >,
std::vector<uint64_t    >,
std::vector<float       >,
std::vector<double      >,
std::vector<bool        >,
std::vector<std::string >,
std::list<int8_t      >,
std::list<uint8_t     >,
std::list<int16_t     >,
std::list<uint16_t    >,
std::list<int32_t     >,
std::list<uint32_t    >,
std::list<int64_t     >,
std::list<uint64_t    >,
std::list<float       >,
std::list<double      >,
std::list<bool        >,
std::list<std::string >
> SerializableTypes;

typedef Types
<
int8_t
, uint8_t
, int16_t
, uint16_t
, int32_t
, uint32_t
, int64_t
, uint64_t
, float
, double
, bool
, std::string
#if defined CPCPW_USE_QT5
  ,QString
#endif
> AsciiSerializableTypes;

TYPED_TEST_CASE(VariantTest, DataTypes);
TYPED_TEST_CASE(VariantStreamableTest, StreamableTypes);
TYPED_TEST_CASE(VariantSerializeTest, SerializableTypes);
TYPED_TEST_CASE(VariantEqualityTest, StreamableTypes);
TYPED_TEST_CASE(VariantToBoolTest, IntegralTypes);
TYPED_TEST_CASE(VariantToStringTest, AsciiSerializableTypes);

enum MyEnum
{
  enum_a,
  enum_b
};

enum MyTypedEnum : int32_t
{
  typed_enum_a = 0,
  typed_enum_b
};

enum MyTEnum : size_t // this will cause msvc12 to emmit an error when using traits::is_output_streamable
{
  t_enum_a = 0,
  t_enum_b
};

TEST(VariantTests, EnumCreation)
{
  auto pVar1 = variant_base::create( enum_a );
  auto pVar2 = variant_base::create( typed_enum_a );
  auto pVar3 = variant_base::create( t_enum_a );
  ASSERT_EQ(pVar1->value<MyEnum>(), enum_a);
  ASSERT_EQ(pVar2->value<MyTypedEnum>(), typed_enum_a);
  ASSERT_EQ(pVar3->value<MyTEnum>(), t_enum_a);
}

TYPED_TEST(VariantToBoolTest, ToBoolTestCases)
{
  cpcpw::variant pVar;
  // create a variant
  ASSERT_NO_THROW(pVar = variant_base::create(VariantToBoolTest<TypeParam>::value));
  ASSERT_NO_THROW(pVar->toBool());
}

TYPED_TEST(VariantEqualityTest, EqualityLValueReference)
{
  cpcpw::variant pVar;
  // create a variant
  ASSERT_NO_THROW(pVar = variant_base::create(VariantEqualityTest<TypeParam>::value));
  ASSERT_NE(nullptr, pVar.get());
  // Check if equal
  EXPECT_EQ( VariantEqualityTest<TypeParam>::value, pVar->value<TypeParam>() );
  EXPECT_TRUE( pVar->equal(VariantEqualityTest<TypeParam>::value) );
  EXPECT_TRUE( pVar->equal(pVar) );
}

TYPED_TEST(VariantEqualityTest, EqualityRValueReference)
{
  cpcpw::variant pVar;
  // create a variant
  ASSERT_NO_THROW(pVar = variant_base::create(VariantEqualityTest<TypeParam>::value));
  ASSERT_NE( nullptr, pVar.get() );
  // Check if equal
  EXPECT_EQ(VariantEqualityTest<TypeParam>::value, pVar->value<TypeParam>());
  TypeParam valuecp = VariantEqualityTest<TypeParam>::value;
  EXPECT_TRUE(pVar->equal(std::move(valuecp)));
  EXPECT_TRUE(pVar->equal(variant_base::create(VariantEqualityTest<TypeParam>::value)));
}


TYPED_TEST(VariantStreamableTest, ToStringTest)
{
  cpcpw::variant pVar;
  // create a variant
  ASSERT_NO_THROW( pVar = variant_base::create(VariantStreamableTest<TypeParam>::value) );

  std::string variant_string;
  ASSERT_NO_THROW( variant_string = pVar->toString() );
}

TYPED_TEST(VariantTest, CreationTest)
{
  cpcpw::variant pVar = nullptr;
  // create a variant
  ASSERT_NO_THROW( pVar = variant_base::create(VariantTest<TypeParam>::value) );
  // check if pointer is ok
  ASSERT_TRUE( static_cast<bool>(pVar) );
  // check if correct type was created
  EXPECT_EQ( typeid(TypeParam), pVar->getTypeInfo() );

  // check conversion
  TypeParam cvt_value = TypeParam();
  EXPECT_TRUE( pVar->convertTo(cvt_value) );
  // check if equal to reference
  EXPECT_EQ( VariantTest<TypeParam>::value, cvt_value );
  EXPECT_EQ( VariantTest<TypeParam>::value, *(pVar->ptr<TypeParam>()) );
  EXPECT_EQ( VariantTest<TypeParam>::value, pVar->data<TypeParam>()   );
  // test serialization
  size_t nBufferSize(0);
  std::shared_ptr<char> pBuffer;
  ASSERT_NO_THROW(pBuffer = pVar->serialize(Endian::no_swap, nBufferSize));
  EXPECT_TRUE(pBuffer != nullptr);
  EXPECT_TRUE(pBuffer.get() != nullptr);
  EXPECT_TRUE(nBufferSize != 0);
}

TYPED_TEST(VariantTest, SizeOfT)
{
  cpcpw::variant pVar = nullptr;
  // create a variant
  ASSERT_NO_THROW( pVar = variant_base::create(VariantTest<TypeParam>::value) );
  ASSERT_NO_THROW( pVar->sizeofT() );
  EXPECT_NE(0U, pVar->sizeofT() );
}

TYPED_TEST(VariantTest, CreationConstTest)
{
  cpcpw::variant pVar = nullptr;
  // create a variant
  ASSERT_NO_THROW(pVar = variant_base::create<const TypeParam>(VariantTest<TypeParam>::value));
  // check if pointer is ok
  ASSERT_TRUE(static_cast<bool>(pVar));
  // check if correct type was created
  EXPECT_EQ(typeid(TypeParam), pVar->getTypeInfo());

  // expect THROW try to get non const type pointer
  EXPECT_THROW(pVar->ptr<TypeParam>(), Exception);
  EXPECT_THROW(pVar->data<TypeParam>(), Exception);
  EXPECT_THROW(pVar->value<TypeParam>(), Exception);
  // check conversion
  TypeParam tCompare = TypeParam();
  EXPECT_TRUE(pVar->convertTo(tCompare));
  // check if equal to reference
  EXPECT_EQ( VariantTest<TypeParam>::value, tCompare );
  EXPECT_EQ(VariantTest<TypeParam>::value, *(pVar->ptr<const TypeParam>()));
  EXPECT_EQ(VariantTest<TypeParam>::value, pVar->data<const TypeParam>());
  // test serialization
  size_t nBufferSize(0);
  std::shared_ptr<char> pBuffer;
  ASSERT_NO_THROW(pBuffer = pVar->serialize(Endian::no_swap, nBufferSize));
  EXPECT_TRUE(pBuffer != nullptr);
  EXPECT_TRUE(pBuffer.get() != nullptr);
  EXPECT_TRUE(nBufferSize != 0);
}
//
//TYPED_TEST(VariantToStringTest, ToStringTypes)
//{
//  cpcpw::variant pVar;
//  // create a variant
//  TypeParam tValue;
//  _createDefaultValue(tValue); 
//
//  ASSERT_NO_THROW( pVar = variant_base::create(tValue) );
//  std::string str = "";
//  ASSERT_NO_THROW( str = pVar->toString());
//  EXPECT_TRUE(!str.empty());
//}