#include "group_identifier.h"
#include <cpcpw/core/exception/Exception.h>
#include <cpcpw/core/MemoryLeakDetection.h>
#define new DEBUG_NEW_LEAKAGE


namespace cpcpw {
  group_identifier<std::string> discriminate(const std::string& strId, const std::string& pattern)
  {
    if (pattern.length() != 1U)
    {
      CPCPW_THROW(Exception, "Pattern must be one char!")
    }
    size_t pos = strId.find_first_of(pattern);

    group_identifier<std::string> groupItem;
    if (pos == std::string::npos) // root item
    {
      groupItem = group_identifier<std::string>(strId);
    }
    else // found another group
    {
      groupItem = group_identifier<std::string>(strId.substr(0U, pos), strId.substr(pos + 1U, strId.size() - pos));
    }

    return groupItem;
  }

  template<>
  group_identifier<std::string> discriminate<std::string>(const std::string& strId)
  {
    return discriminate(strId, ".");
  }
}