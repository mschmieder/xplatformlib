add_subdirectory(exception)
add_subdirectory(memory)
add_subdirectory(datatypes)
add_subdirectory(traits)

install(FILES "MemoryLeakDetection.h" 
        DESTINATION "include/cpcpw/core" )