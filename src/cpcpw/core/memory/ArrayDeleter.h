#ifndef _ARRAYDELETER_H
#define _ARRAYDELETER_H

#include <cpcpw/core/core_config.h>

namespace cpcpw
{
  template <typename type_t>
  class ArrayDeleter
  {
  public:
    void operator () (type_t* pArray) const
    {
      delete [] pArray;
    }
  };
}

#endif //_ARRAYDELETER_H