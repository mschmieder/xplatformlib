#include <cpcpw/core/core_config.h>
#include <cpcpw/core/memory/Memory.h>
#include <cpcpw/core/memory/ArrayDeleter.h>
#include <gtest/gtest.h>
#include <array>

using namespace cpcpw;

union TestUnion
{
  float m_fValue;
  uint32_t iValue;
};

class CpcpwCustomTestClass
{
  public:
    CpcpwCustomTestClass()
      : m_fValue(0.0f),
        m_fOtherValue(0.0f)
    {}

    CpcpwCustomTestClass(const float fTemperature, const float fFrequncy)
      : m_fValue(fTemperature),
        m_fOtherValue(fFrequncy)
    {}

    ~CpcpwCustomTestClass()
    {}

    bool operator==(const CpcpwCustomTestClass& rhs) const
    {
      return (this->m_fOtherValue == rhs.m_fOtherValue) &&
             (this->m_fValue == rhs.m_fValue);
    }

    float getValue() const
    {
      return m_fValue;
    }

    float getOtherValue() const
    {
      return m_fOtherValue;
    }


    size_t serialize(Endian endian, char* pBuffer) const
    {
      return Memory::serializeT(endian, pBuffer, m_fValue, m_fOtherValue);
    }

    static CpcpwCustomTestClass deserialize(Endian endian, const char* pBuffer, size_t& nSize)
    {
      float fTemperature; float fFrequency;
      Memory::deserializeT(endian, pBuffer, nSize, fTemperature, fFrequency);
      return CpcpwCustomTestClass(fTemperature, fFrequency);
    }

    size_t sizeofType() const
    {
      return Memory::sizeofT(m_fValue, m_fOtherValue);
    }

  private:
    float m_fValue;
    float m_fOtherValue;

};


class CpcpwComplexCustomClass
{
  public:
    CpcpwComplexCustomClass():
      m_qsr(3, 4),
      m_pQsr(new CpcpwCustomTestClass(1, 2)),
      m_fValue(5.0f)
    {}

    CpcpwComplexCustomClass(const CpcpwComplexCustomClass& other):
      m_qsr(other.m_qsr),
      m_pQsr(nullptr),
      m_fValue(other.m_fValue)
    {
      m_pQsr = new CpcpwCustomTestClass;
      *m_pQsr = *other.m_pQsr;
    }

    CpcpwComplexCustomClass& operator=(CpcpwComplexCustomClass other)
    {
      m_qsr = other.m_qsr;
      *m_pQsr = *other.m_pQsr;
      m_fValue = other.m_fValue;
      return *this;
    }

    virtual ~CpcpwComplexCustomClass()
    {
      delete m_pQsr;
    }

    bool operator==(const CpcpwComplexCustomClass& rhs) const
    {
      return (this->m_qsr == rhs.m_qsr) &&
             (*(this->m_pQsr) == *(rhs.m_pQsr)) &&
             (this->m_fValue == rhs.m_fValue);
    }

    virtual size_t serialize(Endian endian, char* pBuffer) const
    {
      // pointer must be dereferenced!
      return Memory::serializeT(endian, pBuffer, m_qsr, *m_pQsr, m_fValue);
    }

    static CpcpwComplexCustomClass deserialize(Endian endian, const char* pBuffer, size_t& nSize)
    {
      CpcpwComplexCustomClass c;
      Memory::deserializeT(endian, pBuffer, nSize, c.m_qsr, *c.m_pQsr, c.m_fValue);
      return c;
    }

    virtual size_t sizeofType() const
    {
      return Memory::sizeofT(m_qsr, *m_pQsr, m_fValue);
    }

  private:
    CpcpwCustomTestClass m_qsr;
    CpcpwCustomTestClass* m_pQsr;
    float m_fValue;
};

class CpcpwComplexDerivedCustomClass : CpcpwComplexCustomClass
{
  public:
    CpcpwComplexDerivedCustomClass(size_t nVecSize, int32_t i32Value)
      : CpcpwComplexCustomClass()
    {
      m_vecInts = std::vector<int32_t>(nVecSize, i32Value);
    }

    bool operator==(const CpcpwComplexDerivedCustomClass& rhs) const
    {
      bool bIsEqual = CpcpwComplexCustomClass::operator==(rhs);
      bIsEqual &= (rhs.m_vecInts.size() == this->m_vecInts.size());
      for (size_t n = 0; n != this->m_vecInts.size(); ++n)
      {
        bIsEqual &= (rhs.m_vecInts[n] == this->m_vecInts[n]);
      }
      return bIsEqual;
    }


    virtual size_t serialize(Endian endian, char* pBuffer) const
    {
      size_t nSize = CpcpwComplexCustomClass::serialize(endian, pBuffer);
      return nSize + Memory::serializeT(endian, pBuffer + nSize, m_vecInts);
    }

    static CpcpwComplexDerivedCustomClass deserialize(Endian endian, const char* pBuffer, size_t& nSize)
    {
      CpcpwComplexDerivedCustomClass dc(0, 0);
      *(dynamic_cast<CpcpwComplexCustomClass*>(&dc)) = CpcpwComplexCustomClass::deserialize(endian, pBuffer, nSize);
      size_t nMemberSize = 0;
      dc.m_vecInts = Memory::deserializeT<std::vector<int32_t>>(endian, pBuffer + nSize, nMemberSize);
      nSize += nMemberSize;
      return dc;
    }

    virtual size_t sizeofType() const
    {
      return CpcpwComplexCustomClass::sizeofType() + Memory::sizeofT(m_vecInts);
    }


  private:
    std::vector<int32_t> m_vecInts;
};


class NonSerializableDatatype
{
  public:
    void foo() {}
};

TEST(Memory, CharStringNoSwap)
{
  const char zText[] = "Hello World";

  // get size of data & test expected size (string size + siezof(uint32_t) for string len)
  const size_t nSize = Memory::sizeofT(zText);
  ASSERT_EQ(size_t(sizeof(zText) + sizeof(uint32_t)), nSize);

  // encode data & test size
  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), zText));
  ASSERT_EQ(nSize, nSizeEncode);

  // dencode data & test size
  std::string strDecode;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(strDecode = Memory::deserializeT<std::string>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  ASSERT_EQ(nSize, nSizeDecode);
  ASSERT_EQ(strDecode.find(zText), 0U);
}


TEST(Memory, StringNoSwap)
{
  const std::string strText = "Hello World";

  // get size of data & test expected size (string size + siezof(uint32_t) for string len)
  const size_t nSize = Memory::sizeofT(strText);
  ASSERT_EQ(size_t(strText.size() + sizeof(uint32_t)), nSize);

  // encode data & test size
  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), strText));
  ASSERT_EQ(nSize, nSizeEncode);

  // dencode data & test size
  std::string strDecode;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(strDecode = Memory::deserializeT<std::string>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  ASSERT_EQ(nSize, nSizeDecode);
  ASSERT_EQ(strDecode.find(strText), 0U);
}

TEST(Memory, StringSwap)
{
  const std::string strText = "Hello World";

  // get size of data
  const size_t nSize = Memory::sizeofT(strText);

  // encode data & test size
  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::swap, pBuffer.get(), strText));
  ASSERT_EQ(nSize, nSizeEncode);

  // dencode data & test size
  std::string strDecode;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(strDecode = Memory::deserializeT<std::string>(Endian::swap, pBuffer.get(), nSizeDecode));
  ASSERT_EQ(nSize, nSizeDecode);
  ASSERT_EQ(strDecode.find(strText), 0U);
}

TEST(Memory, VectorNoSwap)
{
  std::vector<float> vecA(10, 1.05f);
  static_assert(traits::is_serializable<const std::vector<float>>::value, "std::vector<float should be serializable");
  static_assert(traits::is_container<const std::vector<float>>::value, "std::vector<float should be a container");

  const size_t nSize = Memory::sizeofT(vecA);
  ASSERT_EQ(size_t(10 * sizeof(float) + sizeof(uint32_t)), nSize);

  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());

  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), vecA));
  ASSERT_EQ(nSize, nSizeEncode);

  std::vector<float> vecB;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(vecB = Memory::deserializeT<std::vector<float>>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  ASSERT_EQ(nSize, nSizeDecode);
  ASSERT_EQ(vecA, vecB);
}

TEST(Memory, VectorSwap)
{
  std::vector<float> vecA(10, 1.05f);
  static_assert(traits::is_serializable<const std::vector<float>>::value, "std::vector<float should be serializable");
  static_assert(traits::is_container<const std::vector<float>>::value, "std::vector<float should be a container");

  const size_t nSize = Memory::sizeofT(vecA);
  ASSERT_EQ(size_t(10 * sizeof(float) + sizeof(uint32_t)), nSize);

  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());

  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), vecA));
  ASSERT_EQ(nSize, nSizeEncode);

  std::vector<float> vecB;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(vecB = Memory::deserializeT<std::vector<float>>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  ASSERT_EQ(nSize, nSizeDecode);
  ASSERT_EQ(vecA, vecB);
}


TEST(Memory, ChangeContainerType)
{
  static_assert(traits::is_serializable<const std::list<float>>::value, "std::list<float should be serializable");
  static_assert(traits::is_container<const std::list<float>>::value, "std::list<float should be a container");

  std::vector<float> vecA(10, 1.05f);

  const size_t nSize = Memory::sizeofT(vecA);

  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());

  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer.get(), vecA));


  std::list<float> lstB;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(lstB = Memory::deserializeT<std::list<float>>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  ASSERT_EQ(nSize, nSizeDecode);

  ASSERT_EQ(vecA.size(), lstB.size());
  // copy  to vector for compare
  std::vector<float> vecC{ std::begin(lstB), std::end(lstB) };
  ASSERT_EQ(vecA, vecC);
}

TEST(Memory, SetToSetType)
{
  static_assert(traits::is_serializable<std::set<float>>::value, "std::set<float> should be serializable");

  std::set<float> setA;
  setA.insert(1.0f);
  setA.insert(2.0f);
  setA.insert(3.0f);

  const size_t nSize = Memory::sizeofT(setA);
  ASSERT_EQ(size_t(3 * sizeof(float) + sizeof(uint32_t)), nSize);

  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), setA));
  ASSERT_EQ(nSize, nSizeEncode);

  std::set<float> setB;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(setB = Memory::deserializeT<std::set<float>>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  EXPECT_EQ(nSize, nSizeDecode);

  ASSERT_EQ(setA.size(), setB.size());
  EXPECT_EQ(setA, setB);
}

TEST(Memory, NotSerializable)
{
  static_assert(!traits::is_serializable<NonSerializableDatatype>::value, "NonSerializableDatatype should not be serializable");
  static_assert(!traits::is_serializable<const NonSerializableDatatype>::value, "const NonSerializableDatatype should not be serializable");
  static_assert(!traits::is_serializable<const NonSerializableDatatype&>::value, "const NonSerializableDatatype& should not be serializable");
  static_assert(!traits::is_serializable<NonSerializableDatatype*>::value, "NonSerializableDatatype* should not be serializable");
  static_assert(!traits::is_serializable<const NonSerializableDatatype*>::value, "const NonSerializableDatatype* should not be serializable");

  // this function call will cause a compilation error due to incompatibilities of NonSerializableDatatype to our serialization framework
  //Memory::sizeofT(NonSerializableDatatype());
}
//
//TEST(Memory, ArraysNoSwap)
//{
//  int a[10];
//  for(int32_t i = 0; i < 10; ++i){
//    a[i] = i + 1;
//  }
//
//  size_t nSize = Memory::sizeofT(a);
//
//  ASSERT_EQ(nSize, size_t(10 * sizeof(int32_t)+sizeof(uint32_t)));
//
//  char* pBuffer = new char[nSize];
//  EXPECT_EQ(nSize, Memory::serializeT(Endian::no_swap, pBuffer.get(), a));
//
//  int b[10];
//  EXPECT_EQ(nSize, Memory::deserializeT(Endian::no_swap, pBuffer.get(), b));
//
//  for(int32_t i = 0; i < 10; ++i){
//    EXPECT_EQ(a[i], b[i]);
//  }
//
//    delete[] pBuffer;
//}
//
//TEST(Memory, ArraysSwap)
//{
//  int a[10];
//  for(int32_t i = 0; i < 10; ++i){
//    a[i] = i + 1;
//  }
//
//  size_t nSize = Memory::sizeofT(a);
//
//  ASSERT_EQ(nSize, size_t(10 * sizeof(int32_t)+sizeof(uint32_t)));
//
//  char* pBuffer = new char[nSize];
//  EXPECT_EQ(nSize, Memory::serializeT(Endian::swap, pBuffer.get(), a));
//
//  int b[10];
//  EXPECT_EQ(nSize, Memory::deserializeT(Endian::swap,pBuffer.get(), b));
//
//  for(int32_t i = 0; i < 10; ++i){
//    EXPECT_EQ(a[i], b[i]);
//  }
//
//    delete[] pBuffer;
//}
//
//TEST(Memory, ArraysSwapSanity)
//{
//  int a[10];
//  for(int32_t i = 0; i < 10; ++i){
//    a[i] = i + std::numeric_limits<int32_t>::max() / 2;
//  }
//
//  size_t nSize = Memory::sizeofT(a);
//
//  ASSERT_EQ(nSize, size_t(10 * sizeof(int32_t) + sizeof(uint32_t)));
//
//  char* pBuffer = new char[nSize];
//  EXPECT_EQ(nSize, Memory::serializeT(Endian::swap, pBuffer.get(), a));
//
//  int b[10];
//  EXPECT_EQ(0U, Memory::deserializeT(Endian::no_swap, pBuffer.get(), b));
//
//    delete[] pBuffer;
//}

TEST(Memory, CustomDatatype)
{
  CpcpwCustomTestClass qsrPackage(5, 6);

  static_assert(traits::is_serializable<CpcpwCustomTestClass>::value, "CpcpwCustomTestClass needs to be serializable");
  static_assert(traits::is_serializable<const CpcpwCustomTestClass>::value, "const CpcpwCustomTestClass needs to be serializable");
  static_assert(traits::is_serializable<const CpcpwCustomTestClass&>::value, "const CpcpwCustomTestClass& needs to be serializable");
  //static_assert(traits::is_serializable<CpcpwCustomTestClass*>::value, "CpcpwCustomTestClass* needs to be serializable");
  //static_assert(traits::is_serializable<const CpcpwCustomTestClass*>::value, "const CpcpwCustomTestClass* needs to be serializable");

  size_t nSize = Memory::sizeofT(qsrPackage);
  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), qsrPackage));
  ASSERT_EQ(nSize, nSizeEncode);

  CpcpwCustomTestClass qsrDecoded(0, 0);
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(qsrDecoded = Memory::deserializeT<CpcpwCustomTestClass>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  EXPECT_EQ(nSize, nSizeDecode);

  EXPECT_EQ(qsrPackage, qsrDecoded);

}

TEST(Memory, ComplexDatatype)
{
  CpcpwComplexCustomClass complexClass;

  static_assert(traits::is_serializable<CpcpwComplexCustomClass>::value, "CpcpwComplexCustomClass needs to be serializable");
  static_assert(traits::is_serializable<const CpcpwComplexCustomClass>::value, "const CpcpwComplexCustomClass needs to be serializable");
  static_assert(traits::is_serializable<const CpcpwComplexCustomClass&>::value, "const CpcpwComplexCustomClass& needs to be serializable");
//  static_assert(traits::is_serializable<CpcpwComplexCustomClass*>::value, "CpcpwComplexCustomClass* needs to be serializable");
//  static_assert(traits::is_serializable<const CpcpwComplexCustomClass*>::value, "const CpcpwComplexCustomClass* needs to be serializable");

  size_t nSize = Memory::sizeofT(complexClass);
  std::shared_ptr<char> pBuffer(new char[nSize], ArrayDeleter<char>());
  size_t nSizeEncode;
  ASSERT_NO_THROW(nSizeEncode = Memory::serializeT(Endian::no_swap, pBuffer.get(), complexClass));
  ASSERT_EQ(nSize, nSizeEncode);

  CpcpwComplexCustomClass complexClassDecoded;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(complexClassDecoded = Memory::deserializeT<CpcpwComplexCustomClass>(Endian::no_swap, pBuffer.get(), nSizeDecode));
  EXPECT_EQ(nSize, nSizeDecode);

  EXPECT_EQ(complexClass, complexClassDecoded);
}

TEST(Memory, CpcpwComplexDerivedCustomClass)
{
  CpcpwComplexDerivedCustomClass objOriginal(10, 5);

  static_assert(traits::is_serializable<CpcpwComplexDerivedCustomClass>::value, "CpcpwComplexDerivedCustomClass needs to be serializable");
  static_assert(traits::is_serializable<const CpcpwComplexDerivedCustomClass>::value, "const CpcpwComplexDerivedCustomClass needs to be serializable");
  static_assert(traits::is_serializable<const CpcpwComplexDerivedCustomClass&>::value, "const CpcpwComplexDerivedCustomClass& needs to be serializable");
  //static_assert(traits::is_serializable<CpcpwComplexDerivedCustomClass*>::value, "CpcpwComplexDerivedCustomClass* needs to be serializable");
  //static_assert(traits::is_serializable<const CpcpwComplexDerivedCustomClass*>::value, "const CpcpwComplexDerivedCustomClass* needs to be serializable");

  size_t nSize = Memory::sizeofT(objOriginal);
  char* pBuffer = new char[nSize];
  EXPECT_EQ(nSize, Memory::serializeT(Endian::no_swap, pBuffer, objOriginal));

  CpcpwComplexDerivedCustomClass objDecoded(0, 0);
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(objDecoded = Memory::deserializeT<CpcpwComplexDerivedCustomClass>(Endian::no_swap, pBuffer, nSizeDecode));
  EXPECT_EQ(nSize, nSizeDecode);

  EXPECT_EQ(objOriginal, objDecoded);

  delete[] pBuffer;
}


int8_t   s8Val = 8;
uint8_t  u8Val = 8;
int16_t  s16Val = 16;
uint16_t u16Val = 16;
int32_t  s32Val = 32;
uint32_t u32Val = 32;
int64_t  s64Val = 64;
uint64_t u64Val = 64;
float    fVal = 1.1f;
double   dVal = 2.2;

size_t nBufferSize = Memory::sizeofT(s8Val,
                                     u8Val,
                                     s16Val,
                                     u16Val,
                                     s32Val,
                                     u32Val,
                                     s64Val,
                                     u64Val,
                                     fVal,
                                     dVal);

TEST(Memory, MemcpyNoSwap)
{
  int8_t   int8_ = 0;
  uint8_t  uint8_ = 0;
  int16_t  int16_ = 0;
  uint16_t uint16_ = 0;
  int32_t  int32_ = 0;
  uint32_t uint32_ = 0;
  int64_t  int64_ = 0;
  uint64_t uint64_ = 0;
  float    fl_ = 0;
  double   dbl_ = 0;

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);

  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer,
                                     s8Val,
                                     u8Val,
                                     s16Val,
                                     u16Val,
                                     s32Val,
                                     u32Val,
                                     s64Val,
                                     u64Val,
                                     fVal,
                                     dVal));

  size_t nSizeDecoded;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecoded,
                                       int8_,
                                       uint8_,
                                       int16_,
                                       uint16_,
                                       int32_,
                                       uint32_,
                                       int64_,
                                       uint64_,
                                       fl_,
                                       dbl_));

  EXPECT_EQ(s8Val, int8_);
  EXPECT_EQ(u8Val, uint8_);
  EXPECT_EQ(s16Val, int16_);
  EXPECT_EQ(u16Val, uint16_);
  EXPECT_EQ(s32Val, int32_);
  EXPECT_EQ(u32Val, uint32_);
  EXPECT_EQ(s64Val, int64_);
  EXPECT_EQ(u64Val, uint64_);
  EXPECT_EQ(fVal, fl_);
  EXPECT_EQ(dVal, dbl_);

  delete[] pBuffer;
}

TEST(Memory, MemcpySwap)
{
  int8_t   int8_ = 0;
  uint8_t  uint8_ = 0;
  int16_t  int16_ = 0;
  uint16_t uint16_ = 0;
  int32_t  int32_ = 0;
  uint32_t uint32_ = 0;
  int64_t  int64_ = 0;
  uint64_t uint64_ = 0;
  float    fl_ = 0;
  double   dbl_ = 0;

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);

  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, s8Val,
                                     u8Val,
                                     s16Val,
                                     u16Val,
                                     u32Val,
                                     s32Val,
                                     s64Val,
                                     u64Val,
                                     fVal,
                                     dVal));

  size_t nSizeDecoded;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecoded,
                                       int8_,
                                       uint8_,
                                       int16_,
                                       uint16_,
                                       int32_,
                                       uint32_,
                                       int64_,
                                       uint64_,
                                       fl_,
                                       dbl_));

  EXPECT_EQ(s8Val, int8_);
  EXPECT_EQ(u8Val, uint8_);
  EXPECT_EQ(s16Val, int16_);
  EXPECT_EQ(u16Val, uint16_);
  EXPECT_EQ(s32Val, int32_);
  EXPECT_EQ(u32Val, uint32_);
  EXPECT_EQ(s64Val, int64_);
  EXPECT_EQ(u64Val, uint64_);
  EXPECT_EQ(fVal, fl_);
  EXPECT_EQ(dVal, dbl_);

  delete[] pBuffer;
}

TEST(Memory, MemcpySwapSanity)
{
  int8_t   int8_ = 0;
  uint8_t  uint8_ = 0;
  int16_t  int16_ = 0;
  uint16_t uint16_ = 0;
  int32_t  int32_ = 0;
  uint32_t uint32_ = 0;
  int64_t  int64_ = 0;
  uint64_t uint64_ = 0;
  float    fl_ = 0;
  double   dbl_ = 0;

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);

  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, s8Val,
                                     u8Val,
                                     s16Val,
                                     u16Val,
                                     s32Val,
                                     u32Val,
                                     s64Val,
                                     u64Val,
                                     fVal,
                                     dVal));

  size_t nSizeDecoded;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecoded,
                                       int8_,
                                       uint8_,
                                       int16_,
                                       uint16_,
                                       int32_,
                                       uint32_,
                                       int64_,
                                       uint64_,
                                       fl_,
                                       dbl_));

  EXPECT_EQ(s8Val, int8_);
  EXPECT_EQ(u8Val, uint8_);
  EXPECT_NE(s16Val, int16_);
  EXPECT_NE(u16Val, uint16_);
  EXPECT_NE(s32Val, int32_);
  EXPECT_NE(u32Val, uint32_);
  EXPECT_NE(s64Val, int64_);
  EXPECT_NE(u64Val, uint64_);
  EXPECT_NE(fVal, fl_);
  EXPECT_NE(dVal, dbl_);

  delete[] pBuffer;
}

TEST(Memory, MemcpyUnion)
{
  TestUnion tUnion;
  TestUnion tUnion_;
  tUnion.m_fValue = 1.0f;

  size_t nUnionSize = 0;
  ASSERT_NO_THROW(nUnionSize = Memory::sizeofT(tUnion));
  EXPECT_EQ(sizeof(TestUnion), nUnionSize);

  ASSERT_NE(size_t(0), nUnionSize);
  char* pBuffer = new char[nUnionSize];
  ASSERT_NE(nullptr, pBuffer);

  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer, tUnion));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(tUnion_ = Memory::deserializeT<TestUnion>(Endian::no_swap, pBuffer, nSizeDecode));

  EXPECT_EQ(tUnion.m_fValue, tUnion_.m_fValue);
  EXPECT_EQ(tUnion.iValue, tUnion_.iValue);
  delete[] pBuffer;
}

TEST(Memory, MemcpyUnionSwap)
{
  TestUnion tUnion;
  TestUnion tUnion_;
  tUnion.m_fValue = 1.0f;

  size_t nUnionSize = 0;
  ASSERT_NO_THROW(nUnionSize = Memory::sizeofT(tUnion));
  EXPECT_EQ(sizeof(TestUnion), nUnionSize);

  ASSERT_NE(size_t(0), nUnionSize);
  char* pBuffer = new char[nUnionSize];
  ASSERT_NE(nullptr, pBuffer);


  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, tUnion));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(tUnion_ = Memory::deserializeT<TestUnion>(Endian::swap, pBuffer, nSizeDecode));

  EXPECT_EQ(tUnion.m_fValue, tUnion_.m_fValue);
  EXPECT_EQ(tUnion.iValue, tUnion_.iValue);
  delete[] pBuffer;
}

TEST(Memory, MemcpyNonTrivialArraySwap)
{
  CpcpwCustomTestClass aQsrMeasurements[10];
  CpcpwCustomTestClass aQsrMeasurementsDecoded[10];
  for (size_t n = 0; n < 10; ++n)
  {
    aQsrMeasurements[n] = CpcpwCustomTestClass(static_cast<float>(n), static_cast<float>(n));
  }

  size_t nBufferSize = 0;
  ASSERT_NO_THROW(nBufferSize = Memory::sizeofT(aQsrMeasurements));
  EXPECT_EQ(Memory::sizeofT(CpcpwCustomTestClass()) * 10 + sizeof(uint32_t), nBufferSize);

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);


  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, aQsrMeasurements));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, aQsrMeasurementsDecoded));

  for (size_t n = 0; n < 10; ++n)
  {
    EXPECT_FLOAT_EQ(aQsrMeasurements[n].getOtherValue(), aQsrMeasurementsDecoded[n].getOtherValue());
    EXPECT_FLOAT_EQ(aQsrMeasurements[n].getValue(), aQsrMeasurementsDecoded[n].getValue());
  }
  delete[] pBuffer;
}

TEST(Memory, MemcpyNonTrivialArrayNoSwap)
{
  CpcpwCustomTestClass aQsrMeasurements[10];
  CpcpwCustomTestClass aQsrMeasurementsDecoded[10];
  for (size_t n = 0; n < 10; ++n)
  {
    aQsrMeasurements[n] = CpcpwCustomTestClass(static_cast<float>(n), static_cast<float>(n));
  }

  size_t nBufferSize = 0;
  ASSERT_NO_THROW(nBufferSize = Memory::sizeofT(aQsrMeasurements));
  EXPECT_EQ(Memory::sizeofT(aQsrMeasurements[0]) * 10 + sizeof(uint32_t), nBufferSize);

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);


  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer, aQsrMeasurements));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, aQsrMeasurementsDecoded));

  for (size_t n = 0; n < 10; ++n)
  {
    EXPECT_FLOAT_EQ(aQsrMeasurements[n].getOtherValue(), aQsrMeasurementsDecoded[n].getOtherValue());
    EXPECT_FLOAT_EQ(aQsrMeasurements[n].getValue(), aQsrMeasurementsDecoded[n].getValue());
  }
  delete[] pBuffer;
}

enum TestEnum
{
  e_a = 0,
  e_b
};

TEST(Memory, EnumSerializationNoSwap)
{
  TestEnum eValue = e_b;

  ASSERT_EQ(sizeof(TestEnum), Memory::sizeofT(eValue));

  size_t nBufferSize = 0;
  ASSERT_NO_THROW(nBufferSize = Memory::sizeofT(eValue));
  EXPECT_EQ(sizeof(TestEnum), nBufferSize);

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);

  TestEnum eValue_ = e_a;
  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer, eValue));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, eValue_));

  ASSERT_EQ(eValue_, eValue);
  delete[] pBuffer;
}

TEST(Memory, EnumSerializationSwap)
{
  TestEnum eValue = e_b;

  ASSERT_EQ(sizeof(TestEnum), Memory::sizeofT(eValue));

  size_t nBufferSize = 0;
  ASSERT_NO_THROW(nBufferSize = Memory::sizeofT(eValue));
  EXPECT_EQ(sizeof(TestEnum), nBufferSize);

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);

  TestEnum eValue_ = e_a;
  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, eValue));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, eValue_));

  ASSERT_EQ(eValue_, eValue);
  delete[] pBuffer;
}

enum class TypedTestEnum : std::int32_t
{
  te_a = 0,
  te_b
};

TEST(Memory, TypeedEnumSerializationSwap)
{
  TypedTestEnum eValue = TypedTestEnum::te_b;
  EXPECT_EQ(sizeof(int32_t), Memory::sizeofT(eValue));

  size_t nBufferSize = 0;
  ASSERT_NO_THROW(nBufferSize = Memory::sizeofT(eValue));
  EXPECT_EQ(sizeof(int32_t), nBufferSize);

  ASSERT_NE(size_t(0), nBufferSize);
  char* pBuffer = new char[nBufferSize];
  ASSERT_NE(nullptr, pBuffer);

  TypedTestEnum eValue_ = TypedTestEnum::te_a;
  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, eValue));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, eValue_));

  ASSERT_EQ(eValue_, eValue);
  delete[] pBuffer;
}

TEST(Memory, STLArray)
{
  std::array<char, 5> myCharArray = { {1, 2, 3, 4, 5} };
  size_t nSize = Memory::sizeofT(myCharArray);
  ASSERT_EQ(nSize, sizeof(std::array<char, 5>));

  char* pBuffer = new char[nSize];
  ASSERT_NE(nullptr, pBuffer);

  std::array<char, 5> myCharArray_;
  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer, myCharArray));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, myCharArray_));

  for (size_t n = 0; n < 5; ++n)
  {
    EXPECT_EQ(myCharArray[n], myCharArray_[n]);
  }

  delete[] pBuffer;
}

TEST(Memory, STLArraySwap)
{
  std::array<int32_t, 5> myCharArray = { {1, 2, 3, 4, 5} };
  size_t nSize = Memory::sizeofT(myCharArray);
  ASSERT_EQ(nSize, sizeof(std::array<int32_t, 5>));

  char* pBuffer = new char[nSize];
  ASSERT_NE(nullptr, pBuffer);

  std::array<int32_t, 5> myCharArray_;
  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, myCharArray));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, myCharArray_));

  for (size_t n = 0; n < 5; ++n)
  {
    EXPECT_EQ(myCharArray[n], myCharArray_[n]);
  }

  delete[] pBuffer;
}

TEST(Memory, STLArrayComplex)
{
  std::array<CpcpwCustomTestClass, 5> myCharArray;
  size_t nSize = Memory::sizeofT(myCharArray);
  ASSERT_EQ(5 * Memory::sizeofT(myCharArray[0]) + sizeof(uint32_t), nSize);

  char* pBuffer = new char[nSize];
  ASSERT_NE(nullptr, pBuffer);

  std::array<CpcpwCustomTestClass, 5> myCharArray_;
  ASSERT_NO_THROW(Memory::serializeT(Endian::no_swap, pBuffer, myCharArray));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, myCharArray_));

  for (size_t n = 0; n < 5; ++n)
  {
    EXPECT_EQ(myCharArray[n], myCharArray_[n]);
  }

  delete[] pBuffer;
}

TEST(Memory, STLArrayComplexSwap)
{
  std::array<CpcpwCustomTestClass, 5> myCharArray;
  size_t nSize = Memory::sizeofT(myCharArray);
  ASSERT_EQ(5 * Memory::sizeofT(myCharArray[0]) + sizeof(uint32_t), nSize);

  char* pBuffer = new char[nSize];
  ASSERT_NE(nullptr, pBuffer);

  std::array<CpcpwCustomTestClass, 5> myCharArray_;
  ASSERT_NO_THROW(Memory::serializeT(Endian::swap, pBuffer, myCharArray));
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, myCharArray_));

  for (size_t n = 0; n < 5; ++n)
  {
    EXPECT_EQ(myCharArray[n], myCharArray_[n]);
  }

  delete[] pBuffer;
}

TEST(Memory, CharTest)
{
  char pText[12] = "Hello World";

  const size_t nSize = Memory::sizeofT(pText);
  ASSERT_EQ(size_t(sizeof("Hello World") + sizeof(uint32_t)), nSize);

  char* pBuffer = new char[nSize];
  ASSERT_EQ(nSize, Memory::serializeT(Endian::no_swap, pBuffer, "Hello World"));

  size_t nSizeDecode = 0;
  std::string strDecode;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, strDecode));
  ASSERT_EQ(nSize, nSizeDecode);

  //ASSERT_EQ(strString, strDecode);

  delete[] pBuffer;
}

TEST(Memory, VecBoolTest)
{
  std::vector<bool> vecBool = {true, true, false};

  const size_t nSize = Memory::sizeofT(vecBool);
  ASSERT_NE(0U, nSize);

  char* pBuffer = new char[nSize];
  ASSERT_EQ(nSize, Memory::serializeT(Endian::no_swap, pBuffer, vecBool));

  std::vector<bool> decodedVecBool;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, decodedVecBool));
  ASSERT_EQ(nSize, nSizeDecode);

  ASSERT_EQ(vecBool.size(), decodedVecBool.size());
  for (size_t cnt = 0; cnt < vecBool.size(); ++cnt)
  {
    EXPECT_EQ(vecBool[cnt], decodedVecBool[cnt]);
  }

  delete[] pBuffer;
}

TEST(Memory, VecBoolTestSwap)
{
  std::vector<bool> vecBool = {true, true, false};

  const size_t nSize = Memory::sizeofT(vecBool);
  ASSERT_NE(0U, nSize);

  char* pBuffer = new char[nSize];
  ASSERT_EQ(nSize, Memory::serializeT(Endian::swap, pBuffer, vecBool));

  std::vector<bool> decodedVecBool;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, decodedVecBool));
  ASSERT_EQ(nSize, nSizeDecode);

  ASSERT_EQ(vecBool.size(), decodedVecBool.size());
  for (size_t cnt = 0; cnt < vecBool.size(); ++cnt)
  {
    EXPECT_EQ(vecBool[cnt], decodedVecBool[cnt]);
  }

  delete[] pBuffer;
}

TEST(Memory, AssociativeContainer)
{
  std::map<int, float> mapInt =
  {
    {0, 1.0f},
    {1, 2.0f}
  };

  const size_t nSize = Memory::sizeofT(mapInt);
  ASSERT_NE(0U, nSize);
  EXPECT_EQ( sizeof(int) + 2 * (sizeof(int) + sizeof(float)), nSize);

  char* pBuffer = new char[nSize];
  ASSERT_EQ(nSize, Memory::serializeT(Endian::no_swap, pBuffer, mapInt));

  std::map<int, float> mapDec;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::no_swap, pBuffer, nSizeDecode, mapDec));

  ASSERT_EQ(mapInt.size(), mapDec.size());
  auto itRef = mapInt.begin();
  auto itDec = mapDec.begin();
  for (size_t cnt = 0; cnt < mapInt.size(); ++cnt, ++itRef, ++itDec)
  {
    EXPECT_EQ(itRef->first, itDec->first);
    EXPECT_EQ(itRef->second, itDec->second);
  }

  delete[] pBuffer;
}

TEST(Memory, AssociativeContainerSwap)
{
  std::map<int, float> mapInt =
  {
    {0, 1.0f},
    {1, 2.0f}
  };

  const size_t nSize = Memory::sizeofT(mapInt);
  ASSERT_NE(0U, nSize);
  EXPECT_EQ( sizeof(int) + 2 * (sizeof(int) + sizeof(float)), nSize );

  char* pBuffer = new char[nSize];
  ASSERT_EQ(nSize, Memory::serializeT(Endian::swap, pBuffer, mapInt));

  std::map<int, float> mapDec;
  size_t nSizeDecode = 0;
  ASSERT_NO_THROW(Memory::deserializeT(Endian::swap, pBuffer, nSizeDecode, mapDec));

  ASSERT_EQ(mapInt.size(), mapDec.size());
  auto itRef = mapInt.begin();
  auto itDec = mapDec.begin();
  for (size_t cnt = 0; cnt < mapInt.size(); ++cnt, ++itRef, ++itDec)
  {
    EXPECT_EQ(itRef->first, itDec->first);
    EXPECT_EQ(itRef->second, itDec->second);
  }

  delete[] pBuffer;
}

TEST(Memory, MapToString)
{
  std::map<std::string, int32_t> mapValues =
  {
    {"A", 1},
    {"BCD", 2},
    {"", 5 },
    {"EFG", 3}
  };

  std::string str = "";
  ASSERT_NO_THROW(str = Memory::toStringT(mapValues));
}

TEST(Memory, MapFromString)
{
  std::map<std::string, int32_t> mapValues =
  {
    { "A", 1 },
    { "BCD", 2 },
    { "", 5 },
    { "EFG", 3 }
  };

  std::string str = "";
  ASSERT_NO_THROW(str = Memory::toStringT(mapValues));
  std::map<std::string, int32_t> mapDecValues = Memory::fromStringT<std::map<std::string, int32_t>>(str);

  ASSERT_EQ(mapValues.size(), mapDecValues.size());
  auto itE = mapDecValues.end();
  for (auto elempair : mapValues)
  {
    auto itF = mapDecValues.find(elempair.first);
    ASSERT_TRUE(itF != itE);
    EXPECT_EQ(elempair.second, itF->second);
  }
}

TEST(Memory, VectorFromToString)
{
  std::vector<float> vecValues =
  { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f };

  std::string str = "";
  ASSERT_NO_THROW(str = Memory::toStringT(vecValues));
  std::vector<float> vecDecValues;
  ASSERT_NO_THROW(vecDecValues = Memory::fromStringT<std::vector<float>>(str));

  ASSERT_EQ(vecValues.size(), vecDecValues.size());
  EXPECT_EQ(vecValues, vecDecValues);
}

TEST(Memory, BoolVectorFromToString)
{
  std::vector<bool> vecValues = { true, false, false, true, false, false};

  std::string str = "";
  ASSERT_NO_THROW(str = Memory::toStringT(vecValues));
  std::vector<bool> vecDecValues = Memory::fromStringT<std::vector<bool>>(str);

  ASSERT_EQ(vecValues.size(), vecDecValues.size());
  EXPECT_EQ(vecValues, vecDecValues);
}