#include "MemoryAscii.h"

#include <string>
#include <sstream>
#include <iomanip>

#include <cpcpw/core/MemoryLeakDetection.h>
#define new DEBUG_NEW_LEAKAGE

namespace cpcpw {
  namespace intern {

    template <>
    std::string toStringTimpl<int8_t>(const int8_t& value)
    {
      std::stringstream sstr;
      sstr << int16_t(value);
      return sstr.str();
    }

    template <>
    std::string toStringTimpl<uint8_t>(const uint8_t& value)
    {
      std::stringstream sstr;
      sstr << uint16_t(value);
      return sstr.str();
    }

    template <>
    std::string toStringTimpl<float>(const float& value)
    {
      std::stringstream sstr;
      sstr << std::setprecision(9) << value;
      return sstr.str();
    }

    template <>
    std::string toStringTimpl<double>(const double& value)
    {
      std::stringstream sstr;
      sstr << std::setprecision(18) << value;
      return sstr.str();
    }

    template <>
    uint8_t fromStringTimpl<uint8_t>(const std::string& strData)
    {
      uint16_t data;
      std::stringstream sstr;
      sstr << strData;
      sstr >> data;
      return static_cast<uint8_t>(data);
    }

    template <>
    int8_t fromStringTimpl<int8_t>(const std::string& strData)
    {
      int16_t data;
      std::stringstream sstr;
      sstr << strData;
      sstr >> data;
      return static_cast<int8_t>(data);
    }

    template <>
    std::string fromStringTimpl<std::string>(const std::string& strData)
    {
      return strData;
    }
  }

}