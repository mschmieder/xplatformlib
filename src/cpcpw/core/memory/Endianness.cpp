#include "Endianness.h"
#include <cpcpw/core/MemoryLeakDetection.h>
#define new DEBUG_NEW_LEAKAGE

using namespace cpcpw;

bool Endianness::isLittleEndianSystem()
{
  union
  {
    uint32_t i;
    char c[4];
  } bigint = {0x01020304U};

  if (bigint.c[0] == 1)
    return false;
  else
    return true;
}

bool Endianness::isBigEndianSystem()
{
  return !isLittleEndianSystem();
}