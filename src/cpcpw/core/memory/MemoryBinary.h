#ifndef _MEMORY_BINARY_H_
#define _MEMORY_BINARY_H_

#include <cpcpw/core/core_config.h>
#include <cpcpw/core/memory/Endianness.h>
#include "MemoryAscii.h"

#include <type_traits>
#include <cpcpw/core/traits/cpcpw_traits.h>
#include <memory>
#include <algorithm>
#include <assert.h>
#include <string.h> //memcpy
#include <sstream>
#include <vector>
#include <list>
#include <map>

#ifdef USE_OPENCV
#include <opencv2/core/core.hpp>
#endif

#if defined (CPCPW_USE_QT5)
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
#include <QString>
#include <QDateTime>
#endif

/**
***************************************************************************
defines
***************************************************************************/
#if defined (MSVC)
# define _CONSTEXPR
#else
# define _CONSTEXPR constexpr
#endif


namespace cpcpw {
  class CPCPW_CORE_MEMORY_DECL Memory
  {
  public:
    /**
    ***************************************************************************
    sizeofT returns the size of serialized data
    It can be used to dertermine the buffer size needed to serialize a datum.
    It is defined for standard types or types of sizeofType function.
    param [in] type
    return size of serialized data
    ***************************************************************************/

    // fundamental types uint32_t/int/float...
    // SFINAE: fallback function for fundamental types
    // !has_member_func_cpcpw_sizeofT && is_fundamental
    template <typename type_t>
    static _CONSTEXPR typename std::enable_if<traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value,
           size_t>::type
           sizeofT(type_t&&)
    {
      return sizeof(type_t);
    }

    // other types
    template <typename type_t>
    static _CONSTEXPR typename std::enable_if <
    !traits::is_serializable<typename std::remove_reference<type_t>::type>::value
#ifdef USE_OPENCV
    && (!std::is_same<typename std::remove_reference<type_t>::type, cv::Mat>::value)
    && (!std::is_same<typename std::remove_reference<type_t>::type, const cv::Mat>::value)
#endif
    && !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value
    && !(traits::is_stl_array<typename std::remove_reference<type_t>::type>::value
         &&   traits::is_serializable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value),
         size_t
         >::type
         sizeofT(type_t&&)
    {
      return 0;
    }

    // custom types with sizeofType function
    // SFINAE: cpcpw datatypes following the correct syntax
    //  has_member_func_cpcpw_sizeofT && !is_fundamental
    template <typename type_t>
    static typename std::enable_if <
    traits::has_member_func_cpcpw_sizeofT <
    typename std::remove_const<typename std::remove_reference<type_t>::type>::type
    >::value,
    size_t
    >::type
    sizeofT(type_t&& t)
    {
      return t.sizeofType();
    }

    // type string
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !std::is_array<typename std::remove_reference<type_t>::type>::value&&
    !std::is_fundamental<typename std::remove_reference<type_t>::type>::value&&
    !std::is_union<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::isString,
           size_t
           >::type sizeofT(type_t&& t)
    {
      return t.size() + sizeof(uint32_t);
    }

#ifdef USE_OPENCV
    // type cv::Mat
    template <typename type_t>
    static typename std::enable_if <
    std::is_same<typename std::remove_reference<type_t>::type, cv::Mat>::value
    || std::is_same<typename std::remove_reference<type_t>::type, const cv::Mat>::value
    , size_t
    >::type sizeofT(type_t&& t)
    {
      size_t nParamSize = 4 * sizeof(uint32_t);     /* parameters dimensions, array depth, channels, channel size */
      size_t nDimsSize = t.dims * sizeof(uint32_t); /* array of integers, specifying the n-dimensional array shape */
      size_t nDataSize = t.total() * t.elemSize();  /*  data size */

      return nParamSize + nDimsSize + nDataSize;
    }
#endif

    // SFINAE: cpcpw datatypes (pointers) following the correct syntax
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value,
           size_t
           >::type sizeofT(type_t&& t)
    {
      size_t nSize = sizeof(uint32_t); // first two bytes will be length of container
      for (const typename std::remove_reference<type_t>::type::value_type& elem : t)
      {
        nSize += sizeofT(elem);
      }
      return nSize;
    }

    template <typename type_t>
    static typename std::enable_if <
    traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value,
           size_t
           >::type sizeofT(type_t&& t)
    {
      size_t nSize = sizeof(uint32_t); // will hold the number of elements in the container
      for (auto elem_pair : t)
      {
        nSize += sizeofT(elem_pair.first);
        nSize += sizeofT(elem_pair.second);
      }
      return nSize;
    }

    template <typename type_t>
    static typename std::enable_if <
    traits::is_stl_array<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_trivialy_memcopyable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value
    && traits::is_serializable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value,
    size_t
    >::type sizeofT(type_t&& t)
    {
      size_t nSize = sizeof(uint32_t); // first two bytes will be length of container
      for (const typename std::remove_reference<type_t>::type::value_type& elem : t)
      {
        nSize += sizeofT(elem);
      }
      return nSize;
    }

    template <typename type_t>
    static typename std::enable_if <
    !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value&&
    std::is_array<typename std::remove_reference<type_t>::type>::value,
        size_t
        >::type sizeofT(type_t&& t)
    {
      return (sizeof(type_t) / sizeof(t[0]) * sizeofT(t[0])) + sizeof(uint32_t);
    }

    /**
     * @brief
     * variadic template function that returns the accumulated size of a list of given datatypes
     *
     * @details
     * the function uses TMP-techniques to evaluate the size of any of the given datatypes. If
     * the types are all integral types the function will evaluate the size at compile time
     * if there are types that implement the BinarySerializebale interface these will be evaluated
     * at runtime
     *
     * if one of the types provided is neither an integral type nor implements the BinarySerializeble
     * interface an compile time error will be invoked.
     *
     * @param first first element of the type list
     * @param rest remaining elements of the type lis
     * @return number of accumulated bytes
     */

    template <typename First, typename... Rest>
    static size_t sizeofT(First&& first, Rest&& ... rest)
    {
      return sizeofT(std::forward<First>(first)) + sizeofT(std::forward<Rest>(rest)...); // recursive call using pack expansion syntax
    }



    /** serializeT, deserializeT
     * @brief functions provides variadic memcpy capabilities
     * @details
     * Those functions helps you to serialize and deserialize datatypes that are either integral types
     * and therefore trivially serializable or implement the BinarySerializable interface and therefore
     * provide the necessary functions to be serialized
     *
     * If a type is not serializeable all functions will invoke compile time errors
     *
     * @param Endian: that tells the class if it should swap the endianess
     * when or not
     */

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // serializeT
    //////////////////////////////////////////////////////////////////////////////////////////////////
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_serializable<typename std::remove_reference<type_t>::type>::value
#ifdef USE_OPENCV
    && (!std::is_same<typename std::remove_reference<type_t>::type, cv::Mat>::value)
    && (!std::is_same<typename std::remove_reference<type_t>::type, const cv::Mat>::value)
#endif
    && !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value
    && !(traits::is_stl_array<typename std::remove_reference<type_t>::type>::value
         && traits::is_serializable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value),
         size_t
         >::type serializeT(Endian /*endian*/, char*, type_t&&)
    {
      static_assert(traits::is_serializable<typename std::remove_reference<type_t>::type>::value, "type_t can not serialized");
      return 0;
    }

    template <typename type_t>
    static typename std::enable_if <
    traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value,
           size_t >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      memcpy(pBuffer, &t, sizeof(type_t));
      if (endian == Endian::swap)
      {
        Endianness::swap_endianness<type_t>(pBuffer);
      }
      return sizeof(type_t);
    }

    template <typename type_t>
    static typename std::enable_if <
    !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value&&
    std::is_array<typename std::remove_reference<type_t>::type>::value,
        size_t
        >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      // first copy two bytes containing the size of the vector to the buffer
      uint32_t u32ContainerSize = static_cast<uint32_t>(sizeof(type_t) / sizeof(t[0]));
      size_t nSize = serializeT(endian, pBuffer, u32ContainerSize);
      for (uint32_t n = 0; n < u32ContainerSize; ++n)
      {
        nSize += serializeT(endian, pBuffer + nSize, t[n]);
      }
      return nSize;
    }


    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value,
           size_t
           >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      // first copy two bytes containing the size of the vector to the buffer
      uint32_t u32ContainerSize = static_cast<uint32_t>(t.size());
      size_t nSize = serializeT(endian, pBuffer, u32ContainerSize);
      for (const typename std::remove_reference<type_t>::type::value_type& elem : t)
      {
        nSize += serializeT(endian, pBuffer + nSize, elem);
      }
      return nSize;
    }

    template <typename type_t>
    static typename std::enable_if <
    traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value,
           size_t
           >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      uint32_t u32ContainerSize = static_cast<uint32_t>(t.size());
      size_t nSize = serializeT(endian, pBuffer, u32ContainerSize);
      for (auto elem_pair : t)
      {
        nSize += serializeT(endian, pBuffer + nSize, elem_pair.first);
        nSize += serializeT(endian, pBuffer + nSize, elem_pair.second);
      }
      return nSize;
    }

    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !std::is_array<typename std::remove_reference<type_t>::type>::value&&
    !std::is_fundamental<typename std::remove_reference<type_t>::type>::value&&
    !std::is_union<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_serializable<typename std::remove_reference<type_t>::type>::isString,
    size_t
    >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      return t.serialize(endian, pBuffer);
    }

    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !std::is_array<typename std::remove_reference<type_t>::type>::value&&
    !std::is_fundamental<typename std::remove_reference<type_t>::type>::value&&
    !std::is_union<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::isString,
           size_t
           >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      // first copy two bytes containing the size of the string
      uint32_t u32ContainerSize = static_cast<uint32_t>(t.size());
      size_t nSize = serializeT(endian, pBuffer, u32ContainerSize);
      memcpy(pBuffer + nSize, t.c_str(), u32ContainerSize);
      return nSize + u32ContainerSize;
    }

    template <typename type_t>
    static typename std::enable_if <
    traits::is_stl_array<typename std::remove_reference<type_t>::type>::value
    && traits::is_serializable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value
    && !std::is_fundamental<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value,
    size_t
    >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      size_t nSize = serializeT(endian, pBuffer, static_cast<uint32_t>(t.size()));
      for (auto elem : t)
      {
        nSize += serializeT(endian, pBuffer + nSize, elem);
      }
      return nSize;
    }

    template <typename First, typename... Rest>
    static size_t serializeT(Endian endian, char* pBuffer, First&& first, Rest&& ... rest)
    {
      const size_t nFirst = serializeT(endian, pBuffer, first);
      return nFirst + serializeT(endian, pBuffer + nFirst, std::forward<Rest>(rest)...);
    }


#ifdef USE_OPENCV
    // type cv::Mat
    template <typename type_t>
    static typename std::enable_if <
    std::is_same<typename std::remove_reference<type_t>::type, cv::Mat>::value
    || std::is_same<typename std::remove_reference<type_t>::type, const cv::Mat>::value
    , size_t >::type serializeT(Endian endian, char* pBuffer, type_t&& t)
    {
      if (endian == Endian::swap)
        CPCPW_THROW(cpcpw::Exception, "Mat only serializable for no_swap endianes");

      /* The array dimensionality */
      size_t nSize = serializeT(endian, pBuffer, static_cast<uint32_t>(t.dims));

      /* The array of integers, specifying the n-dimensional array shape */
      for (int i = 0; i < t.dims; ++i)
      {
        nSize += serializeT(endian, pBuffer + nSize, static_cast<uint32_t>(t.size[i]));
      }

      /* array depth, channels, channel size*/
      uint32_t u32Depth = static_cast<uint32_t>(t.depth());
      uint32_t u32Channels = static_cast<uint32_t>(t.channels());
      uint32_t u32ElSize = static_cast<uint32_t>(t.elemSize1());
      nSize += serializeT(endian, pBuffer + nSize, u32Depth, u32Channels, u32ElSize);

      /* copy data */
      size_t nDataSize = t.total() * t.elemSize();
      memcpy(pBuffer + nSize, t.data, nDataSize);
      nSize += nDataSize;

      return nSize;
    }
#endif


    //////////////////////////////////////////////////////////////////////////////////////////////////
    // deserializeT
    //////////////////////////////////////////////////////////////////////////////////////////////////
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
#ifdef USE_OPENCV
    !std::is_same<typename std::remove_reference<type_t>::type, cv::Mat>::value&&
#endif
    !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value
    && !(traits::is_stl_array<typename std::remove_reference<type_t>::type>::value
         && traits::is_serializable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value),
         type_t
         >::type deserializeT(Endian /*endian*/, const char*, size_t&)
    {
      static_assert(traits::is_serializable<typename std::remove_reference<type_t>::type>::value, "type_t can not serialized");
      return type_t();
    }

    template <typename type_t>
    static typename std::enable_if<traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value,
           type_t>::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      type_t t;
      memcpy(&t, pBuffer, sizeof(type_t));
      if (endian == Endian::swap)
      {
        Endianness::swap_endianness<type_t>(t);
      }
      nSize = sizeof(type_t);
      return t;
    }

    // c array
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value&&
    std::is_array<typename std::remove_reference<type_t>::type>::value,
        type_t
        >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      type_t t;
      size_t nContainerSize = sizeof(type_t) / sizeof(t[0]);
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      nSize = sizeof(uint32_t);

      if (static_cast<size_t>(u32ContainerSize) == nContainerSize)
      {
        for (uint32_t u32Cnt = 0; u32Cnt < u32ContainerSize; ++u32Cnt)
        {
          size_t nElementSize = 0;
          deserializeT(endian, pBuffer + nSize, nElementSize, t[u32Cnt]);
          nSize += nElementSize;
        }
      }

      return t;
    }

    // std::vector
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_stl_set<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    !std::is_same<typename std::remove_reference<type_t>::type, std::vector<bool>>::value,
    type_t
    >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      type_t t;
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      t.resize(u32ContainerSize);
      for (auto& elem : t)
      {
        size_t nElementSize = 0;
        deserializeT(endian, pBuffer + nSize, nElementSize, elem);
        nSize += nElementSize;
      }
      return t;
    }

    //std::vector<bool>
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_stl_set<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    std::is_same<typename std::remove_reference<type_t>::type, std::vector<bool>>::value,
        type_t
        >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      type_t t;
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      for (size_t nCnt = 0; nCnt < u32ContainerSize; nCnt++)
      {
        size_t nElementSize = 0;
        // construct the element in-place with emplace_back
        t.push_back(deserializeT<bool>(endian, pBuffer + nSize, nElementSize));
        nSize += nElementSize;
      }
      return t;
    }


    // std::set
    template <typename type_t>
    static typename std::enable_if <
    traits::is_stl_set<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value,
           type_t
           >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      type_t c;
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);

      for (uint32_t u32Cnt = 0; u32Cnt < u32ContainerSize; ++u32Cnt)
      {
        size_t nElementSize = 0;
        c.emplace(deserializeT<typename type_t::value_type>(endian, pBuffer + nSize, nElementSize));
        nSize += nElementSize;
      }
      return c;
    }


    // std::map ...
    template <typename type_t>
    static typename std::enable_if <
    traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value,
           type_t
           >::type deserializeT(Endian endian, const char* pBuffer, size_t nSize)
    {
      type_t t;
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      for (uint32_t u32Cnt = 0; u32Cnt < u32ContainerSize; ++u32Cnt)
      {
        typedef typename std::remove_reference<type_t>::type::key_type type_k;
        typedef typename std::remove_reference<type_t>::type::mapped_type type_v;
        size_t nKey = 0;
        size_t nValue = 0;
        type_k key = deserializeT<type_k>(endian, pBuffer + nSize, nKey);
        t.emplace(
          std::move(key),
          deserializeT<type_v>(endian, pBuffer + nSize + nKey, nValue));
        nSize += nKey + nValue;
      }
      return t;
    }

    // custom type with method deserialize()
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !std::is_array<typename std::remove_reference<type_t>::type>::value&&
    !std::is_fundamental<typename std::remove_reference<type_t>::type>::value&&
    !std::is_union<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_serializable<typename std::remove_reference<type_t>::type>::isString,
    type_t
    >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      return type_t::deserialize(endian, pBuffer, nSize);
    }

    // strings & const char* -> string
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_associative_container<typename std::remove_reference<type_t>::type>::value&&
    !traits::is_container<typename std::remove_reference<type_t>::type>::value&&
    !std::is_array<typename std::remove_reference<type_t>::type>::value&&
    !std::is_fundamental<typename std::remove_reference<type_t>::type>::value&&
    !std::is_union<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::value&&
    traits::is_serializable<typename std::remove_reference<type_t>::type>::isString,
           type_t
           >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      type_t t(pBuffer + nSize, u32ContainerSize);
      nSize += u32ContainerSize;
      return t;
    }


    // std::array
    template <typename type_t>
    static typename std::enable_if <
    traits::is_stl_array<typename std::remove_reference<type_t>::type>::value
    && traits::is_serializable<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value
    && !std::is_fundamental<typename traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_value_type>::value,
    type_t
    >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize)
    {
      type_t t;
      static const size_t nArraySize = traits::is_stl_array<typename std::remove_reference<type_t>::type>::array_size;
      nSize = 0;
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      if (nArraySize == static_cast<size_t>(u32ContainerSize))
      {
        for (size_t nCount = 0; nCount < u32ContainerSize; ++nCount)
        {
          size_t nElementSize = 0;
          deserializeT(endian, pBuffer + nSize, nElementSize, t[nCount]);
          nSize += nElementSize;
        }
      }
      else
      {
        nSize = 0;
      }
      return t;
    }


    // last element function needed here because of diffrent signature to other deserializeT functions
    template <typename Last>
    static typename std::enable_if <
    traits::is_trivialy_memcopyable<typename std::remove_reference<Last>::type>::value ||
    !std::is_array<typename std::remove_reference<Last>::type>::value,
    void
    >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize, Last& last)
    //void deserializeT(Endian endian, const char* pBuffer, size_t& nSize, Last& last)
    {
      last = deserializeT<Last>(endian, pBuffer, nSize);
    }

    // c array (extry impl. to avoid copy of array data)
    template <typename type_t>
    static typename std::enable_if <
    !traits::is_trivialy_memcopyable<typename std::remove_reference<type_t>::type>::value&&
    std::is_array<typename std::remove_reference<type_t>::type>::value,
        void
        >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize, type_t& t)
    {
      nSize = sizeof(uint32_t);
      size_t nContainerSize = sizeof(type_t) / sizeof(t[0]);
      uint32_t u32ContainerSize = deserializeT<uint32_t>(endian, pBuffer, nSize);
      if (static_cast<size_t>(u32ContainerSize) == nContainerSize)
      {
        for (uint32_t u32Cnt = 0; u32Cnt < u32ContainerSize; ++u32Cnt)
        {
          size_t nElementSize = 0;
          deserializeT(endian, pBuffer + nSize, nElementSize, t[u32Cnt]);
          nSize += nElementSize;
        }
      }
    }

#ifdef USE_OPENCV
    // type cv::Mat
    template <typename type_t>
    static typename std::enable_if <
    std::is_same<typename std::remove_reference<type_t>::type, cv::Mat>::value, void
    >::type deserializeT(Endian endian, const char* pBuffer, size_t& nSize, cv::Mat& t)
    {
      if (endian == Endian::swap)
        CPCPW_THROW(cpcpw::Exception, "Mat only serializable for no_swap endianes");

      /**< Get the array dimensionality */
      uint32_t u32Ndims = deserializeT<uint32_t>(endian, pBuffer, nSize);

      /**< Get the array of integers, specifying the n-dimensional array shape */
      size_t nSizeItem = 0;
      std::shared_ptr<int> pSizes(new int[u32Ndims]);

      for (uint32_t u32Cnt = 0; u32Cnt < u32Ndims; ++u32Cnt)
      {
        uint32_t u32SizeEntry = deserializeT<uint32_t>(endian, pBuffer + nSize, nSizeItem);
        pSizes.get()[u32Cnt] = u32SizeEntry; // use uint32 and push it into int array (opencv uses int)
        nSize += nSizeItem;
      }

      /* array depth, channels, channel size*/
      uint32_t u32Depth = 0U;
      uint32_t u32Channels = 0U;
      uint32_t u32ChannelSize = 0U;

      deserializeT<uint32_t>(endian, pBuffer + nSize, nSizeItem, u32Depth, u32Channels, u32ChannelSize);
      nSize += nSizeItem;

      /* create mat */
      if (2U == u32Ndims)
      {
        t.create(pSizes.get()[0], pSizes.get()[1], CV_MAKE_TYPE(u32Depth, u32Channels));
      }
      else
      {
        t.create(u32Ndims, (const int*)pSizes.get(), CV_MAKE_TYPE(u32Depth, u32Channels));
      }

      /* copy data */
      size_t nDataSize = t.total() * t.elemSize();
      memcpy(t.data, pBuffer + nSize, nDataSize);
      nSize += nDataSize;
    }
#endif

    template <typename First, typename... Rest>
    static void deserializeT(Endian endian, const char* pBuffer, size_t& nSize, First& first, Rest& ... rest)
    {
      first = deserializeT<First>(endian, pBuffer, nSize);
      size_t nRest = 0;
      deserializeT(endian, pBuffer + nSize, nRest, rest...);
      nSize += nRest;
    }

#ifdef MSVC
    /* disable warning "unreachable code" in visual studio release build */
#pragma warning(push)
#pragma warning(disable: 4702)
#endif
    /***************************************************************************
    toStringT
    ***************************************************************************/
    template <typename type_t>
    static std::string  toStringT(const type_t& value)
    {
      // forward to free function (with gcc no explicit template can be defined inside a class!)
      return intern::toStringTimpl(value);
    }

    /***************************************************************************
    fromStringT
    ***************************************************************************/
    template <typename type_t>
    static type_t fromStringT(const std::string& strData)
    {
      // forward to free function (with gcc no explicit template can be defined inside a class!)
      return intern::fromStringTimpl<type_t>(strData);
    }
#ifdef MSVC
#pragma warning(pop)
#endif
  }; // memory class

} // cpcpw



#endif // _MEMORY_BINARY_H_
