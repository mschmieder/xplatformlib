set(module_name "memory")
cpcpw_module_option(cpcpw core CPCPW_BUILD_CORE_MEMORY "build module memory" ON)

if(CPCPW_BUILD_CORE_MEMORY)
  set(MODULE_SOURCES "ArrayDeleter.h"
                     "Memory.h"
                     "MemoryBinary.h"
                     "MemoryAscii.h"
                     "MemoryAscii.cpp"
                     "Endianness.h"
                     "Endianness.cpp")
endif()

if(MODULE_SOURCES)
  set(lib_name core_${module_name})

  # add files to library
  cpcpw_add_library(${lib_name}
                    ${MODULE_SOURCES}) 

  # get all libraries that need to be linked against FWLibrary
  cpcpw_target_link_libraries(${lib_name}
                              core_exception
                              ${MODULE_LINK_LIBS})

  add_subdirectory(tests)
endif()
