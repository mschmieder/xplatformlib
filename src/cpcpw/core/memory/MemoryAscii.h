#ifndef MEMORY_ACSII_H
#define MEMORY_ACSII_H

#include <cpcpw/core/core_config.h>
#include <cpcpw/core/exception/Exception.h>
#include <cpcpw/core/memory/Endianness.h>
#include <type_traits>
#include <cpcpw/core/traits/cpcpw_traits.h>
#include <memory>
#include <algorithm>
#include <assert.h>
#include <string.h> //memcpy
#include <sstream>
#include <vector>
#include <list>
#include <map>

#if defined (CPCPW_USE_QT5)
  #ifdef min
  #undef min
  #endif
  #ifdef max
  #undef max
  #endif

#include <QString>
#include <QDateTime>
#endif

namespace cpcpw
{
    namespace intern
    {
      /***************************************************************************
      toStringTimpl
      ***************************************************************************/
      template <typename type_t>
      typename std::enable_if < !traits::has_to_string<type_t>::value&&
        (!traits::is_ascii_serializable<type_t>::value ||
        (traits::is_ascii_serializable<type_t>::value&&
        !std::is_arithmetic<type_t>::value&&
        !traits::is_container<type_t>::value&&
        !std::is_same<std::string, type_t>::value&&
#if defined (CPCPW_USE_QT5)
        !std::is_same<QString, typename std::remove_const<typename std::remove_reference<type_t>::type>::type>::value&&
        !std::is_same<QDateTime, typename std::remove_const<typename std::remove_reference<type_t>::type>::type>::value&&
#endif
        !std::is_same<const char*, type_t>::value)),
        std::string >::type
        toStringTimpl(const type_t& /*value*/)
      {
        // enum cv::Mat ... are output streamable -> is_ascii_serializable!
        CPCPW_THROW(cpcpw::Exception, "type '" << typeid(type_t).name() << "' can not be serialized to string");
      }

      template <typename type_t>
      typename std::enable_if < traits::has_to_string<type_t>::value,
        std::string >::type
        toStringTimpl(const type_t& value)
      {
        return value.toString();
      }

      template <typename type_t>
      typename std::enable_if < traits::is_ascii_serializable<type_t>::value&&
        !traits::has_to_string<type_t>::value&&
        std::is_arithmetic<type_t>::value,
        std::string >::type
        toStringTimpl(const type_t& value)
      {
        std::stringstream sstr;
        sstr << value;
        return sstr.str();
      }

      template <typename type_t>
      typename std::enable_if < std::is_same<const char*, type_t>::value,
        std::string >::type
        toStringTimpl(type_t pValue)
      {
        return std::string(pValue);
      }

#if defined (CPCPW_USE_QT5)
      template <typename type_t>
      typename std::enable_if < std::is_same<QString, typename std::remove_const<typename std::remove_reference<type_t>::type>::type>::value,
        std::string >::type
        toStringTimpl(type_t strValue)
      {
        return strValue.toStdString();
      }

      template <typename type_t>
      typename std::enable_if <
        std::is_same<QDateTime, typename std::remove_const<typename std::remove_reference<type_t>::type>::type>::value
        , std::string >::type
        toStringTimpl(type_t value)
      {
        return toStringTimpl(value.toString());
      }

#endif

      template <typename type_t>
      typename std::enable_if < std::is_same<std::string, type_t>::value,
        std::string >::type
        toStringTimpl(const type_t& strValue)
      {
        return strValue;
      }


      template <typename type_t>
      typename std::enable_if < traits::is_container<type_t>::value&&
        !traits::is_associative_container<type_t>::value&&
        traits::is_ascii_serializable<type_t>::value&&
        !std::is_same<type_t,std::vector<bool>>::value,
        std::string >::type
        toStringTimpl(const type_t& value)
      {
        std::stringstream sstr;
        for (const auto& elem : value)
        {
          sstr << toStringTimpl(elem) << ";";
        }
        return sstr.str();
      }

    template <typename type_t>
    typename std::enable_if < std::is_same<type_t,std::vector<bool>>::value,std::string >::type
      toStringTimpl(const type_t& value)
    {
      std::stringstream sstr;
      for (const bool& elem : value)
      {
        sstr << toStringTimpl(elem) << ";";
      }
      return sstr.str();
    }



      template <typename type_t>
      typename std::enable_if <
        traits::is_associative_container<type_t>::value,
        std::string >::type
        toStringTimpl(const type_t& value)
      {
        std::stringstream sstr;
        for (auto elem_pair : value)
        {
          sstr << "<" << toStringTimpl(elem_pair.first) << ";" << toStringTimpl(elem_pair.second) << ">;";
        }
        return sstr.str();
      }

      template <>
      CPCPW_CORE_MEMORY_DECL std::string toStringTimpl<int8_t>(const int8_t& value);

      template <>
      CPCPW_CORE_MEMORY_DECL std::string toStringTimpl<uint8_t>(const uint8_t& value);

      template <>
      CPCPW_CORE_MEMORY_DECL std::string toStringTimpl<float>(const float& value);

      template <>
      CPCPW_CORE_MEMORY_DECL std::string toStringTimpl<double>(const double& value);

      /***************************************************************************
        fromStringTimpl
        ***************************************************************************/
      template <typename type_t>
      typename std::enable_if < !traits::has_from_string<type_t>::value&&
        !std::is_arithmetic<type_t>::value&&
        !traits::is_container<type_t>::value, type_t >::type
        fromStringTimpl(const std::string& /*strData*/)
      {
        // todo assert
        CPCPW_THROW(cpcpw::Exception, "type '" << typeid(type_t).name() << "' can not be deserialized from string");
      }

      template <typename type_t>
      typename std::enable_if < traits::has_from_string<type_t>::value,
        type_t>::type
        fromStringTimpl(const std::string& strData)
      {
        return type_t::fromString(strData);
      }

      template <typename type_t>
      typename std::enable_if < !traits::has_from_string<type_t>::value&&
        std::is_arithmetic<type_t>::value,
        type_t >::type
        fromStringTimpl(const std::string& strData)
      {
        type_t data;
        std::stringstream sstr;
        sstr << strData;
        sstr >> data;
        return data;
      }

      template <>
      CPCPW_CORE_MEMORY_DECL uint8_t fromStringTimpl<uint8_t>(const std::string& strData);

      template <>
      CPCPW_CORE_MEMORY_DECL int8_t fromStringTimpl<int8_t>(const std::string& strData);

      template <>
      CPCPW_CORE_MEMORY_DECL std::string fromStringTimpl<std::string>(const std::string& strData);

      template <typename type_t>
      typename std::enable_if < !traits::has_from_string<type_t>::value&&
        traits::is_container<type_t>::value&&
        !traits::is_associative_container<type_t>::value&&
        !std::is_same<std::vector<bool>, type_t>::value,
        type_t >::type
        fromStringTimpl(const std::string& strData)
      {
        size_t nCountElements = std::count(strData.begin(), strData.end(), ';');
        type_t container(nCountElements);
        size_t nPosStart = 0;
        size_t nPosEnd = strData.find(";");
        for (typename std::remove_reference<type_t>::type::value_type& elem : container)
        {
          elem = fromStringTimpl<typename type_t::value_type>(strData.substr(nPosStart, nPosEnd - nPosStart));
          nPosStart = nPosEnd + 1; // skip seperator
          nPosEnd = strData.find(";", nPosStart);
        }
        return container;
      }

      template <typename type_t>
      typename std::enable_if < !traits::has_from_string<type_t>::value&&
        traits::is_container<type_t>::value&&
        !traits::is_associative_container<type_t>::value&&
        std::is_same<std::vector<bool>, type_t>::value,
        type_t >::type
        fromStringTimpl(const std::string& strData)
      {
        size_t nCountElements = std::count(strData.begin(), strData.end(), ';');
        std::vector<bool> container;
        size_t nPosStart = 0;
        size_t nPosEnd = strData.find(";");
        for (size_t nIdx = 0; nIdx < nCountElements; ++nIdx)
        {
          container.push_back(fromStringTimpl<bool>(strData.substr(nPosStart, nPosEnd - nPosStart)));
          nPosStart = nPosEnd + 1; // skip seperator
          nPosEnd = strData.find(";", nPosStart);
        }
        return container;
      }


      template <typename type_t>
      typename std::enable_if < !traits::has_from_string<type_t>::value&&
        traits::is_associative_container<type_t>::value,
        type_t >::type
        fromStringTimpl(const std::string& strData)
      {
        typedef typename std::remove_reference<type_t>::type::key_type k_type;
        typedef typename std::remove_reference<type_t>::type::mapped_type v_type;

        type_t ac;
        size_t nCountElements = std::count(strData.begin(), strData.end(), '>');
        size_t nPosPairBegin = strData.find("<");
        size_t nPosPairEnd = strData.find(">", nPosPairBegin);
        for (size_t u32Cnt = 0; u32Cnt < nCountElements; ++u32Cnt)
        {
          std::string strPair = strData.substr(nPosPairBegin, nPosPairEnd - nPosPairBegin);
          size_t nPosSep = strPair.find(";");
          std::string strKey = strPair.substr(1, nPosSep - 1);
          std::string strValue = strPair.substr(nPosSep + 1);
          ac.insert(std::make_pair(fromStringTimpl<k_type>(strKey), fromStringTimpl<v_type>(strValue)));
          nPosPairBegin = strData.find("<", nPosPairEnd + 1);
          nPosPairEnd = strData.find(">", nPosPairBegin + 1);
        }
        return ac;
      }
    } // intern
  } // cpcpw




#endif // MEMORY_ASCII_H
