#ifndef __ENDIANNESS_H__
#define __ENDIANNESS_H__

#include <cpcpw/core/core_config.h>

#include <memory>
#include <type_traits>
#include <functional>
#include <algorithm>

namespace cpcpw {
  /** @enum Endian
  ***************************************************************************
  enum class endian defines if data must be swap or not
  this enum can be extented for other kind if endianess handling if
  it is necessary
  ***************************************************************************/
  enum class Endian
  {
    swap,
    no_swap
  };


  /** @class Endianness
  ******************************************************************************************************
  @brief
  Class that enables you to find out the system endianness at runtime. There are also some
  functions that allow you to easily swap the endianness of an integral/POD data type

  uint32_t myInt32 = 10;
  Endianness::swap_endianness(myInt32); // inplace swapping

  uint16_t myInt16 = 255;
  uint16_t mySwappedInt16;
  Endianness::swap_endianness(myInt16,mySwappedInt16);


  @author schmieder (2012)
  *****************************************************************************************************/
  class CPCPW_CORE_MEMORY_DECL Endianness
  {
  public:
    template <typename type_t>
    static void swap_endianness(type_t& obj)
    {
      int8_t* objAddress = reinterpret_cast<int8_t*>(&obj);
      std::reverse(objAddress, objAddress + sizeof(type_t));
    }

    template <typename type_t>
    static void swap_endianness(char* obj)
    {
      std::reverse(obj, obj + sizeof(type_t));
    }

    template <typename type_t>
    static void swap_endianness(const type_t& obj, type_t& objSwaped)
    {
      objSwaped = obj;
      Endianness::swap_endianness(objSwaped);
    }

    static bool isLittleEndianSystem();
    static bool isBigEndianSystem();
  };
} // namespace cpcpw

#endif
