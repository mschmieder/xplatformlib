#-------------------------------------------------------------------------------------------
# debug macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(debug)
  option(CPCPW_ENABLE_CMAKE_DEBUG_OUTPUT "enables debug output on cmake run" OFF)
  mark_as_advanced(CPCPW_ENABLE_CMAKE_DEBUG_OUTPUT)
  if(CPCPW_ENABLE_CMAKE_DEBUG_OUTPUT)
    message("${ARGN}")
  endif()
endmacro(debug)

#-------------------------------------------------------------------------------------------
# cpcpw_status macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(cpcpw_status)
  message(STATUS ${ARGN})
endmacro(cpcpw_status)

#-------------------------------------------------------------------------------------------
# list_append_if_defined macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(list_append_if_defined var_out)
  foreach(VAR ${ARGN})
    if(DEFINED "${VAR}")
      list(APPEND ${var_out} "${${VAR}}" )
    endif()
  endforeach()
endmacro(list_append_if_defined)

#-------------------------------------------------------------------------------------------
# list_append_target_if_defined macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(list_append_target_if_defined var_out)
  foreach(VAR ${ARGN})
    if(TARGET ${VAR})
      list(APPEND ${var_out} "${VAR}" )
    endif()
  endforeach()
endmacro(list_append_target_if_defined)


#-------------------------------------------------------------------------------------------
# cpcpw_add_compiler_flags macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro( cpcpw_add_compiler_flags )
  if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  elseif(MSVC)
    # ENABLE PARALLEL BUILDS IN VISUAL STUDIO AND C++ EXCEPTIONS
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP /bigobj /EHsc")

    cpcpw_option( ${PROJECT_NAME}  
                  CPCPW_ENABLE_PROFILING
                  "enables instrumentation of binaries in RelWithDebInfo mode using visual studio. Adds /PROFILE to EXE_LINKER_FLAGS, MODULE_LINKER_FLAGS and SHARED_LINKER_FLAGS"
                  OFF)

    if(CPCPW_ENABLE_PROFILING)
      set(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO    "${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO} /PROFILE")
      set(CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO} /PROFILE")
      set(CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO} /PROFILE")

      # remove potential /INCREMENTAL:YES /INCREMENTAL /INCREMENTAL:NO
      string(REPLACE "/INCREMENTAL:YES" "/INCREMENTAL:NO" CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO    ${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO})
      string(REPLACE "/INCREMENTAL:YES" "/INCREMENTAL:NO" CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO ${CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO})
      string(REPLACE "/INCREMENTAL:YES" "/INCREMENTAL:NO" CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO ${CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO})

      string(REPLACE "/INCREMENTAL " "/INCREMENTAL:NO " CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO    ${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO})
      string(REPLACE "/INCREMENTAL " "/INCREMENTAL:NO " CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO ${CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO})
      string(REPLACE "/INCREMENTAL " "/INCREMENTAL:NO " CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO ${CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO})
    endif()
  endif()
endmacro(cpcpw_add_compiler_flags)

#-------------------------------------------------------------------------------------------
# cpcpw_find_package macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(cpcpw_find_package var_package_name)
  find_package(${var_package_name} ${ARGN})

  string(TOUPPER ${var_package_name} _package_uc_)
  string(TOLOWER ${var_package_name} _package_lc_)

  # BINARY DIRECTORIES
  set(_package_binary_dirs_)
  list_append_if_defined(_package_binary_dirs_
                         ${_package_uc_}_BINARY_DIR
                         ${_package_uc_}_BIN_DIR 
                         ${_package_lc_}_BINARY_DIR
                         ${_package_lc_}_BIN_DIR 
                         ${var_package_name}_BINARY_DIR
                         ${var_package_name}_BIN_DIR)

  set(_package_library_dirs_)
  list_append_if_defined(_package_library_dirs_
                         ${_package_uc_}_LIBRARY_DIR
                         ${_package_uc_}_LIBRARY_DIRECTORY 
                         ${_package_uc_}_LIB_DIR
                         ${_package_ul_}_LIBRARY_DIR
                         ${_package_ul_}_LIBRARY_DIRECTORY 
                         ${_package_ul_}_LIB_DIR
                         ${var_package_name}_LIBRARY_DIR
                         ${var_package_name}_LIBRARY_DIRECTORY 
                         ${var_package_name}_LIB_DIR)

  set(_package_include_dirs)
  list_append_if_defined(_package_include_dirs
                         ${_package_uc_}_INCLUDE_DIR
                         ${_package_uc_}_INCLUDE_DIRECTORY
                         ${_package_uc_}_INCLUDE_DIRECTORIES
                         ${_package_uc_}_INCLUDE_DIRS
                         ${_package_lc_}_INCLUDE_DIR
                         ${_package_lc_}_INCLUDE_DIRECTORY
                         ${_package_lc_}_INCLUDE_DIRECTORIES
                         ${_package_lc_}_INCLUDE_DIRS
                         ${var_package_name}_INCLUDE_DIR
                         ${var_package_name}_INCLUDE_DIRECTORY
                         ${var_package_name}_INCLUDE_DIRECTORIES
                         ${var_package_name}_INCLUDE_DIRS)

  set(_package_targets_)
  list_append_target_if_defined(_package_targets_
                                 ${var_package_name}
                                 ${var_package_name}::${var_package_name}
                                 ${var_package_name}::Main )

  if(NOT _package_targets_)
    set(_package_libraries_)
    list_append_if_defined(_package_libraries_
                           ${_package_uc_}_LIBRARIES
                           ${_package_uc_}_LIBS 
                           ${_package_uc_}_BOTH_LIBRARIES
                           ${_package_uc_}_MAIN_LIBRARIES
                           ${_package_lc_}_LIBRARIES
                           ${_package_lc_}_LIBS 
                           ${_package_lc_}_BOTH_LIBRARIES
                           ${_package_lc_}_MAIN_LIBRARIES
                           ${var_package_name}_LIBRARIES
                           ${var_package_name}_LIBS 
                           ${var_package_name}_BOTH_LIBRARIES
                           ${var_package_name}_MAIN_LIBRARIES)
  endif()

  # check if libraries are linked with full file path
  foreach(lib_path ${_package_libraries_})
    if(NOT TARGET ${lib_path})
      get_filename_component(_link_dir "${lib_path}" DIRECTORY)
      debug("${_link_dir}")
      if( _link_dir )
        debug("${_link_dir}")
        list(APPEND _package_library_dirs_ ${_link_dir})
      endif()
    else()
      list(APPEND _package_targets_ ${lib_path})
    endif()
  endforeach()

  set(_binaries)
  set(_target_lib_names)
  if(_package_targets_)
    foreach(target ${_package_targets_})
      get_target_property(_libs    ${target} IMPORTED_LOCATION)
      get_target_property(_libsrel ${target} IMPORTED_LOCATION_RELEASE)
      get_target_property(_libsdbg ${target} IMPORTED_LOCATION_DEBUG)

      if(_libs)
        foreach(lib ${_libs})
           get_filename_component(lib_name "${lib}" NAME_WE)
           get_filename_component(lib_ext  "${lib}" EXT)
           list(APPEND _target_lib_names ${lib_name})
           if(lib_ext STREQUAL ".dll" OR lib_ext STREQUAL ".so" OR lib_ext STREQUAL ".dylib")
             list(APPEND _binaries ${lib})
           endif()
        endforeach()   
      endif()
      if(_libsrel)
        foreach(lib ${_libsrel})
           get_filename_component(lib_name "${lib}" NAME_WE)
           get_filename_component(lib_ext  "${lib}" EXT)
           list(APPEND _target_lib_names ${lib_name})
           if(lib_ext STREQUAL ".dll" OR lib_ext STREQUAL ".so" OR lib_ext STREQUAL ".dylib")
             list(APPEND _binaries ${lib})
           endif()
        endforeach()   
      endif()
      if(_libsdbg)
        foreach(lib ${_libsdbg})
           get_filename_component(lib_name "${lib}" NAME_WE)
           get_filename_component(lib_ext  "${lib}" EXT)
           list(APPEND _target_lib_names ${lib_name})
           if(lib_ext STREQUAL ".dll" OR lib_ext STREQUAL ".so" OR lib_ext STREQUAL ".dylib")
             list(APPEND _binaries ${lib})
           endif()
        endforeach()   
      endif()
    endforeach()
  else()
    foreach(lib ${_package_libraries_})
      get_filename_component(lib_name "${lib}" NAME_WE)
      get_filename_component(lib_ext  "${lib}" EXT)
      list(APPEND _target_lib_names ${lib_name})
      if(lib_ext STREQUAL ".dll" OR lib_ext STREQUAL ".so" OR lib_ext STREQUAL ".dylib")
        list(APPEND _binaries ${lib})
      endif()
    endforeach()   
  endif()

  if(NOT _binaries)
    foreach(lib ${_target_lib_names})
      string(FIND ${lib} ".dll" _is_dll REVERSE)
      string(FIND ${lib} ".so" _is_so REVERSE)
      string(FIND ${lib} ".dylib" _is_dylib REVERSE)
      if(_is_dll GREATER 0 OR _is_so GREATER 0 OR _is_dylib GREATER 0 )
        list(APPEND _binaries ${lib})
      else()
        if(${_package_uc_}_ROOT)
          file(GLOB_RECURSE dll_files   "${${_package_uc_}_ROOT}/*.dll" )
          file(GLOB_RECURSE so_files    "${${_package_uc_}_ROOT}/*.so" )
          file(GLOB_RECURSE dylib_files "${${_package_uc_}_ROOT}/*.dylib" )
        elseif(${var_package_name}_ROOT)
          file(GLOB_RECURSE dll_files   "${${var_package_name}_ROOT}/*.dll" )
          file(GLOB_RECURSE so_files    "${${var_package_name}_ROOT}/*.so" )
          file(GLOB_RECURSE dylib_files "${${var_package_name}_ROOT}/*.dylib" )
        endif()
        if(dll_files OR so_files OR dylib_files)
          list(APPEND _binaries ${dll_files} ${so_files} ${dylib_files})
        endif()
      endif()
    endforeach()   
  endif()
  foreach(bin ${_binaries})
    get_filename_component(bin_path "${bin}" PATH)
    list(APPEND _package_binary_dirs_ ${bin_path})
  endforeach()  

  if(_package_library_dirs_)
    list(REMOVE_DUPLICATES _package_library_dirs_)
  endif()
  if(_package_binary_dirs_)
    list(REMOVE_DUPLICATES _package_binary_dirs_)
  endif()
  if(_package_include_dirs)
    list(REMOVE_DUPLICATES _package_include_dirs)
  endif()

  cpcpw_status("---------------------------------------------------------------------")
  cpcpw_status("Package: ${var_package_name}")
  cpcpw_status("---------------------------------------------------------------------")
  cpcpw_status("INCLUDE-DIRECTORIES:")
if(_package_include_dirs)
  foreach(incdir ${_package_include_dirs})
  cpcpw_status("  + ${incdir}")
  endforeach()
endif()
if(_package_binary_dirs_)
  cpcpw_status("BINARY-DIRECTORIES:")
  foreach(bindir ${_package_binary_dirs_})
  cpcpw_status("  + ${bindir}")
  endforeach()
endif()
if(_package_library_dirs_)
  cpcpw_status("LINK-DIRECTORIES:")
  foreach(link ${_package_library_dirs_})
  cpcpw_status("  + ${link}")
  endforeach()
endif()
if(NOT _package_targets_)
  cpcpw_status("LIBRARIES:")
  foreach(lib ${_package_libraries_})
  cpcpw_status("  + ${lib}")
  endforeach()
else()
cpcpw_status("TARGETS:")
  foreach(lib ${_package_targets_})
  cpcpw_status("  + ${lib}")
  endforeach()
endif()
  cpcpw_status("---------------------------------------------------------------------")

  if(_package_include_dirs)
    debug("adding include_directories: ${_package_include_dirs}")
    include_directories(${_package_include_dirs})
  endif()
  
  if(_package_link_dirs_)
    debug("adding link_directories: ${_package_link_dirs_}")
    link_directories(${_package_link_dirs_})
  endif()

if(NOT _package_targets_)
  list(APPEND CPCPW_THIRDPARTY_LIBRARIES          ${_package_libraries_})
else()
  list(APPEND CPCPW_THIRDPARTY_LIBRARIES          ${_package_targets_})
endif()

list(APPEND CPCPW_THIRDPARTY_BINARY_DIRECTORIES ${_package_binary_dirs_} ${_package_library_dirs_})
list(REMOVE_DUPLICATES CPCPW_THIRDPARTY_BINARY_DIRECTORIES)

endmacro(cpcpw_find_package)

#-------------------------------------------------------------------------------------------
# cpcpw_export_thirdparty_binary_dirs macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------

macro(cpcpw_export_thirdparty_binary_dirs file_path)
  set(_file_content)
  foreach(dir ${CPCPW_THIRDPARTY_BINARY_DIRECTORIES})
    set(_file_content "${_file_content}${dir}\n${content}")
  endforeach()
  debug("cpcpw_export_thirdparty_binary_dirs:")
  debug("-> content: ${_file_content}")
  file(WRITE ${file_path} ${_file_content})
endmacro(cpcpw_export_thirdparty_binary_dirs)

#-------------------------------------------------------------------------------------------
# cpcpw_export_include_dirs macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(cpcpw_export_include_dirs file_path)
  get_property(_dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
  set(_file_content)  
  list(REMOVE_DUPLICATES _dirs)
  foreach(_dir ${_dirs})
    set(_file_content "${_dir}\n${_file_content}")
  endforeach()

  debug("cpcpw_export_thirdparty_include_dirs:")
  debug("-> content: ${_file_content}")
  file(WRITE ${file_path} ${_file_content})
endmacro(cpcpw_export_include_dirs)

#-------------------------------------------------------------------------------------------
# cpcpw_find_packages macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(cpcpw_find_packages var_pack_definitions_file var_search_dir)
  file(READ "${var_pack_definitions_file}" file_content)

  # Convert file contents into a CMake list (where each element in the list
  # is one line of the file)
  string(REGEX REPLACE ";" "\\\\;" file_content "${file_content}")
  string(REGEX REPLACE "\n" ";" file_content "${file_content}")

  cpcpw_status("Loading packages from ${var_pack_definitions_file}:")  
  
  foreach(line ${file_content})
    # skipt lines that start with
    string(FIND ${line} "#" POSITION)
    if( NOT ${POSITION} EQUAL 0 )
      string(REPLACE " " ""  PACKAGE_COMPONENTS ${line})
      string(REPLACE ":" ";" PACKAGE_COMPONENTS ${PACKAGE_COMPONENTS})

      list(LENGTH PACKAGE_COMPONENTS _length_)
    
      if(${_length_} GREATER 0)
        list(GET PACKAGE_COMPONENTS 0 _first_element)
        if( ${_first_element} STREQUAL "local" )
          list(REMOVE_AT PACKAGE_COMPONENTS 0)
          list(GET PACKAGE_COMPONENTS 0 _package_name)
        elseif( ${_first_element} STREQUAL "maven" )
          list(GET PACKAGE_COMPONENTS 1 _artifact_group)
          list(GET PACKAGE_COMPONENTS 2 _artifact_packaging)
          list(GET PACKAGE_COMPONENTS 3 _artifact_name)
          list(GET PACKAGE_COMPONENTS 4 _artifact_version)

          string(TOLOWER ${_artifact_name}            _artifact_name_lc)
          string(TOLOWER ${CPCPW_COMPILER_ID}         _compiler_lc)
          string(TOLOWER ${CPCPW_TARGET_SYSTEM}       _target_system_lc)
          string(TOLOWER ${CPCPW_TARGET_ARCHITECTURE} _target_architecture_lc)

          set(_artifact_group_complete "${_artifact_group}.${_target_system_lc}.${_target_architecture_lc}.${_compiler_lc}")

          debug("cpcpw_find_packages::maven_download_artifact:")
          debug(" -> ARTIFACT_NAME      ${_artifact_name_lc} ")
          debug(" -> ARTIFACT_VERSION   ${_artifact_version}")
          debug(" -> ARTIFACT_GROUP     ${_artifact_group_complete} ")
          debug(" -> ARTIFACT_PACKAGING ${_artifact_packaging}")
          debug(" -> REPO_URL           ${MAVEN_REPOSITORY}/thirdparty")
          debug(" -> MAVEN_HOME         ${MAVEN_HOME}")
          debug(" -> INSTALL_DIRECTORY  ${var_search_dir}")

          cpcpw_maven_download_artifact(ARTIFACT_NAME      ${_artifact_name_lc} 
                                        ARTIFACT_VERSION   ${_artifact_version}
                                        ARTIFACT_GROUP     ${_artifact_group_complete} 
                                        ARTIFACT_PACKAGING ${_artifact_packaging}
                                        REPO_URL           ${MAVEN_REPOSITORY}/thirdparty
                                        MAVEN_HOME         ${MAVEN_HOME}
                                        INSTALL_DIRECTORY  ${var_search_dir})

          list(REMOVE_AT PACKAGE_COMPONENTS 0 1 2)
          list(GET PACKAGE_COMPONENTS 0 _package_name)
       endif()
      endif()
    
      if( "${_package_name}" MATCHES "Qt5" )
        set(_package_name_upper_case_ ${_package_name})
        set(_package_name_lower_case_ ${_package_name})
      else()
        string(TOUPPER ${_package_name} _package_name_upper_case_)
        string(TOLOWER ${_package_name} _package_name_lower_case_)
      endif()

      cpcpw_option(${PROJECT_NAME} USE_${_package_name_upper_case_} "enable use of ${_package_name}" ON)
      if(USE_${_package_name_upper_case_})
        set(${_package_name_upper_case_}_ROOT "${var_search_dir}/${_package_name_lower_case_}" )
        if(NOT ${_package_name}_FOUND) 
          cpcpw_find_package(${PACKAGE_COMPONENTS})
        endif()
      endif()
    endif()
  endforeach()

endmacro(cpcpw_find_packages)

#-------------------------------------------------------------------------------------------
# add_target_property macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   this macro is a wrapper for cmake's own set_target_properties macro which does not have
#   the flexibility to simply 'add' a property. To add to a targets property we need TOUPPER
#   first get the current value and append the new value to it
# USAGE:
#   target:  target name
#   FLAG:    not used
#   prop1:   name of the property
#   value1:  value
# AUTHOR:
#   Matthias Schmieder
#-------------------------------------------------------------------------------------------
macro(cpcpw_add_target_properties target FLAG prop1 value1)
  get_target_property(CURRENT_PROPERTIES ${target} ${prop1})
  if(NOT CURRENT_PROPERTIES)
    set(CURRENT_PROPERTIES "${value1}")
  else()
    set(CURRENT_PROPERTIES "${CURRENT_PROPERTIES} ${value1}")
  endif()
  set_target_properties(${target}
    PROPERTIES
    ${prop1} ${CURRENT_PROPERTIES})
endmacro(cpcpw_add_target_properties)

#-------------------------------------------------------------------------------------------
# cpcpw_read_library_version macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   reads the library version from a file. The function will parse only lines that do
#   not start with '#''
# USAGE:
#   VAR_VERSION_MAJOR:  will hold major version
#   VAR_VERSION_MINOR:  will hold minor version
#   VAR_VERSION_PATCH:  will hold patch version
#   VERSION_FILE_PATH:  path to the version file
# EXAMPLE:
#   MACRO_TEMPLATE(arg1 arg2)
# AUTHOR:
#   your name
#-------------------------------------------------------------------------------------------
macro(cpcpw_read_library_version VAR_VERSION_MAJOR VAR_VERSION_MINOR VAR_VERSION_PATCH VERSION_FILE_PATH)
  if(NOT ${ARGC} EQUAL 4)
    message(FATAL_ERROR "Macro READ_LIBRARY_VERSION requires 2 arguments but ${ARGC} given.")
  endif()

  FILE(READ "${VERSION_FILE_PATH}" file_content)

  # Convert file contents into a CMake list (where each element in the list
  # is one line of the file)
  STRING(REGEX REPLACE ";" "\\\\;" file_content "${file_content}")
  STRING(REGEX REPLACE "\n" ";" file_content "${file_content}")
  
  foreach(line ${file_content})
    # skipt lines that start with
    string(FIND ${line} "#" POSITION)
    if( NOT ${POSITION} EQUAL 0 )
      string(REGEX MATCHALL "[0-9]+" VERSION_COMPONENTS ${line})
      list(GET VERSION_COMPONENTS 0 ${VAR_VERSION_MAJOR})
      list(GET VERSION_COMPONENTS 1 ${VAR_VERSION_MINOR})
      list(GET VERSION_COMPONENTS 2 ${VAR_VERSION_PATCH})
      break()
    endif()
  endforeach()
  
  # your functionality
endmacro(cpcpw_read_library_version)


#-------------------------------------------------------------------------------------------
# cpcpw_define macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   adds an option that will be put to cpcpw_config.h
# USAGE:
#   var       :  variable
#   descrition:  option description
#   value     :  either TRUE or FALSE
#-------------------------------------------------------------------------------------------
macro(cpcpw_define GROUP VAR DESCRIPTION VALUE)
  if ( ${VALUE} STREQUAL "ON" OR ${VALUE} STREQUAL "OFF")
    option(${VAR} ${DESCRIPTION} ${VALUE})
  else()
    set(${VAR} ${VALUE} CACHE STRING ${DESCRIPTION})
  endif()

  set_property(GLOBAL APPEND PROPERTY "PROP_${GROUP}_DEFINE" "${VAR}" )
  set_property(GLOBAL PROPERTY "${VAR}_DESCRIPION" "${DESCRIPTION}" )
endmacro(cpcpw_define)

macro(cpcpw_module_define GROUP MODULE VAR DESCRIPTION VALUE)
  set(${VAR} ${VALUE} CACHE STRING ${DESCRIPTION})
  set_property(GLOBAL APPEND PROPERTY "PROP_${GROUP}_${MODULE}_DEFINE" "${VAR}" )
  set_property(GLOBAL PROPERTY "${VAR}_DESCRIPION" "${DESCRIPTION}" )
endmacro(cpcpw_module_define)

#-------------------------------------------------------------------------------------------
# cpcpw_get_defines macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#-------------------------------------------------------------------------------------------
macro(cpcpw_get_defines GROUP DEFINES_OUT)
  get_property(${DEFINES_OUT} GLOBAL PROPERTY "PROP_${GROUP}_DEFINE")
  if(${DEFINES_OUT})
    list(REMOVE_DUPLICATES ${DEFINES_OUT})
  endif()  
endmacro(cpcpw_get_defines)

macro(cpcpw_get_module_defines GROUP MODULE DEFINES_OUT)
  get_property(${DEFINES_OUT} GLOBAL PROPERTY "PROP_${GROUP}_${MODULE}_DEFINE")
  if(${DEFINES_OUT})
    list(REMOVE_DUPLICATES ${DEFINES_OUT})
  endif()  
endmacro(cpcpw_get_module_defines)

#-------------------------------------------------------------------------------------------
# CPCPW_OPTION macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   adds an option to your cmake project. This option will be stored in a property so you can
#   iterate over them.
# USAGE:
#   var       :  variable
#   descrition:  option description
#   value     :  either TRUE or FALSE
#-------------------------------------------------------------------------------------------
macro(cpcpw_option GROUP VARIABLE_NAME DESCRIPTION VALUE)
  option(${VARIABLE_NAME} ${DESCRIPTION} ${VALUE})
  set_property(GLOBAL APPEND PROPERTY "PROP_${GROUP}_OPTIONS" "${VARIABLE_NAME}" )
  set_property(GLOBAL PROPERTY "${VARIABLE_NAME}_DESCRIPION" "${DESCRIPTION}" )
endmacro(cpcpw_option)

macro(cpcpw_module_option GROUP MODULE VARIABLE_NAME DESCRIPTION VALUE)
  option(${VARIABLE_NAME} ${DESCRIPTION} ${VALUE})
  set_property(GLOBAL APPEND PROPERTY "PROP_${GROUP}_${MODULE}_OPTIONS" "${VARIABLE_NAME}" )
  set_property(GLOBAL PROPERTY "${VARIABLE_NAME}_DESCRIPION" "${DESCRIPTION}" )
endmacro(cpcpw_module_option)


#-------------------------------------------------------------------------------------------
# cpcpw_get_options macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#-------------------------------------------------------------------------------------------
macro(cpcpw_get_options GROUP OPTIONS_OUT)
  get_property(${OPTIONS_OUT} GLOBAL PROPERTY "PROP_${GROUP}_OPTIONS")
  if(${OPTIONS_OUT})
    list(REMOVE_DUPLICATES ${OPTIONS_OUT})
  endif()  
endmacro(cpcpw_get_options)

macro(cpcpw_get_module_options GROUP MODULE OPTIONS_OUT)
  get_property(${OPTIONS_OUT} GLOBAL PROPERTY "PROP_${GROUP}_${MODULE}_OPTIONS")
  if(${OPTIONS_OUT})
    list(REMOVE_DUPLICATES ${OPTIONS_OUT})
  endif()  
endmacro(cpcpw_get_module_options)


#-------------------------------------------------------------------------------------------
# cpcpw_get_libraries macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#-------------------------------------------------------------------------------------------
macro(cpcpw_get_libraries library_group libraries_out)
  get_property(${libraries_out} GLOBAL PROPERTY "${library_group}_libraries")
  if(${libraries_out})
    list(REMOVE_DUPLICATES ${libraries_out})
  endif()  
endmacro(cpcpw_get_libraries)

macro(cpcpw_get_modules module_group modules_out)
  get_property(${modules_out} GLOBAL PROPERTY "${module_group}_modules")
  if(${modules_out})
    list(REMOVE_DUPLICATES ${modules_out})
  endif()  
endmacro(cpcpw_get_modules)


#-------------------------------------------------------------------------------------------
# cpcpw_enable_warning_as_error macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   prepare setting to handle compiler warnings as errors
# EXAMPLE:
#   cpcpw_enable_warning_as_error(${WARNING_AS_ERROR})
# AUTHOR:
#   vw
#-------------------------------------------------------------------------------------------
macro(cpcpw_enable_warning_as_error)
  if(MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
    string(REPLACE "/W3" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
    string(REPLACE "/W2" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
    string(REPLACE "/W1" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
    string(REPLACE "/W0" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
    if(${warning_as_error})
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /WX")
    endif()
  elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_CLANG)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-unused-parameter -Wno-error=deprecated-declarations -Wno-error=strict-overflow -Wno-unknown-pragmas -Wno-conversion-null -Wno-error=comment")
    if(${warning_as_error})
     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
    endif()
  endif()
endmacro(cpcpw_enable_warning_as_error)


#-------------------------------------------------------------------------------------------
# cpcpw_enable_openmp macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   find OpenMp and do preparation
# EXAMPLE:
#   cpcpw_enable_openmp(${USE_OPENMP})
# AUTHOR:
#   vw
#-------------------------------------------------------------------------------------------
macro(cpcpw_enable_openmp)
  message(STATUS "Check for compiler OpenMP support...")
  include(CheckFunctionExists)
  set(OPENMP_FLAG)
  set(OPENMP_FLAG_FOUND FALSE)
  set(
    OPENMP_FLAGS
    "-openmp"   # cl, icc (/ and - works both)
    "/openmp"   # the above shoudl work, too.
    "-fopenmp"  # gcc
    )

  set(OPENMP_FUNCTION omp_set_num_threads)
  foreach(FLAG ${OPENMP_FLAGS})
    if(NOT OPENMP_FLAG_FOUND)
      set(CMAKE_REQUIRED_FLAGS ${FLAG})
      CHECK_FUNCTION_EXISTS(${OPENMP_FUNCTION} OPENMP_FUNCTION_${FLAG}_FOUND)
      if(OPENMP_FUNCTION_${FLAG}_FOUND)
        set(OPENMP_FLAG ${FLAG})
        set(OPENMP_FLAG_FOUND TRUE)
        set(OPENMP_FOUND TRUE)
      endif(OPENMP_FUNCTION_${FLAG}_FOUND)
    endif(NOT OPENMP_FLAG_FOUND)
  endforeach(FLAG)

  if(OPENMP_FOUND)
    if(CMAKE_COMPILER_IS_CLANG)
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -openmp")
    else()
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OPENMP_FLAG}")
    endif()
    message(STATUS "OpenMP supported by compiler.")
  else()
    message(STATUS "OpenMP required but no supporting compiler flags found.")
  endif()
endmacro(cpcpw_enable_openmp)


#-------------------------------------------------------------------------------------------
# cpcpw_make_declspec macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#-------------------------------------------------------------------------------------------
macro(cpcpw_make_declspec library_group declspec_prefix module_name declspec_txt_var )
  cpcpw_get_modules( ${library_group} libs )
  list(APPEND ${declspec_txt_var} "#if defined (WIN32) && defined(BUILD_SHARED_LIBS) \n")
  foreach(lib ${libs})
    string(FIND ${lib} ${module_name} FOUND_MODULE)
    if(NOT ${FOUND_MODULE} LESS 0)
      set(module ${lib})
      string(TOUPPER ${module} MODULE)
      set(${declspec_txt_var} "${${declspec_txt_var}}#  if defined (${lib}_EXPORTS) \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#   if !defined(${declspec_prefix}${MODULE}_DECL) \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#     define ${declspec_prefix}${MODULE}_DECL __declspec(dllexport) \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#   endif \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#  endif \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#  if !defined (${declspec_prefix}${MODULE}_DECL) \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#     define ${declspec_prefix}${MODULE}_DECL __declspec(dllimport) \n")
      set(${declspec_txt_var} "${${declspec_txt_var}}#  endif \n")
    endif()
  endforeach()
  set(${declspec_txt_var} "${${declspec_txt_var}}#else \n")
  foreach( lib ${libs})
    string(FIND ${lib} ${module_name} FOUND_MODULE)
    set(module ${lib})
    string(TOUPPER ${module} MODULE)
    set(${declspec_txt_var} "${${declspec_txt_var}}#if !defined(${declspec_prefix}${MODULE}_DECL) \n")
    set(${declspec_txt_var} "${${declspec_txt_var}}#  define ${declspec_prefix}${MODULE}_DECL \n")
    set(${declspec_txt_var} "${${declspec_txt_var}}# endif \n")
    #    endif()
  endforeach()
  set(${declspec_txt_var} "${${declspec_txt_var}}#endif \n")
endmacro(cpcpw_make_declspec)


#-------------------------------------------------------------------------------------------
# cpcpw_add_library_module macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   This macro adds a subdirectory that holds a new library part. It will create
#   a ModuleConfig.h containing all declspecs as well as the defines associated with
#   the module
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(cpcpw_add_executable var_name)
  if(ANDROID OR IOS_PLATFORM)
    add_executable(${var_name} EXCLUDE_FROM_ALL ${ARGN})
  else()
    add_executable(${var_name} ${ARGN})
  endif()
  set_property(GLOBAL APPEND PROPERTY "PROP_${PROJECT_NAME}_EXECUTABLES" "${var_name}" )
endmacro(cpcpw_add_executable)
#-------------------------------------------------------------------------------------------
# cpcpw_add_library_module macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   This macro adds a subdirectory that holds a new library part. It will create
#   a ModuleConfig.h containing all declspecs as well as the defines associated with
#   the module
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(cpcpw_add_library_module MODULE_NAME)
  string(TOUPPER ${MODULE_NAME} MODULE_NAME_UPPER_CASE)
  string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPER_CASE)

  set_property(GLOBAL APPEND PROPERTY "PROP_${PROJECT_NAME}_LIBRARY_MODULES" "${MODULE_NAME}" )

  # add the directory
  add_subdirectory(${MODULE_NAME})

  cpcpw_make_declspec("${PROJECT_NAME}" "${PROJECT_NAME_UPPER_CASE}_" ${MODULE_NAME} ${MODULE_NAME_UPPER_CASE}_DECL)

  cpcpw_get_module_options( ${PROJECT_NAME} ${MODULE_NAME} OPTS )
  foreach(opt ${OPTS})
    set(${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES "${${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES}#cmakedefine ${opt} \n")
  endforeach()

  cpcpw_get_module_defines( ${PROJECT_NAME} ${MODULE_NAME} DEFS )
  foreach( def ${DEFS} )
    set(${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES "${${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES}#ifndef ${def}\n")
    if ( "${${def}}" STREQUAL "ON" OR "${${def}}" STREQUAL "OFF")
      set(${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES "${${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES}#cmakedefine ${def}\n")
    elseif( "${${def}}" MATCHES "^[0-9]+$" )
      set(${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES "${${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES}#define ${def} ${${def}}\n")
    else()
      set(${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES "${${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES}# define ${def} \"${${def}}\"\n")
    endif()
    set(${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES "${${MODULE_NAME_UPPER_CASE}_CMAKE_DEFINES}#endif //${def}\n")
  endforeach()


  configure_file( "${${PROJECT_NAME}_SOURCE_DIR}/configure/cmake/configs/cpcpw_module_config.h.in"           "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}/${MODULE_NAME}/${MODULE_NAME}_config.h.in1" @ONLY)
  configure_file( "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}/${MODULE_NAME}/${MODULE_NAME}_config.h.in1" "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}/${MODULE_NAME}/${MODULE_NAME}_config.h.in2" )
  configure_file( "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}/${MODULE_NAME}/${MODULE_NAME}_config.h.in2" "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}/${MODULE_NAME}/${MODULE_NAME}_config.h" )

  install(FILES "${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME}/${MODULE_NAME}/${MODULE_NAME}_config.h"
          DESTINATION "include/${PROJECT_NAME}/${MODULE_NAME}")

endmacro(cpcpw_add_library_module)


#-------------------------------------------------------------------------------------------
# cpcpw_add_googletest macro definition
#-------------------------------------------------------------------------------------------
macro(cpcpw_add_googletest exec_name)
  set(UnitTestMain "${PROJECT_BINARY_DIR}/UnitTests/${exec_name}Main.cpp")
  configure_file( ${PROJECT_SOURCE_DIR}/configure/cmake/configs/GoogleTestUnitTestMain.cpp.in ${UnitTestMain})
  add_executable( "${exec_name}" ${UnitTestMain} ${ARGN})
  add_test(${exec_name} ${exec_name})
endmacro(cpcpw_add_googletest)

macro(cpcpw_target_link_libraries lib_name)
  if(NOT BUILD_SHARED_LIBS)
    set_property(GLOBAL APPEND PROPERTY "${PROJECT_NAME}_LINK_LIBRARIES" "${ARGN}")
  else()
    if(TARGET ${lib_name})
      target_link_libraries(${lib_name} ${ARGN})
    endif()
  endif()
endmacro(cpcpw_target_link_libraries)

#-------------------------------------------------------------------------------------------
# cpcpw_add_library macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   register a library
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(cpcpw_add_library var_target_name)
  set_property(GLOBAL APPEND PROPERTY "${PROJECT_NAME}_modules" "${var_target_name}" )

  debug("cpcpw_add_library: ${var_target_name}")

  # check if module has actually files to compile
  set(_header_only ON)
  foreach(src_file ${ARGN})
    if( (src_file MATCHES "\\.c$") OR (src_file MATCHES "\\.cpp$") OR (src_file MATCHES "\\.cc$") )
        message("${src_file} matches !!!!!!")
        set(_header_only OFF)
        break()
    endif()
  endforeach()

  if (BUILD_SHARED_LIBS AND NOT _header_only)
    debug("-> building shared libs")
    set_property(GLOBAL APPEND PROPERTY "${PROJECT_NAME}_libraries" "${var_target_name}" )
    add_library(${var_target_name} ${ARGN})

    if(MSVC)
      debug("-> adding MSVC specific flags")
      if(${CMAKE_SIZEOF_VOID_P} EQUAL 4) # check 32bit compiler
        cpcpw_add_target_properties("${var_target_name}" PROPERTIES STATIC_LIBRARY_FLAGS "/MACHINE:X86")
        cpcpw_add_target_properties("${var_target_name}" PROPERTIES LINK_FLAGS "/MACHINE:X86")
      elseif(${CMAKE_SIZEOF_VOID_P} EQUAL 8)  # check 64bit compiler
        cpcpw_add_target_properties("${var_target_name}" PROPERTIES STATIC_LIBRARY_FLAGS "/MACHINE:X64")
        cpcpw_add_target_properties("${var_target_name}" PROPERTIES LINK_FLAGS "/MACHINE:X64")
      endif()

      install(TARGETS ${var_target_name} EXPORT cpcpw-targets
              INCLUDES DESTINATION "include"
              RUNTIME DESTINATION "bin"
              LIBRARY DESTINATION "lib"
              ARCHIVE DESTINATION "lib")

      install(EXPORT cpcpw-targets 
              DESTINATION "."
              NAMESPACE "cpcpw::")
    endif()

    # set extra module CXX_CMAKE_FLAGS
    string(TOUPPER ${PROJECT_NAME} MODULE_NAME)
    set(${MODULE_NAME}_EXTRA_CXX_FLAGS "" CACHE STRING "insert extra flags that will be added to all libraries in ${MODULE_NAME}")
    mark_as_advanced(${MODULE_NAME}_EXTRA_CXX_FLAGS )

    # set extra module CXX_CMAKE_FLAGS
    string(TOUPPER ${var_target_name} LIB_NAME)
    set(${MODULE_NAME}_${LIB_NAME}_EXTRA_CXX_FLAGS "" CACHE STRING "insert extra flags that will only be applied to ${var_target_name}")
    mark_as_advanced(${MODULE_NAME}_${LIB_NAME}_EXTRA_CXX_FLAGS)

    # add additional target flags
    if(NOT "${${MODULE_NAME}_EXTRA_CXX_FLAGS}" STREQUAL "")
      cpcpw_add_target_properties(${var_target_name} PROPERTIES COMPILE_FLAGS "${${MODULE_NAME}_EXTRA_CXX_FLAGS}" )
    endif()

    # add additional target flags
    if(NOT "${${MODULE_NAME}_${LIB_NAME}_EXTRA_CXX_FLAGS}" STREQUAL "")
      cpcpw_add_target_properties(${var_target_name} PROPERTIES COMPILE_FLAGS "${${MODULE_NAME}_${LIB_NAME}_EXTRA_CXX_FLAGS}" )
    endif()
  else()
    debug("-> building static libs")
    # we need to make full paths out of the source files
    foreach(source_file ${ARGN})
      string(FIND ${source_file} ${CMAKE_CURRENT_LIST_DIR} B_FOUND)
      string(FIND ${source_file} ${${PROJECT_NAME}_BINARY_DIR} B_FOUND_BUILD)

      get_source_file_property(_var_generated_ "${source_file}" GENERATED)

      if(${B_FOUND} LESS 0 AND ${B_FOUND_BUILD} LESS 0)
        set(FPATH "${CMAKE_CURRENT_LIST_DIR}/${source_file}")
      else()
        set(FPATH ${source_file})
      endif()
      
      if(_var_generated_)
        list(APPEND gen_sources "${FPATH}")
      else()
        list(APPEND sources "${FPATH}")
      endif()
    
      # IF FILE IS A HEADER FILE PREPARE IT FOR CORRECT INSTALLATION PATH
      if(${FPATH} MATCHES ".*.h$")
        string(REPLACE "${${PROJECT_NAME}_SOURCE_DIR}/" "" REL_INSTALL_DIR ${FPATH})
        get_filename_component(REL_INSTALL_DIR ${REL_INSTALL_DIR} DIRECTORY)

        install(FILES ${FPATH} 
                DESTINATION "include/${REL_INSTALL_DIR}")
      endif()    
    endforeach()

    set_property(GLOBAL APPEND PROPERTY "${PROJECT_NAME}_LIBRARY_SOURCES" "${sources}")
    set_property(GLOBAL APPEND PROPERTY "${PROJECT_NAME}_GENERATED_LIBRARY_SOURCES" "${gen_sources}")    
  endif()

  debug("-> search for headers to install")
  foreach(_arg_ ${ARGN})
    if( ${_arg_} MATCHES "\\.h$")
      get_source_file_property(_header_location_ ${_arg_} LOCATION)
      
      string(REPLACE "${${PROJECT_NAME}_SOURCE_DIR}" "" _header_location_ "${_header_location_}")
      string(REPLACE "${${PROJECT_NAME}_BINARY_DIR}" "" _header_location_ "${_header_location_}")
      get_filename_component(_header_location_ ${_header_location_} DIRECTORY)

      debug("->   header:     ${_arg_}")  
      debug("->   location:   ${_header_location_}")  
      debug("->   install to: include/${_header_location_}")

      install(FILES ${_arg_} DESTINATION "include/${_header_location_}")
    endif()
  endforeach()
endmacro(cpcpw_add_library target_name)

#-------------------------------------------------------------------------------------------
# cpcpw_create_libraries macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   register a library
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(cpcpw_create_libraries)
  if(NOT BUILD_SHARED_LIBS)
    get_property(_sources_ GLOBAL PROPERTY "${PROJECT_NAME}_LIBRARY_SOURCES")
    get_property(_gen_sources_ GLOBAL PROPERTY "${PROJECT_NAME}_GENERATED_LIBRARY_SOURCES")
    get_property(_qt5_resources_ GLOBAL PROPERTY "${PROJECT_NAME}_QT5_ADD_RESOURCES")

    set(CMAKE_AUTOMOC ON)

    if(qt5_resources)
      qt5_add_resources(_resource_added_ ${_qt5_resources_})
    endif()

    set_source_files_properties(${gen_sources}
                                PROPERTIES
                                GENERATED 1)

    get_property(_link_libs_ GLOBAL PROPERTY "${PROJECT_NAME}_LINK_LIBRARIES")
    
    add_library(${PROJECT_NAME}_static 
                ${_sources_} 
                ${_gen_sources_} 
                ${_resource_added_})

    target_link_libraries(${PROJECT_NAME}_static ${_link_libs_})

    install(TARGETS ${PROJECT_NAME}_static
            RUNTIME DESTINATION "bin"
            LIBRARY DESTINATION "lib"
            ARCHIVE DESTINATION "lib")

    set_property(GLOBAL APPEND PROPERTY "${PROJECT_NAME}_libraries" "${PROJECT_NAME}_static" )    
  endif()
endmacro(cpcpw_create_libraries)