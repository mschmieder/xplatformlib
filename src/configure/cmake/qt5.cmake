#-------------------------------------------------------------------------------------------
# LOAD_QT_MODULES macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
#   tries to find all qt modules that are availabe in the system. Make sure cmake is in
#   your PATH or CMAKE_PREFIX_PATH is set accordingly
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(cpcpw_load_qt5_modules)
    set(CMAKE_AUTOMOC ON)

    find_package(Qt5Core REQUIRED)

    # find all additional packages
    file(GLOB _qt_module_dirs "${Qt5Core_DIR}/../Qt5*")

    # get all module names
    foreach(dir ${_qt_module_dirs})
      list(APPEND CMAKE_PREFIX_PATH ${dir})
      get_filename_component(module_name ${dir} NAME)
      list(APPEND qt_modules ${module_name})
    endforeach()

    # remove folders that are not modules
    list(REMOVE_ITEM qt_modules Qt5)
    set(QT_VERSION  "${Qt5Core_VERSION_STRING}" CACHE STRING "Qt Version" FORCE)

    string(REPLACE "." ";" VERSION_LIST ${Qt5Core_VERSION_STRING})
    list(GET VERSION_LIST 0 MAJOR)
    list(GET VERSION_LIST 1 MINOR)
    list(GET VERSION_LIST 2 PATCH)

    set(QT_VERSION_MAJOR "${MAJOR}" CACHE STRING "qt major version" FORCE)
    set(QT_VERSION_MINOR "${MINOR}" CACHE STRING "qt minor version" FORCE)
    set(QT_VERSION_PATCH "${PATCH}" CACHE STRING "qt patch version" FORCE)

    cpcpw_define(${PROJECT_NAME} QT_VERSION_MAJOR "this define tells us if android build is active" ${QT_VERSION_MAJOR} )
    cpcpw_define(${PROJECT_NAME} QT_VERSION_MINOR "this define tells us if android build is active" ${QT_VERSION_MINOR} )
    cpcpw_define(${PROJECT_NAME} QT_VERSION_PATCH "this define tells us if android build is active" ${QT_VERSION_PATCH} )

    mark_as_advanced( QT_VERSION_MAJOR QT_VERSION_MINOR QT_VERSION_PATCH )

    # add option for each module
    foreach(qt_module ${qt_modules})
      cpcpw_option(${PROJECT_NAME} USE_${qt_module} "use qt module ${module}" OFF)
      # find package if needed
      if(${USE_${qt_module}})
        cpcpw_find_package(${qt_module} REQUIRED)
      endif()
    endforeach()
 endmacro(cpcpw_load_qt5_modules)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_line macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_line VAR LINE)
  set( ${VAR} "${${VAR}} ${LINE} \n")
endmacro(qt_pro_file_add_line)

#-------------------------------------------------------------------------------------------
# QMAKE_SET_VARIABLE macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(QMAKE_SET_VARIABLE VAR VAR_NAME VALUES)
  foreach(element ${${VALUES}})
    set(${VAR} "${${VAR}} ${VAR_NAME} += ${element} \n")
  endforeach()
endmacro(QMAKE_SET_VARIABLE)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_files macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_files VAR VALUES)
  set(_headers_   "")
  set(_sources_   "")
  set(_others_    "")
  set(_objective_sources_ "")
  set(_resources_ "")
  foreach(element ${${VALUES}})
    get_property(is_generated SOURCE ${element} PROPERTY GENERATED)
    if(NOT is_generated)
      get_source_file_property(element_file_full_path ${element} LOCATION)
      if(${element_file_full_path} MATCHES "\\.h" )
        list(APPEND _headers_ ${element_file_full_path})
      elseif(${element_file_full_path} MATCHES "(\\.cpp)" OR
             ${element_file_full_path} MATCHES "(\\.c)")
        list(APPEND _sources_ ${element_file_full_path})
      elseif(${element_file_full_path} MATCHES "\\.mm" )
        list(APPEND _objective_sources_ ${element_file_full_path})
      elseif(${element_file_full_path} MATCHES "\\.qrc" )
        list(APPEND _resources_ ${element_file_full_path})
      else()
        list(APPEND _others_ ${element_file_full_path})
      endif()
    endif()
  endforeach(element)

  if(_headers_)
    list(REMOVE_DUPLICATES _headers_)
    QMAKE_SET_VARIABLE(${VAR} "HEADERS" _headers_)
  endif()

  if(_sources_)
    list(REMOVE_DUPLICATES _sources_)
    QMAKE_SET_VARIABLE(${VAR} "SOURCES" _sources_)
  endif()

  if(_others_)
    list(REMOVE_DUPLICATES _others_ )
    QMAKE_SET_VARIABLE(${VAR} "OTHER_FILES" _others_)
  endif()

  if(_objective_sources_)
    list(REMOVE_DUPLICATES _objective_sources_)
    QMAKE_SET_VARIABLE(${VAR} "OBJECTIVE_SOURCES" _objective_sources_)
  endif()

  if(_resources_)
    list(REMOVE_DUPLICATES _resources_ )
  QMAKE_SET_VARIABLE(${VAR} "RESOURCES" _resources_)
  endif()
endmacro(qt_pro_file_add_files)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_link_directories macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_link_directories VAR VALUES)
  foreach(element ${${VALUES}})
    qt_pro_file_add_line(${VAR} "QMAKE_LIBDIR += ${element}")
  endforeach()
endmacro(qt_pro_file_add_link_directories)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_include_directories macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_include_directories VAR PATHES)
foreach(element ${${PATHES}})
    string(REGEX MATCH "Qt/[0-9](\\.[0-9])?(\\.[0-9])?/([0-9]?)(\\.[0-9])?(\\.[0-9])?" MATCHES_QT_INCLUCED_DIR ${element})
    if(NOT MATCHES_QT_INCLUCED_DIR)
      set(${VAR} "${${VAR}} INCLUDEPATH += ${element} \n")
    endif()

  endforeach(element)
endmacro(qt_pro_file_add_include_directories)

#-------------------------------------------------------------------------------------------
# parse_target_link_libraries macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(parse_target_link_libraries LINK_LIBS TARGET_LINK_LIBRARIES_DEBUG TARGET_LINK_LIBRARIES_RELEASE TARGET_LINK_FRAMEWORKS TARGET_LINK_THIRDPARTY_DEBUG TARGET_LINK_THIRDPARTY_RELEASE VAR_CPCPW_TARGETS)
  foreach(element ${${LINK_LIBS}})

    set(EXCLUDE_MATCH OFF)
    foreach(EXCLUDE_LIBRARY ${EXCLUDE_LIBRARIES})
      if(${element} MATCHES ${EXCLUDE_LIBRARY})
        set(EXCLUDE_MATCH ON)
      endif()
    endforeach()

    if(NOT EXCLUDE_MATCH)
      STRING(REGEX MATCH "^[A-Za-z_0-9]+$" lib ${element})
      if(NOT lib)
        STRING(REGEX MATCH ":([A-Za-z_0-9.]+)\\>$" lib ${element})
        if(lib)
          STRING(REPLACE ":" "" lib ${lib})
          STRING(REPLACE ">" "" lib ${lib})
          STRING(FIND ${element} "$<NOT:$<CONFIG:DEBUG>>" IS_RELEASE) # RELEASE LIB
          STRING(FIND ${element} "$<CONFIG:DEBUG>"        IS_DEBUG  ) # DEBUG LIB

          get_filename_component(lib ${lib} NAME_WE)
          STRING(REPLACE "lib" "" lib ${lib})

          #TODO: sma: Search library path
          if(IS_RELEASE GREATER 0)
            list(APPEND TARGET_LINK_THIRDPARTY_RELEASE "${lib}")
          elseif(IS_DEBUG GREATER 0)
            list(APPEND TARGET_LINK_THIRDPARTY_DEBUG "${lib}")
          else()
          endif()
      else() # check for ios frameworks
        STRING(FIND ${element} ".framework" FOUND)
        if( FOUND GREATER -1)
           list(APPEND TARGET_LINK_FRAMEWORKS "${element}")
        endif()
      endif()
      else()
       get_target_property(TARGET_DEBUG_POSTFIX   ${element} DEBUG_POSTFIX)
       get_target_property(TARGET_RELEASE_POSTFIX ${element} RELEASE_POSTFIX)

       if(TARGET_RELEASE_POSTFIX)
         list(APPEND TARGET_LINK_LIBRARIES_DEBUG   "${element}${TARGET_RELEASE_POSTFIX}d")
         list(APPEND TARGET_LINK_LIBRARIES_RELEASE "${element}${TARGET_RELEASE_POSTFIX}")
       elseif(TARGET_DEBUG_POSTFIX)
         list(APPEND TARGET_LINK_LIBRARIES_DEBUG   "${element}${TARGET_DEBUG_POSTFIX}")
         string(LENGTH ${TARGET_DEBUG_POSTFIX} LEN)

         math(EXPR substrlen ${LEN}-1)
         string(SUBSTRING ${TARGET_DEBUG_POSTFIX} 0 ${substrlen} TARGET_RELEASE_POSTFIX)
         list(APPEND TARGET_LINK_LIBRARIES_RELEASE "${element}${TARGET_RELEASE_POSTFIX}")
       endif()

       list(APPEND ${VAR_CPCPW_TARGETS} ${element})

      endif()
    endif()
  endforeach(element)
endmacro(parse_target_link_libraries)

#-------------------------------------------------------------------------------------------
# parse_target_link_libraries macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION:
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_target_link_libraries VAR LIBS LIBRARY_MODULES)
  parse_target_link_libraries(${LIBS} TARGET_LINK_LIBRARIES_DEBUG TARGET_LINK_LIBRARIES_RELEASE TARGET_LINK_FRAMEWORKS TARGET_LINK_THIRDPARTY_DEBUG TARGET_LINK_THIRDPARTY_RELEASE CPCPW_TARGETS)

  set(_LIB_TYPE_ "")
  set(_LIB_FOLDER_ "")
  set(_LIB_PATH_ "")

  # Shared or static Builds
  if(${BUILD_SHARED_LIBS})
    set(_LIB_TYPE_ "so")
  else()
    set(_LIB_TYPE_ "a")
  endif()

  foreach(lib ${TARGET_LINK_FRAMEWORKS})
    get_filename_component(fw_dir "${lib}" DIRECTORY)
    get_filename_component(fwname "${lib}" NAME_WE)
    set(${VAR} "${${VAR}}\n LIBS += -F${fw_dir} -framework ${fwname}")
  endforeach()

  set(${VAR} "${${VAR}}\n CONFIG(debug, debug|release){")
  foreach(lib ${TARGET_LINK_THIRDPARTY_DEBUG})

    # Shared or static Builds
    if(${BUILD_SHARED_LIBS})
      set(_LIB_FOLDER_ "/bin/")
    else()
      set(_LIB_FOLDER_ "/lib/")
    endif()

    if ( "${lib}" MATCHES "lib" )
      message(${_LIB_FILE_PATH_})
      file(GLOB_RECURSE _LIB_FILE_PATH_ "${CPCPWLIB_THIRD_PARTY_LIBS_DIR}/${lib}.*")
    else()
      file(GLOB_RECURSE _LIB_FILE_PATH_ "${CPCPWLIB_THIRD_PARTY_LIBS_DIR}/lib${lib}.*")
    endif()

    set(_LIB_FOLDER_ "")

    list(APPEND ANDROID_EXTRA_LIBS_DEBUG "${_LIB_FILE_PATH_}")

    set(${VAR} "${${VAR}}\n   LIBS += -l${lib}")
  endforeach()

   foreach(lib ${TARGET_LINK_LIBRARIES_DEBUG})

    if(${BUILD_SHARED_LIBS})
      set(_LIB_FOLDER_ "/bin/")
    else()
      set(_LIB_FOLDER_ "/lib/")
    endif()

    set(_LIB_PATH_ "${PROJECT_BINARY_DIR}")

    list(APPEND ANDROID_EXTRA_LIBS_DEBUG "${_LIB_PATH_}${_LIB_FOLDER_}lib${lib}.${_LIB_TYPE_}")

    set(${VAR} "${${VAR}}\n   LIBS += -l${lib}")
  endforeach()

  set(${VAR} "${${VAR}} \n } \n else { ")


    foreach(lib ${TARGET_LINK_THIRDPARTY_RELEASE})
      # Shared or static Builds
      if(${BUILD_SHARED_LIBS})
        set(_LIB_FOLDER_ "/bin/")
      else()
        set(_LIB_FOLDER_ "/lib/")
      endif()

      file(GLOB_RECURSE _LIB_FILE_PATH_ "${CPCPWLIB_THIRD_PARTY_LIBS_DIR}/*lib${lib}.*")
      set(_LIB_FOLDER_ "")


      list(APPEND ANDROID_EXTRA_LIBS_RELEASE "${_LIB_FILE_PATH_}")

      set(${VAR} "${${VAR}}\n   LIBS += -l${lib}")
    endforeach()


  foreach(lib ${TARGET_LINK_LIBRARIES_RELEASE})
  # Shared or static Builds
    if(${BUILD_SHARED_LIBS})
      set(_LIB_FOLDER_ "/bin/")
    else()
      set(_LIB_FOLDER_ "/lib/")
    endif()

    # Check for thirdpartys
    set(_LIB_PATH_ "${PROJECT_BINARY_DIR}")

    list(APPEND ANDROID_EXTRA_LIBS_RELEASE "${_LIB_PATH_}${_LIB_FOLDER_}lib${lib}.${_LIB_TYPE_}")

    set(${VAR} "${${VAR}}\n   LIBS += -l${lib}")

  endforeach()

  set(${VAR} "${${VAR}} \n } \n\n")

  if(ANDROID)
    set(${VAR} "${${VAR}}\n CONFIG(debug, debug|release){")

    foreach(lib ${ANDROID_EXTRA_LIBS_DEBUG})
      set(${VAR} "${${VAR}}\n   ANDROID_EXTRA_LIBS += ${lib}")
    endforeach()
    set(${VAR} "${${VAR}} \n } \n else { ")

    foreach(lib ${ANDROID_EXTRA_LIBS_RELEASE})
      set(${VAR} "${${VAR}}\n   ANDROID_EXTRA_LIBS += ${lib}")
    endforeach()
    set(${VAR} "${${VAR}} \n } \n\n")
  endif()
endmacro(qt_pro_file_add_target_link_libraries)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_androidextralibs macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION: Add ANDROID_EXTRA_LIBS
# USAGE:
# AUTHOR:
#   schw
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_androidextralibs VAR VAR_LIBS_DEBUG VAR_LIBS_RELEASE)

  set(${VAR} "${${VAR}} \n CONFIG(debug, debug|release){")

  foreach(libs ${${VAR_LIBS_DEBUG}})
    set(${VAR} "${${VAR}} \n   ANDROID_EXTRA_LIBS += ${libs}")
  endforeach(libs)

  set(${VAR} "${${VAR}} \n } \n else { ")

  foreach(libs ${${VAR_LIBS_RELEASE}})
    set(${VAR} "${${VAR}} \n   ANDROID_EXTRA_LIBS += ${libs}")
  endforeach(libs)

  set(${VAR} "${${VAR}} \n } \n")

endmacro(qt_pro_file_add_androidextralibs)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_deploymentfolders macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION: Add Deploymentfolders
# USAGE:
# AUTHOR:
#   schw
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_deploymentfolders VAR VAR_FOLDERS)

  set(_FOLDER_ "folder")
  set(_COUNTER_ "1")

  foreach(folder ${${VAR_FOLDERS}})
    set(_SOURCE_ "${_FOLDER_}${_COUNTER_}.source = ${folder}")
    set(_TARGET_ "${_FOLDER_}${_COUNTER_}.target = ")
    set(${VAR} "${${VAR}} \n ${_SOURCE_}")
    set(${VAR} "${${VAR}} \n ${_TARGET_}")
    set(${VAR} "${${VAR}} \n DEPLOYMENTFOLDERS += ${_FOLDER_}${_COUNTER_} \n \n")
    MATH(EXPR _COUNTER_ "${_COUNTER_}+1")
  endforeach(folder)
endmacro(qt_pro_file_add_deploymentfolders)

#-------------------------------------------------------------------------------------------
# qt_pro_file_add_qtmodules macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION: The values stored in the QT variable control which of the Qt modules are used by your project
#  The table shows the options that can be used with the QT variable and the features that are associated with each of them:
#  core (included by default) QtCore module
#  gui (included by default) QtGui module
#  network QtNetwork module
#  opengl  QtOpenGL module
#  phonon  Phonon Multimedia Framework
#  sql QtSql module
#  svg QtSvg module
#  xml QtXml module
#  webkit  WebKit integration
#  qt3support  Qt3Support module
#  By default, QT contains both core and gui, ensuring that standard GUI applications can be built without further configuration
#  If you want to build a project without the Qt GUI module, you need to exclude the gui value with the "-=" operator
#  e.g. QT -= gui # Only the core module is used
# AUTHOR:
#-------------------------------------------------------------------------------------------
macro(qt_pro_file_add_qtmodules VAR)

set(_QTMODULES_ "QT += ")

if(${USE_Qt5AndroidExtras})
  set(_QTMODULES_ "${_QTMODULES_} androidextras")
endif()

if(${USE_Qt5Bluetooth})
  set(_QTMODULES_ "${_QTMODULES_} bluetooth")
endif()

if(${USE_Qt5Core})
  set(_QTMODULES_ "${_QTMODULES_} core")
endif()

if(${USE_Qt5Declarative})
  set(_QTMODULES_ "${_QTMODULES_} declarative")
endif()

if(${USE_Qt5Gui})
  set(_QTMODULES_ "${_QTMODULES_} gui")
endif()

if(${USE_Qt5Help})
  set(_QTMODULES_ "${_QTMODULES_} help")
endif()

if(${USE_Qt5Multimedia})
  set(_QTMODULES_ "${_QTMODULES_} multimedia")
endif()

if(${USE_Qt5MultimediaWidgets})
  set(_QTMODULES_ "${_QTMODULES_} multimediawidgets")
endif()

if(${USE_Qt5Network})
  set(_QTMODULES_ "${_QTMODULES_} network")
endif()

if(${USE_Qt5OpenGL})
  set(_QTMODULES_ "${_QTMODULES_} opengl")
endif()

if(${USE_Qt5PrintSupport})
  set(_QTMODULES_ "${_QTMODULES_} printsupport")
endif()

if(${USE_Qt5Qml})
  set(_QTMODULES_ "${_QTMODULES_} qml")
endif()

if(${USE_Qt5Quick})
  set(_QTMODULES_ "${_QTMODULES_} quick")
endif()

if(${USE_Qt5QuickTest})
  set(_QTMODULES_ "${_QTMODULES_} quicktest")
endif()

if(${USE_Qt5Sensors})
  set(_QTMODULES_ "${_QTMODULES_} sensors")
endif()

if(${USE_Qt5Sql})
  set(_QTMODULES_ "${_QTMODULES_} sql")
endif()

if(${USE_Qt5Svg})
  set(_QTMODULES_ "${_QTMODULES_} svg")
endif()

if(${USE_Qt5Xml})
  set(_QTMODULES_ "${_QTMODULES_} xml")
endif()

if(${USE_Qt5XmlPatterns})
  set(_QTMODULES_ "${_QTMODULES_} xmlpatterns")
endif()

if(${USE_Qt5Webkit})
  set(_QTMODULES_ "${_QTMODULES_} webkit")
endif()

if(${USE_Qt5Widgets})
  set(_QTMODULES_ "${_QTMODULES_} widgets")
endif()

if(${USE_Qt5Phonon})
  set(_QTMODULES_ "${_QTMODULES_} phonon")
endif()

set(${VAR} "${${VAR}} ${_QTMODULES_} \n")

endmacro(qt_pro_file_add_qtmodules)

#-------------------------------------------------------------------------------------------
# create_library_links macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION: // TODO
#  Input Params:
#    TARGET_DIR                 // TODO
#    VAR_LINK_TARGETS           // TODO
# USAGE:
# AUTHOR:
#   scm
#-------------------------------------------------------------------------------------------
macro(create_library_links TARGET_DIR VAR_LINK_TARGETS)
  parse_target_link_libraries(${VAR_LINK_TARGETS} TARGET_LINK_LIBRARIES_DEBUG TARGET_LINK_LIBRARIES_RELEASE TARGET_LINK_FRAMEWORKS TARGET_LINK_THIRDPARTY_DEBUG TARGET_LINK_THIRDPARTY_RELEASE CPCPW_TARGETS) 
  if(NOT "${CMAKE_HOST_SYSTEM_NAME}" STREQUAL "Windows" )
     foreach(lib ${TARGET_LINK_LIBRARIES_DEBUG})
        execute_process(COMMAND ln -sf "../bin/lib${lib}.so" "${TARGET_DIR}/")
    endforeach()
  endif()
endmacro(create_library_links)

#-------------------------------------------------------------------------------------------
# ADD_QT_PROJECT macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION: Generates a .pro file for qmake use (android / apple)
#  Input Params:
#    target_name                 Modulname -> Name of the App
#    var_qt_resources            Resources
#    var_qt_deploymentfolders    Deployment Folders
#    var_library_modules         All module names of linked libraries
#    var_os                      Operating System (APPLE, ANDROID)
#    var_os_specialpath          Special Path for the OS where the xml or the Info.plist with Bundle data are
#    out_file_path               Path where to build the .pro file
# USAGE:
# AUTHOR:
#   scm/schw
#-------------------------------------------------------------------------------------------
macro(cpcpw_wrap_qtcreator_project 
  target_name var_qt_resources 
  var_qt_deploymentfolders 
  var_library_modules
  out_file_path)
  
  set(_pro_file_content)

  # ADDING CONFIG AND TARGET NAME
  qt_pro_file_add_line(_pro_file_content "CONFIG += qt c++11 resources exceptions rtti stl" )
  qt_pro_file_add_line(_pro_file_content "TARGET = ${target_name}" )

  if(USE_SQUISH)
    qt_pro_file_add_line(_pro_file_content "SQUISH_ATTACH_PORT=${SQUISH_ATTACH_PORT}" )
    qt_pro_file_add_line(_pro_file_content "include(${SQUISH_PRI_FILE})" )
  endif()

  #qt_pro_file_add_line(_pro_file_content "QT += core gui network opengl sql svg xml widgets quick declarative" )
  qt_pro_file_add_qtmodules(_pro_file_content)

  set(TARGET_RC_FILES ${${var_qt_resources}})
  qt_pro_file_add_files(_pro_file_content TARGET_RC_FILES)

  # SETTING UP TARGET TYPE
  get_target_property(TARGET_CMAKE_TYPE ${target_name} TYPE)
  if(${TARGET_CMAKE_TYPE} STREQUAL "EXECUTABLE")
    qt_pro_file_add_line(_pro_file_content "TEMPLATE = app")
  else()
    qt_pro_file_add_line(_pro_file_content "TEMPLATE = lib")
  endif()

  # ADD DEFINES
  get_target_property(TARGET_CMAKE_COMPILE_DEFINITIONS ${target_name} COMPILE_DEFINITIONS)
  if( NOT "${TARGET_CMAKE_COMPILE_DEFINITIONS}" MATCHES ".*-NOTFOUND$" )
    foreach( define ${TARGET_CMAKE_COMPILE_DEFINITIONS} )
      qt_pro_file_add_line(_pro_file_content "DEFINES += ${define}")
    endforeach()
  endif()
  get_directory_property(DIRECTORY_CMAKE_COMPILE_DEFINITIONS COMPILE_DEFINITIONS)
  if( NOT ${DIRECTORY_CMAKE_COMPILE_DEFINITIONS} MATCHES ".*-NOTFOUND$" )
    foreach( define ${DIRECTORY_CMAKE_COMPILE_DEFINITIONS} )
      qt_pro_file_add_line(_pro_file_content "DEFINES += ${define}")
    endforeach()
  endif()

  qt_pro_file_add_line(_pro_file_content "CONFIG(debug, debug|release) {")
  get_target_property(TARGET_CMAKE_COMPILE_DEFINITIONS_DEBUG ${target_name} COMPILE_DEFINITIONS_DEBUG)
  if( NOT ${TARGET_CMAKE_COMPILE_DEFINITIONS_DEBUG} MATCHES ".*-NOTFOUND$" )
    foreach( define ${TARGET_CMAKE_COMPILE_DEFINITIONS_DEBUG} )
      qt_pro_file_add_line(_pro_file_content "    DEFINES += ${define}")
    endforeach()
  endif()
  get_directory_property(DIRECTORY_CMAKE_COMPILE_DEFINITIONS_DEBUG COMPILE_DEFINITIONS_DEBUG)
  if( NOT ${DIRECTORY_CMAKE_COMPILE_DEFINITIONS_DEBUG} MATCHES ".*-NOTFOUND$" )
    foreach( define ${DIRECTORY_CMAKE_COMPILE_DEFINITIONS_DEBUG} )
      qt_pro_file_add_line(_pro_file_content "    DEFINES += ${define}")
    endforeach()
  endif()
  qt_pro_file_add_line(_pro_file_content "} else {")
  get_target_property(TARGET_CMAKE_COMPILE_DEFINITIONS_RELEASE ${target_name} COMPILE_DEFINITIONS_RELEASE)
  if( NOT ${TARGET_CMAKE_COMPILE_DEFINITIONS_RELEASE} MATCHES ".*-NOTFOUND$" )
    foreach( define ${TARGET_CMAKE_COMPILE_DEFINITIONS_RELEASE} )
      qt_pro_file_add_line(_pro_file_content "    DEFINES += ${define}")
    endforeach()
  endif()
  get_directory_property(DIRECTORY_CMAKE_COMPILE_DEFINITIONS_RELEASE COMPILE_DEFINITIONS_RELEASE)
  if( NOT ${DIRECTORY_CMAKE_COMPILE_DEFINITIONS_RELEASE} MATCHES ".*-NOTFOUND$" )
    foreach( define ${DIRECTORY_CMAKE_COMPILE_DEFINITIONS_RELEASE} )
      qt_pro_file_add_line(_pro_file_content "    DEFINES += ${define}")
    endforeach()
  endif()
  qt_pro_file_add_line(_pro_file_content "}")

  # ADDING LINK SOURCES
  get_target_property(TARGET_SOURCES ${target_name} SOURCES)
  qt_pro_file_add_files(_pro_file_content TARGET_SOURCES)

  # ADDING LINK SOURCES AND HEADER FOR LIBRARIES (STATIC BUILD)
  if(NOT ${BUILD_SHARED_LIBS})
    get_property(link_sources GLOBAL PROPERTY "PROP_LIBRARY_SOURCES")
    list(REMOVE_DUPLICATES link_sources)
    qt_pro_file_add_files(_pro_file_content link_sources)
  endif()

  # ADDING INCLUDE DIRECTORIES
  get_target_property(TARGET_INCLUDE_DIRECTORIES ${target_name} INCLUDE_DIRECTORIES)
  if(TARGET_INCLUDE_DIRECTORIES)
    list(REMOVE_DUPLICATES TARGET_INCLUDE_DIRECTORIES)
    qt_pro_file_add_include_directories(_pro_file_content TARGET_INCLUDE_DIRECTORIES)
  endif()
  
  # ADDING LINK DIRECTORIES
  get_property(TARGET_LINK_DIRECTORIES DIRECTORY ${CMAKE_CURRENT_LIST_DIR} PROPERTY LINK_DIRECTORIES)
  if(TARGET_LINK_DIRECTORIES)
    list(REMOVE_DUPLICATES TARGET_LINK_DIRECTORIES)
    qt_pro_file_add_link_directories(_pro_file_content TARGET_LINK_DIRECTORIES)
  endif()

  get_target_property(TARGET_ARCHIVE_OUTPUT_DIRECTORY         ${target_name} ARCHIVE_OUTPUT_DIRECTORY)
  get_target_property(TARGET_ARCHIVE_OUTPUT_DIRECTORY_DEBUG   ${target_name} ARCHIVE_OUTPUT_DIRECTORY_DEBUG)
  get_target_property(TARGET_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${target_name} ARCHIVE_OUTPUT_DIRECTORY_RELEASE)
  get_target_property(TARGET_LIBRARY_OUTPUT_DIRECTORY         ${target_name} LIBRARY_OUTPUT_DIRECTORY)
  get_target_property(TARGET_LIBRARY_OUTPUT_DIRECTORY_DEBUG   ${target_name} LIBRARY_OUTPUT_DIRECTORY_DEBUG)
  get_target_property(TARGET_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${target_name} LIBRARY_OUTPUT_DIRECTORY_RELEASE)

  if(${TARGET_ARCHIVE_OUTPUT_DIRECTORY_DEBUG} STREQUAL "TARGET_ARCHIVE_OUTPUT_DIRECTORY_DEBUG_NOTFOUND")
    qt_pro_file_add_link_directories(_pro_file_content TARGET_ARCHIVE_OUTPUT_DIRECTORY_DEBUG)
  endif()
  if(${TARGET_ARCHIVE_OUTPUT_DIRECTORY_RELEASE} STREQUAL "TARGET_ARCHIVE_OUTPUT_DIRECTORY_RELEASE_NOTFOUND")
    qt_pro_file_add_link_directories(_pro_file_content TARGET_ARCHIVE_OUTPUT_DIRECTORY_RELEASE)
  endif()
  if(${TARGET_LIBRARY_OUTPUT_DIRECTORY_DEBUG} STREQUAL "TARGET_LIBRARY_OUTPUT_DIRECTORY_DEBUG_NOTFOUND")
    qt_pro_file_add_link_directories(_pro_file_content TARGET_LIBRARY_OUTPUT_DIRECTORY_DEBUG)
  endif()
  if(${TARGET_LIBRARY_OUTPUT_DIRECTORY_RELEASE} STREQUAL "TARGET_LIBRARY_OUTPUT_DIRECTORY_RELEASE_NOTFOUND")
     qt_pro_file_add_link_directories(_pro_file_content TARGET_LIBRARY_OUTPUT_DIRECTORY_RELEASE)
  endif()

  qt_pro_file_add_link_directories(_pro_file_content TARGET_ARCHIVE_OUTPUT_DIRECTORY)
  qt_pro_file_add_link_directories(_pro_file_content TARGET_LIBRARY_OUTPUT_DIRECTORY)

  if(${var_qt_deploymentfolders})
    qt_pro_file_add_deploymentfolders(_pro_file_content ${var_qt_deploymentfolders})
  endif()

  # ADDING LINK LIBRARIES
  get_target_property(TARGET_LINK_LIBRARIES ${target_name} LINK_LIBRARIES)
  if(${var_library_modules})
    qt_pro_file_add_target_link_libraries(_pro_file_content TARGET_LINK_LIBRARIES ${var_library_modules})
  endif()

  # ADDING QML IMPORT PATH
  qt_pro_file_add_line(_pro_file_content "QML_IMPORT_PATH += ${PROJECT_BINARY_DIR}/qml/")

  # RUNTIME
  get_target_property(TARGET_RUNTIME_OUTPUT_DIRECTORY ${target_name} RUNTIME_OUTPUT_DIRECTORY)

  # INSTALL TARGET
  qt_pro_file_add_line(_pro_file_content "target.path = ${PROJECT_BINARY_DIR}/bin")
  qt_pro_file_add_line(_pro_file_content "INSTALLS += target")

  file(WRITE ${out_file_path} ${_pro_file_content} )

  file(GLOB _qml_files_top_level ${CURRENT_CMAKE_LIST_DIR} "*.qml")
  if(_qml_files_top_level)
    get_filename_component(_pro_file_dir "${out_file_path}" DIRECTORY)
    file(COPY ${_qml_files_top_level} 
         DESTINATION ${_pro_file_dir})
  endif()

  create_library_links("${PROJECT_BINARY_DIR}/${target_name}" TARGET_LINK_LIBRARIES)

  set(_qmake_params "-recursive")
  if(ANDROID)
    set(_qmake_params "${_qmake_params} -spec android-g++")
  elseif(IOS_PLATFORM)
    set(_qmake_params "${_qmake_params} -spec macx-ios-clang")
    if("${IOS_PLATFORM}" STREQUAL "SIMULATOR")
      set(_qmake_params "${_qmake_params} CONFIG+=iphonesimulator")
    endif()
  endif()

  if( ${CMAKE_BUILD_TYPE} STREQUAL "debug")
    set(_qmake_params "${_qmake_params} CONFIG+=qml_debug")
  endif()


  set(_build_script_content "#!/usr/bin/env bash\n")
  set(_build_script_content "${_build_script_content}cd ${_pro_file_dir} && qmake ${out_file_path} ${_qmake_params} && ${CMAKE_MAKE_PROGRAM} && ${CMAKE_MAKE_PROGRAM} install")

  file(WRITE ${_pro_file_dir}/build_qt_pro.sh ${_build_script_content})

  add_custom_target(${target_name}_qtcreator_project ALL
                    WORKING_DIRECTORY ${_pro_file_dir}
                    COMMAND chmod +x ${_pro_file_dir}/build_qt_pro.sh
                    COMMAND ${_pro_file_dir}/build_qt_pro.sh
                    COMMAND ${CMAKE_MAKE_PROGRAM} && ${CMAKE_MAKE_PROGRAM} install
                    COMMENT "running qmake project ${out_file_path}"
                    VERBATIM)

endmacro(cpcpw_wrap_qtcreator_project)