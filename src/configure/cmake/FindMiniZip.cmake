# Try to find the MiniZip lib
#  MINIZIP_FOUND - system has MiniZip
#  MINIZIP_INCLUDE_DIRS - the MiniZip include directory
#  MINIZIP_LIBRARIES - The libraries needed to use MiniZip 

if (MINIZIP_INCLUDE_DIRS)
  # Already in cache, be silent
  set(MINIZIP_FIND_QUIETLY TRUE)
endif (MINIZIP_INCLUDE_DIRS)

if (NOT MINIZIP_INCLUDE_DIRS)
    find_path(MINIZIP_INCLUDE_DIRS NAMES unzip.h zip.h 
                                   PATHS ${MINIZIP_ROOT}/include
                                   PATH_SUFFIXES minizip)

    set(MINIZIP_INCLUDE_DIRS ${MINIZIP_INCLUDE_DIRS}/minizip CACHE PATH "MiniZip include directory")
endif (NOT MINIZIP_INCLUDE_DIRS)

FIND_LIBRARY(MINIZIP_LIBRARIES_RELEASE PATHS ${MINIZIP_ROOT}
                               PATH_SUFFIXES lib
                               NAMES minizip)

FIND_LIBRARY(MINIZIP_LIBRARIES_DEBUG PATHS ${MINIZIP_ROOT}
                               PATH_SUFFIXES lib
                               NAMES minizipd)

if(MINIZIP_LIBRARIES_RELEASE)
  list(APPEND MINIZIP_LIBRARIES optimized ${MINIZIP_LIBRARIES_RELEASE})
  list(APPEND PACKAGE_CONFIGS "release")
endif()
if(MINIZIP_LIBRARIES_DEBUG)
  list(APPEND MINIZIP_LIBRARIES debug ${MINIZIP_LIBRARIES_DEBUG})
  list(APPEND PACKAGE_CONFIGS "debug")
endif()

if (MINIZIP_INCLUDE_DIRS AND MINIZIP_LIBRARIES)
   set(MINIZIP_FOUND TRUE)

    add_library (minizip STATIC IMPORTED)
    set_target_properties(minizip PROPERTIES
        VERSION "1.1"
        IMPORTED_CONFIGURATIONS "${PACKAGE_CONFIGS}"
        INTERFACE_INCLUDE_DIRECTORIES "${MINIZIP_INCLUDE_DIRS}"
        # import libraries
        IMPORTED_LOCATION_DEBUG     "${MINIZIP_LIBRARIES_DEBUG}"
        IMPORTED_LOCATION_RELEASE   "${MINIZIP_LIBRARIES_RELEASE}"
    )

endif (MINIZIP_INCLUDE_DIRS AND MINIZIP_LIBRARIES)

if (MINIZIP_FOUND)
   if (NOT MINIZIP_FIND_QUIETLY)
      message(STATUS "Found MiniZip: ${MINIZIP_LIBRARIES}")
   endif (NOT MINIZIP_FIND_QUIETLY)
else (MINIZIP_FOUND)
    if (MINIZIP_FIND_REQUIRED)
      message(FATAL_ERROR "Could NOT find MiniZip")
    else (MINIZIP_FIND_REQUIRED)
      message(STATUS "Could NOT find MiniZip")
    endif (MINIZIP_FIND_REQUIRED)
endif (MINIZIP_FOUND)

MARK_AS_ADVANCED(MINIZIP_INCLUDE_DIRS MINIZIP_LIBRARIES)