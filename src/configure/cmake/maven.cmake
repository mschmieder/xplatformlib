option(USE_MAVEN "enables maven support for cpcpw" ON)
if(USE_MAVEN)
  set(MAVEN_REPOSITORY "http://192.168.250.10:8081/nexus/content/repositories" CACHE STRING "maven repository url")
  set(MAVEN_HOME       "$HOME/.m2"                                             CACHE STRING "local repository")
  mark_as_advanced(MAVEN_REPOSITORY MAVEN_HOME)
endif()

#-------------------------------------------------------------------------------------------
# cpcpw_maven_download_artifact macro definition
#-------------------------------------------------------------------------------------------
# DESCRIPTION: 
#   downloads a given maven Artifact from a maven repository defined by the user
# USAGE:
#   The following parameters are mandatory:
#   <ARTIFACT_NAME>
#      Name of the artifact to download 
#   <ARTIFACT_VERSION> 
#      Version of the artifact to download
#   <ARTIFACT_GROUP>
#      Group where to find the artifact on the maven repository 
#   <ARTIFACT_PACKAGING>
#      Artifact packaging type (zip,jar,war,...)
#   <REPO_URL>
#      Maven repository URL where artifact can be downloaded (i.e. http://192.168.250.10:8081/nexus/content/repositories/thirdparty)
#   <MAVEN_HOME>
#      Local maven home directory (usually $MAVEN_HOME) where your local repository resides
#   <INSTALL_DIRECTORY>
#      Directory where to install/unpack the downloaded artifact 
#   
# EXAMPLE:
#   cpcpw_maven_download_artifact(ARTIFACT_NAME      gtest
#                                 ARTIFACT_VERSION   1.7.0
#                                 ARTIFACT_GROUP     org.dci.gtest.windows.x86_64.msvc12
#                                 ARTIFACT_PACKAGING zip
#                                 REPO_URL           http://192.168.250.10:8081/nexus/content/repositories/thirdparty
#                                 MAVEN_HOME         c:/Users/myuser/.m2
#                                 INSTALL_DIRECTORY  c:/dev/myproject/thirdparty)
# 
#
# AUTHOR:
#   Matthias Schmieder (2015)
#-------------------------------------------------------------------------------------------
function(cpcpw_maven_download_artifact)
  set(options)
  set(oneValueArgs ARTIFACT_NAME ARTIFACT_VERSION ARTIFACT_GROUP ARTIFACT_PACKAGING REPO_URL MAVEN_HOME INSTALL_DIRECTORY)
  set(multiValueArgs)

  include(CMakeParseArguments)
  cmake_parse_arguments(Param "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set (mvn_get_cmd          "${BUILD_TOOLS_DIRECTORY}/scm-scripts/bash/maven_get_artifact.sh")
  set (mvn_get_cmd_args     "-m ${Param_MAVEN_HOME} -n ${Param_ARTIFACT_NAME} -v ${Param_ARTIFACT_VERSION} -p ${Param_ARTIFACT_PACKAGING} --repo-url ${Param_REPO_URL} -g ${Param_ARTIFACT_GROUP} -i ${Param_INSTALL_DIRECTORY} --verbose")
  set( download_script_file "${PROJECT_BINARY_DIR}/maven/download_${Param_ARTIFACT_NAME}.sh")

  file(WRITE "${download_script_file}" "#!/usr/bin/env bash\n${mvn_get_cmd} ${mvn_get_cmd_args}" )

  execute_process(COMMAND bash ${download_script_file}
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    RESULT_VARIABLE _result
    ERROR_VARIABLE _error
    OUTPUT_VARIABLE _output)

  message("RESULT_VARIABLE: ${_result}")
  message("OUTPUT_VARIABLE:\n${_output}")

  if(_error)
    if( ${_result} EQUAL 0 )
      message("${_error}")
    else()
      message(FATAL_ERROR "Error:\n${_error}")
    endif()
  endif()
endfunction(cpcpw_maven_download_artifact)