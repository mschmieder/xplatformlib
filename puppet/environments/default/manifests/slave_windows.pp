# make sure package manager is available
class {'chocolatey':
choco_install_location => 'c:/tools/choco',
}

if $::kernel == 'windows' {
  Package { provider => chocolatey, }
}

$system_packages = ['git','jre8','jdk8','rapidee']
package { $system_packages:
  ensure   => latest,
}

class { 'dci::slave':
  jenkins_url => 'http://192.168.250.10:8080',
  ssh_key_file => 'puppet:///data/jenkins_ssh_key',
  manage_slave_user => true,
  require => [Package['wget'],Package['jdk8'],Package['jre8']]
}

################################################################################
# INSTALL CYGWIN AND ADD NECESSARY MODULES AND ADD IT TO SYSTEM PATH
################################################################################
class { 'cygwin' : }

windows_env {'cygwin_bin_to_path':
  ensure    => present,
  variable  => 'PATH',
  value     => 'c:/Cygwin64/bin',
  mergemode => insert,
}

package { 'make':
  ensure => installed,
  provider => 'cygwin'
}

################################################################################
# INSTALL CMAKE AND ADD IT TO SYSTEM PATH
################################################################################
package { 'wget':
  ensure   => latest,
}

windows_env {'append_wget_to_path':
  ensure    => present,
  variable  => 'PATH',
  value     => 'c:/tools/choco/lib/Wget/tools',
  mergemode => insert,
}


################################################################################
# INSTALL CMAKE AND ADD IT TO SYSTEM PATH
################################################################################
package { 'cmake':
  ensure   => latest,
}

windows_env {'append_cmake_to_path':
  ensure    => present,
  variable  => 'PATH',
  value     => 'c:/Program Files (x86)/CMake/bin',
  mergemode => insert,
}

################################################################################
# INSTALL CMAKE AND ADD IT TO SYSTEM PATH
################################################################################
package { 'nssm':
  ensure   => latest,
}

windows_env {'append_nssm_to_path':
  ensure    => present,
  variable  => 'PATH',
  value     => 'c:/tools/choco/lib/NSSM/Tools/nssm-2.24/win64',
  mergemode => insert,
}



################################################################################
# MAKE SURE ALL WINDOWS UPDATES ARE INSTALLED
################################################################################
windows_updates::list { 'Windows Updates':
  ensure => 'present',
  name => '*'
}
# reboot { 'after_windows_update':
#   subscribe  => Windows_updates::List['Windows Updates'],
# }

class { 'visualstudio' : }

class { 'qt' : 
   root => 'c:/qt',
   components => { "5.6" => ['win64_msvc2015_64'] }
}

# # setup windows license key
# $windows_license_key=hiera('windows_license_key')
# exec { 'change_windows_license':
#   command      => "cmd.exe -c slmgr.vbs -ipk $windows_license_key",
#   path         => $::path
# }->
# exec { 'activate_windows_license':
#   command      => "cmd.exe -c slmgr.vbs -ato",
#   path         => $::path
# }