cpcpw_jobs::build_cpcpw{ 'cpcpw-linux-clang-x86_64' :
                        branch              => 'master',
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'clang',
                        target_system       => 'linux',
                        target_architecture => 'x86_64',
                        maven_repository    => 'snapshots'
                      }

cpcpw_jobs::build_cpcpw{ 'cpcpw-linux-gcc-x86_64' :
                        branch              => 'master',
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'gcc',
                        target_system       => 'linux',
                        target_architecture => 'x86_64',
                        maven_repository    => 'snapshots'
                      }

cpcpw_jobs::build_gtest{ 'gtest-linux-clang-x86_64' :
                        branch              => 'release-1.7.0',
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'clang',
                        target_system       => 'linux',
                        target_architecture => 'x86_64',
                        maven_repository    => 'thirdparty',
                        version             => '1.7.0'
                       }

cpcpw_jobs::build_gtest{ 'gtest-linux-gcc-x86_64' :
                        branch              => 'release-1.7.0',
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'gcc',
                        target_system       => 'linux',
                        target_architecture => 'x86_64',
                        maven_repository    => 'thirdparty',
                        version             => '1.7.0'
                       }


cpcpw_jobs::build_gtest{ 'gtest-windows-msvc14-x86_64' :
                        branch              => 'release-1.7.0',
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'msvc14',
                        target_system       => 'windows',
                        target_architecture => 'x86_64',
                        maven_repository    => 'thirdparty',
                        version             => '1.7.0'
                       }

cpcpw_jobs::build_zlib{ 'zlib-linux-clang-x86_64' :
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'clang',
                        target_system       => 'linux',
                        target_architecture => 'x86_64',
                        maven_repository    => 'thirdparty',
                        version             => '1.2.8'
                       }

cpcpw_jobs::build_zlib{ 'zlib-linux-gcc-x86_64' :
                        build_types         => ['release','debug'],
                        link_type           => 'shared',
                        compiler            => 'gcc',
                        target_system       => 'linux',
                        target_architecture => 'x86_64',
                        maven_repository    => 'thirdparty',
                        version             => '1.2.8'
                       }
