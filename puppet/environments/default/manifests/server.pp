Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

################################################################################
# SETUP PACKAGING SYSTEM WITH APT
################################################################################
class { 'apt':
update => {
  frequency => 'daily',
  },
}


################################################################################
# SETUP DISTRIBUTED CI SERVER
################################################################################
class { 'dci::server' : 
        jenkins_ssh_key_file=>'puppet:///data/jenkins_ssh_key'
      }


################################################################################
# BUILD ENVIRONMENT SETUP
################################################################################
$buildtools_packages = 
[ 'gcc',
  'gcc-4.9',
  'clang',
  'clang-3.6',
  'cmake',
  'python3-pip',
  'libtinyxml2-dev',
  'libjsoncpp-dev']


package { $buildtools_packages:
  ensure => 'latest',
}

class { 'opencv' :
        dependencies_only => true,
        manage_packages => true 
      }

define pip3::package () {
  exec { "$name" :
    command      => "pip3 install $name",
    path         => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    require      => Package['python3-pip']
  }
}

pip3::package{ 'pysftp' : }
pip3::package{ 'paramiko' : }
pip3::package{ 'beautifulsoup4' : }

class { 'qt' : 
   root => '/opt/qt',
   components => { "5.6" => [''] }
}