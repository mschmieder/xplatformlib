Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

################################################################################
# SETUP PACKAGING SYSTEM WITH APT
################################################################################
class { 'apt':
update => {
  frequency => 'daily',
  },
}

exec { 'java-cacerts':
  command => '/var/lib/dpkg/info/ca-certificates-java.postinst configure',
  require => Package['openjdk-8-jre-headless']
}

################################################################################
# GIT SETUP
################################################################################
class { 'git': }

git::config { 'user.name':
  value => 'jenkins-slave',
}

git::config { 'user.email':
  value => "jenkins-slave@$fqdn",
}

define pip3::package () {
  exec { "$name" :
    command      => "pip3 install $name",
    path         => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    require      => Package['python3-pip']
  }
}
  
class { 'dci::slave':
  jenkins_url => 'http://192.168.250.10:8080',
  ssh_key_file => 'puppet:///data/jenkins_ssh_key',
}

class { 'opencv' :
        dependencies_only => true,
        manage_packages => true 
      }

pip3::package{ 'pysftp' : }
pip3::package{ 'paramiko' : }
pip3::package{ 'beautifulsoup4' : }

class { 'qt' : 
   root => '/opt/qt',
   components => { "5.6" => [''] }
}