define cpcpw_jobs::build_cpcpw( $scm_trigger_expression='H/60 * * * *',
                                $branch="master",
                                $target_system='linux',
                                $target_architecture='x86_64',
                                $build_types=['release'],
                                $link_type='shared',
                                $compiler='clang',
                                $cmake_generator='Unix Makefiles',
                                $cmakew_additional_params='',
                                $qt5=true,
                                $qt5_install_directory='/opt/qt',
                                $maven_repository='snapshots',
                                $snapshot=true )
{

  $build_cmd=inline_template( 
'
mkdir -p $WORKSPACE/build


OPTS="$OPTS --target-system=<%=@target_system%>"
OPTS="$OPTS --target-architecture=<%=@target_architecture%>"
OPTS="$OPTS --link-type=<%=@link_type%>"
OPTS="$OPTS --compiler=<%=@compiler%>"

OPTS="$OPTS --qt5"
OPTS="$OPTS --qt5-install-directory=<%=@qt5_install_directory%>"

OPTS="$OPTS --build"
OPTS="$OPTS --test"
OPTS="$OPTS --test-output-directory=$WORKSPACE/testresults"
OPTS="$OPTS --install"
OPTS="$OPTS --install-directory=$WORKSPACE/install/cpcpw"

cd $WORKSPACE/cpcpw
git submodule init
git submodule update

<% @build_types.each do |build_type| -%>
mkdir -p ${WORKSPACE}/build/<%=build_type%>
cd ${WORKSPACE}/build/<%=build_type%>
bash $WORKSPACE/cpcpw/build-tools/build-scripts/bash/cmakew --source-directory=${WORKSPACE}/cpcpw/src --cmake-generator="<%=@cmake_generator%>" --build-type=<%=@build_type%> ${OPTS} <%=@cmakew_additional_params%>
<%end-%>

cd $WORKSPACE/install
zip -r cpcpw-<%=@branch%>-<%=@target_system%>-<%=@target_architecture%>-<%=@compiler%>.zip cpcpw 
cp *.zip $WORKSPACE/
' 
)


  $cleanup_cmd='rm -rf $WORKSPACE/build && rm -rf $WORKSPACE/install'


  $deploy_cmd=inline_template(
'
artifact_file="$WORKSPACE/cpcpw-<%=@branch%>-<%=@target_system%>-<%=@target_architecture%>-<%=@compiler%>.zip" 
artifact_group="org.dci.cpcpw.<%=@target_system%>.<%=@target_architecture%>.<%=@compiler%>"
artifact_packaging="zip"
artifact_version=$(grep -Eo \'^[0-9]+.[0-9]+.[0-9]+\' $WORKSPACE/cpcpw/src/version | head -n 1)
<% if @snapshot -%>
artifact_version=${artifact_version}-SNAPSHOT
<%end-%>
repo_url="http://192.168.250.10:8081/nexus/content/repositories/<%=@maven_repository%>"
repo_id="nexus"

bash $WORKSPACE/cpcpw/build-tools/scm-scripts/bash/maven_deploy_artifact.sh -g ${artifact_group} -f ${artifact_file} -n cpcpw -p ${artifact_packaging} -u ${repo_url} -r ${repo_id} -v ${artifact_version}
')


  $builders=[
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $build_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $deploy_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $cleanup_cmd
                },
              }
            ]

  cpcpw_jobs::job{ $name:
                   git_url => 'git@bitbucket.org:mschmieder/cpcpw.git',
                   checkout_dir => 'cpcpw',
                   builders => $builders,
                   scm_trigger_expression => $scm_trigger_expression,
                   branch => $branch,
                   assigned_node => $target_system
                  }

}