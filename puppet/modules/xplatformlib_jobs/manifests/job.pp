define cpcpw_jobs::job( $git_url = '',
                        $checkout_dir = '',
                        $branch = '',
                        $builders,
                        $scm_trigger_expression='H/60 * * * *',
                        $with_build_tools=false,
                        $assigned_node=''
                      )
{
  if $with_build_tools 
  {
      jenkins_job::create{ $name : 
      triggers => 
      [ 
        {
          'type' => "SCMTrigger",
          'params' => { 'spec' => $scm_trigger_expression }
        },
      ],
      scm => 
      [
        {
          'type' => "GitSCM",
          'params' =>  {
            'url' => $git_url,
            'branch' => $branch,
            'extensions' => {
                'type' => 'RelativeTargetDirectory',
                'params' => { 'relativeTargetDir' => $checkout_dir}
                }
            },
        },
        {
          'type' => "GitSCM",
          'params' =>  {
            'url' => 'git@bitbucket.org:mschmieder/build-tools.git',
            'branch' => 'master',
            'extensions' => {
                'type' => 'RelativeTargetDirectory',
                'params' => { 'relativeTargetDir' => 'build-tools'}
                }
            },
        },
      ],
      builders => $builders,
      publishers => [
        {
          'type' => "ArtifactArchiver",
          'params'  => {
            'artifacts' => '*.zip',
            'onlyIfSuccessful' => true,
          },
        }
      ],
      assignedNode => $assigned_node
    }
  }
  else
  {
   jenkins_job::create{ $name : 
      triggers => 
      [ 
        {
          'type' => "SCMTrigger",
          'params' => { 'spec' => $scm_trigger_expression }
        },
      ],
      scm => 
      [
        {
          'type' => "GitSCM",
          'params' =>  {
            'url' => $git_url,
            'branch' => $branch,
            'extensions' => {
                'type' => 'RelativeTargetDirectory',
                'params' => { 'relativeTargetDir' => $checkout_dir}
                }
            },
        },
      ],
      builders => $builders,
      publishers => [
        {
          'type' => "ArtifactArchiver",
          'params'  => {
            'artifacts' => '*.zip',
            'onlyIfSuccessful' => true,
          },
        }
      ],
      assignedNode => $assigned_node
    }
  }
}