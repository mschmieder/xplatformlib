define cpcpw_jobs::build_gtest( $scm_trigger_expression='',
                                $branch="release-1.7.0",
                                $target_system='linux',
                                $target_architecture='x86_64',
                                $build_types= ['release'],
                                $link_type='shared',
                                $compiler='clang',
                                $cmake_generator='Unix Makefiles',
                                $maven_repository = 'thirdparty',
                                $version = "1.7.0" )
{

  $build_cmd=inline_template( 
'
OPTS="--target-system=<%=@target_system%>"
OPTS="$OPTS --target-architecture=<%=@target_architecture%>"
OPTS="$OPTS --link-type=<%=@link_type%>"
OPTS="$OPTS --compiler=<%=@compiler%>"
OPTS="$OPTS -DBUILD_GTEST:BOOL=ON"
OPTS="$OPTS -DCMAKE_DEBUG_POSTFIX:STRING=d"
OPTS="$OPTS --build"

export CFLAGS="-fPIC"
export CXXFLAGS="-fPIC"

<% @build_types.each do |build_type| -%>
mkdir -p ${WORKSPACE}/build/<%=build_type%>
cd ${WORKSPACE}/build/<%=build_type%>
bash ${WORKSPACE}/build-tools/build-scripts/bash/cmakew --source-directory=${WORKSPACE}/gtest ${OPTS} --cmake-generator="<%=@cmake_generator%>" --build-type=<%=build_type%> <%=@cmakew_additional_params%>
<%end-%>
' )


  $install_cmd=inline_template(
'
mkdir -p ${WORKSPACE}/install/gtest
mkdir -p ${WORKSPACE}/install/gtest/lib
mkdir -p ${WORKSPACE}/install/gtest/bin
mkdir -p ${WORKSPACE}/install/gtest/include

<% @build_types.each do |build_type| -%>
rsync -av --ignore-missing-args ${WORKSPACE}/build/<%=build_type%>/*.so ${WORKSPACE}/install/gtest/lib/
rsync -av --ignore-missing-args ${WORKSPACE}/build/<%=build_type%>/*.lib ${WORKSPACE}/install/gtest/lib/
rsync -av --ignore-missing-args ${WORKSPACE}/build/<%=build_type%>/*.dylib ${WORKSPACE}/install/gtest/lib/
rsync -av --ignore-missing-args ${WORKSPACE}/build/<%=build_type%>/*.dll ${WORKSPACE}/install/gtest/bin/
<%end-%>

rsync -av --ignore-missing-args -r ${WORKSPACE}/gtest/include ${WORKSPACE}/install/gtest/

cd ${WORKSPACE}/install
zip -r gtest-<%=@branch%>-<%=@target_system%>-<%=@target_architecture%>-<%=@compiler%>.zip gtest 
cp *.zip ${WORKSPACE}/
')

  $deploy_cmd=inline_template(
'
artifact_file="${WORKSPACE}/gtest-<%=@branch%>-<%=@target_system%>-<%=@target_architecture%>-<%=@compiler%>.zip" 
artifact_group="org.dci.gtest.<%=@target_system%>.<%=@target_architecture%>.<%=@compiler%>"
artifact_packaging="zip"
artifact_version=<%=@version%>

repo_url="http://192.168.250.10:8081/nexus/content/repositories/<%=@maven_repository%>"
repo_id="nexus"

bash ${WORKSPACE}/build-tools/scm-scripts/bash/maven_deploy_artifact.sh -g ${artifact_group} -f ${artifact_file} -n gtest -p ${artifact_packaging} -u ${repo_url} -r ${repo_id} -v ${artifact_version}
')

  $cleanup_cmd='rm -rf ${WORKSPACE}/build && rm -rf ${WORKSPACE}/install'

  $builders=[
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $build_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $install_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $cleanup_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $deploy_cmd
                },
              },
            ]


  cpcpw_jobs::job{ $name:
                   git_url => 'https://github.com/google/googletest.git',
                   checkout_dir => 'gtest',
                   builders => $builders,
                   scm_trigger_expression => $scm_trigger_expression,
                   branch => $branch,
                   with_build_tools => true,                   
                   assigned_node => $target_system
                 }

}