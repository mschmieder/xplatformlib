define cpcpw_jobs::build_zlib( $scm_trigger_expression='',
                               $target_system='linux',
                               $target_architecture='x86_64',
                               $build_types= ['release'],
                               $link_type='shared',
                               $compiler='clang',
                               $cmake_generator='Unix Makefiles',
                               $maven_repository = 'thirdparty',
                               $version = "1.2.8" )
{
  $download_and_extract_cmd=inline_template(
'
  if [ ! -d "zlib-<%=@version%>" ];then
    wget http://zlib.net/zlib-<%=@version%>.tar.gz -O $WORKSPACE/zlib-<%=@version%>.tar.gz
    tar -xzf $WORKSPACE/zlib-<%=@version%>.tar.gz
    rm -rf zlib-<%=@version%>.tar.gz
  fi
')

  $build_cmd=inline_template( 
'
OPTS="--target-system=<%=@target_system%>"
OPTS="$OPTS --target-architecture=<%=@target_architecture%>"
OPTS="$OPTS --link-type=<%=@link_type%>"
OPTS="$OPTS --compiler=<%=@compiler%>"
OPTS="$OPTS --build"
OPTS="$OPTS --install"
OPTS="$OPTS --install-directory=$WORKSPACE/install/zlib"

OPTS="$OPTS -DINSTALL_BIN_DIR:STRING=$WORKSPACE/install/zlib/bin"
OPTS="$OPTS -DINSTALL_INC_DIR:STRING=$WORKSPACE/install/zlib/include"
OPTS="$OPTS -DINSTALL_LIB_DIR:STRING:STRING=$WORKSPACE/install/zlib/lib"
OPTS="$OPTS -DINSTALL_MAN_DIR:STRING:STRING=$WORKSPACE/install/zlib/share/man"
OPTS="$OPTS -DINSTALL_PKGCONFIG_DIR:STRING:STRING=$WORKSPACE/install/zlib/plkconfig"

<% @build_types.each do |build_type| -%>
mkdir -p ${WORKSPACE}/build/<%=build_type%>
cd ${WORKSPACE}/build/<%=build_type%>
bash ${WORKSPACE}/build-tools/build-scripts/bash/cmakew --source-directory=${WORKSPACE}/zlib-<%=@version%> ${OPTS} --cmake-generator="<%=@cmake_generator%>" --build-type=<%=build_type%> <%=@cmakew_additional_params%>
<%end-%>
' )


  $install_cmd=inline_template(
'
cd ${WORKSPACE}/install
zip -r zlib-<%=@branch%>-<%=@target_system%>-<%=@target_architecture%>-<%=@compiler%>.zip zlib 
cp *.zip ${WORKSPACE}/
')

  $deploy_cmd=inline_template(
'
artifact_file="${WORKSPACE}/zlib-<%=@branch%>-<%=@target_system%>-<%=@target_architecture%>-<%=@compiler%>.zip" 
artifact_group="org.dci.zlib.<%=@target_system%>.<%=@target_architecture%>.<%=@compiler%>"
artifact_packaging="zip"
artifact_version=<%=@version%>

repo_url="http://192.168.250.10:8081/nexus/content/repositories/<%=@maven_repository%>"
repo_id="nexus"

bash ${WORKSPACE}/build-tools/scm-scripts/bash/maven_deploy_artifact.sh -g ${artifact_group} -f ${artifact_file} -n zlib -p ${artifact_packaging} -u ${repo_url} -r ${repo_id} -v ${artifact_version}
')

  $cleanup_cmd='rm -rf ${WORKSPACE}/build && rm -rf ${WORKSPACE}/install'

  $builders=[ {
                "type" => "Shell",
                "params" =>  {
                  "command" => $download_and_extract_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $build_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $install_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $cleanup_cmd
                },
              },
              {
                "type" => "Shell",
                "params" =>  {
                  "command" => $deploy_cmd
                },
              },
            ]


  cpcpw_jobs::job{ $name:
                   git_url                => 'git@bitbucket.org:mschmieder/build-tools.git',
                   branch                 => 'master',
                   checkout_dir           => 'build-tools',
                   builders               => $builders,
                   scm_trigger_expression => $scm_trigger_expression,
                   assigned_node          => $target_system
                 }

}