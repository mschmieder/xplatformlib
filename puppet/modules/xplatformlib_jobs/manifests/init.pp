define cpcpw_build_job( $scm_trigger_expression='H/60 * * * *',
                        $branch="master",
                        $target_system='linux',
                        $target_architecture='x86_64',
                        $build_type='release',
                        $link_type='shared',
                        $compiler='clang',
                        $cmake_generator='Unix Makefiles',
                        $cmakew_additional_params='',
                        $qt5=true,
                        $qt5_install_directory='/opt/qt' )
{

  $build_cmd1=inline_template( 
'
mkdir -p $WORKSPACE/build


OPTS="$OPTS --target-system=<%=@target_system%>"
OPTS="$OPTS --target-architecture=<%=@target_architecture%>"
OPTS="$OPTS --build-type=<%=@build_type%>"
OPTS="$OPTS --link-type=<%=@link_type%>"
OPTS="$OPTS --compiler=<%=@compiler%>"
OPTS="$OPTS --cmake-generator=\"<%=@cmake_generator%>\""

OPTS="--target-architecture=<%=@target_architecture%>"
OPTS="--build-type=<%=@build_type%>"
OPTS="--link-type=<%=@link_type%>"
OPTS="--compiler=<%=@compiler%>"

OPTS="$OPTS --qt5"
OPTS="$OPTS --qt5-install-directory=<%=@qt5_install_directory%>"

OPTS="$OPTS --build"
OPTS="$OPTS --test"
OPTS="$OPTS --test-output-directory=$WORKSPACE/testresults"
OPTS="$OPTS --install"
OPTS="$OPTS --install-directory=$WORKSPACE/install"

cd $WORKSPACE/cpcpw
git submodule init
git submodule update

cd $WORKSPACE/build
$WORKSPACE/cpcpw/build-tools/build-scripts/bash/cmakew --source-directory=${WORKSPACE}/cpcpw/src "${OPTS}" <%=@cmakew_additional_params%>
' 
  )


  $build_cmd2='rm -rf $WORKSPACE/build && rm -rf $WORKSPACE/install'

  cpcpw_build_job::job{ $name:
                         scm_trigger_expression => $scm_trigger_expression,
                         branch => $branch,
                         target_system => $target_system,
                         target_architecture => $target_architecture,
                         build_type => $build_type,
                         link_type => $link_type,
                         compiler => $compiler,
                         cmake_generator => $cmake_generator,
                         cmakew_additional_params => $cmakew_additional_params,
                         qt5 => $qt5,
                         qt5_install_directory => $qt5_install_directory,
                         shell_build_cmds => ["$build_cmd1","$build_cmd2"] 
                       }

}